import data from './../data';
import inde from './../index';
var fs = require('fs')
import * as tool from '../tool/tool';
import { NetworkPhases } from '../NetworkPhases';
import DTConstants from '../DTConstants';

import { makeTheTuto } from '../tuto';
import { backStartMap } from '../backStartMap';
import { moveToZaap } from '../moveToZaap';
import mapData from "../path/map";
var md5 = require('md5');
const shell = require('shelljs');
export default class frame {

	constructor(
		public dis: data,
		public io: any
	) {
		this.register()
	}
	private register(){
		// connection
		this.dis.dis.register('HelloConnectMessage', this.onHelloConnectMessage, this);
		this.dis.dis.register('assetsVersionChecked', this.onAssetsVersionChecked, this);
		this.dis.dis.register('ServersListMessage', this.onServersListMessage, this);
		this.dis.dis.register('SelectedServerDataMessage', this.onSelectedServerDataMessage, this);
		this.dis.dis.register('HelloGameMessage', this.onHelloGameMessage, this);
		this.dis.dis.register('AuthenticationTicketAcceptedMessage', this.onAuthenticationTicketAcceptedMessage, this)
		this.dis.dis.register('CharactersListMessage', this.onCharactersListMessage, this)
		this.dis.dis.register('CharacterSelectedSuccessMessage', this.onCharacterSelectedSuccessMessage, this)
		this.dis.dis.register('IdentificationSuccessMessage', this.onIdentificationSuccessMessage, this)
		this.dis.dis.register('SequenceNumberRequestMessage', this.onSequenceNumberRequestMessage, this)
		this.dis.dis.register('BasicLatencyStatsRequestMessage', this.onBasicLatencyStatsRequestMessage, this)
		this.dis.dis.register('CurrentMapMessage', this.onCurrentMapMessage, this)
		this.dis.dis.register('CharacterCreationResultMessage', this.onCharacterCreationResultMessage, this)
		this.dis.dis.register('CharacterDeletionErrorMessage', this.onCharacterDeletionErrorMessage, this)
		
		// map path
		this.dis.dis.register('TextInformationMessage', this.onTextInformationMessage, this)
		this.dis.dis.register('MapComplementaryInformationsDataMessage', this.onMapComplementaryInformationsDataMessage, this)
		this.dis.dis.register('GameRolePlayShowActorMessage', this.onGameRolePlayShowActorMessage, this)

		// invetory
		this.dis.dis.register('InventoryContentMessage', this.onInventoryContentMessage, this)

		// hdv
		this.dis.dis.register('ExchangeStartedBidBuyerMessage', this.onExchangeStartedBidBuyerMessage, this)
		this.dis.dis.register('ExchangeTypesExchangerDescriptionForUserMessage', this.onExchangeTypesExchangerDescriptionForUserMessage, this)
		this.dis.dis.register('ExchangeTypesItemsExchangerDescriptionForUserMessage', this.onExchangeTypesItemsExchangerDescriptionForUserMessage, this)

		// 

		this.dis.dis.register('MapComplementaryInformationsDataInHouseMessage', this.onMapComplementaryInformationsDataInHouseMessage, this)


		//pod
		this.dis.dis.register('InventoryWeightMessage', this.onInventoryWeightMessage, this)
		
	}

	private onInventoryWeightMessage(data: any) {
		console.log(data.weight)
		if (data.weight >= 4000) {
			this.goBanque()
		}
	}

	private goBanque() {
		this.dis.network.sendMessage("LeaveDialogRequestMessage");
		this.dis.network.sendMessage("NpcGenericActionRequestMessage", {
			npcId: -2, npcActionId: 3, npcMapId: 83887104
		});
		this.dis.network.sendMessage("NpcDialogReplyMessage", {
			replyId: 259
		});
		this.dis.network.sendMessage("ExchangeObjectTransfertAllFromInvMessage");
		this.dis.network.sendMessage("LeaveDialogRequestMessage");
		this.dis.network.sendMessage("NpcGenericActionRequestMessage", {
			npcId: 0, npcActionId: 6, npcMapId: 83887104
		});
	}

	
	private onExchangeTypesItemsExchangerDescriptionForUserMessage(data: any) {
		//console.log('receive price', data['itemTypeDescriptions'][0])
		if (this.dis.open === 0) {
			this.dis.open = 1
			if (data['itemTypeDescriptions'][0]) {
				if (data['itemTypeDescriptions'][0].prices[0] <= 800 && data['itemTypeDescriptions'][0].prices[0] !== 0) {
					this.dis.network.sendMessage("ExchangeBidHouseBuyMessage", {
						uid: data['itemTypeDescriptions'][0].objectUID, qty: 1, price: data['itemTypeDescriptions'][0].prices[0]
					});
				}
				if (data['itemTypeDescriptions'][0].prices[1] <= 8000 && data['itemTypeDescriptions'][0].prices[1] !== 0) {
					this.dis.network.sendMessage("ExchangeBidHouseBuyMessage", {
						uid: data['itemTypeDescriptions'][0].objectUID, qty: 10, price: data['itemTypeDescriptions'][0].prices[1]
					});
				}
				if (data['itemTypeDescriptions'][0].prices[2] <= 80000 && data['itemTypeDescriptions'][0].prices[2] !== 0) {
					this.dis.network.sendMessage("ExchangeBidHouseBuyMessage", {
						uid: data['itemTypeDescriptions'][0].objectUID, qty: 100, price: data['itemTypeDescriptions'][0].prices[2]
					});
				}				
			}
			
		} else {
			this.dis.open = 0
		}
		this.dis.network.sendMessage("ExchangeBidHouseTypeMessage", {
			type: 164
		});
	}


	private async onExchangeTypesExchangerDescriptionForUserMessage(data: any) {
		//console.log("Select item")
		
			//await tool.wait(1000)
			this.dis.network.sendMessage("ExchangeBidHouseListMessage", {
				id: 8760
			});
		
	}

	private async onExchangeStartedBidBuyerMessage(data: any) {
		console.log("Select catégorie")
		//await tool.wait(1000)
		this.dis.network.sendMessage("ExchangeBidHouseTypeMessage", {
			type: 164
		});
	}
	

	private async onMapComplementaryInformationsDataMessage(data: any) {
		this.dis.network.sendMessage("NpcGenericActionRequestMessage", {
			npcId: 0, npcActionId: 6, npcMapId: 83887104
		});
		/*
		console.log("Connecter", data.mapId)
		const player = data.actors.find(a => a.contextualId === this.dis.characterId);
		this.dis.currentCell = player.disposition.cellId;
		await this.dis.Movement.setMap(data.mapId);			
		if (data.mapId === 84804102) {
			console.log("Dehors on va dedans")
			this.dis.network.sendMessage("InteractiveUseRequestMessage", {
				elemId: 465555, skillInstanceUid: 19535197
			});
			this.dis.network.sendMessage("LockableUseCodeMessage", {
				code: "231111__"
			});
			this.dis.network.sendMessage("MapInformationsRequestMessage", {
				mapId: 82840582
			});    		
		}
		*/
	}

	private async onMapComplementaryInformationsDataInHouseMessage(data: any) {
		if (this.dis.open2 === 0 ) {
			this.dis.open2 = 1

			console.log("dedans")
			const player = data.actors.find(a => a.contextualId === this.dis.characterId);
			this.dis.currentCell = player.disposition.cellId;
			await this.dis.Movement.setMap(data.mapId);			
			if (data.mapId === 82840582) {
				console.log("Dedans prend l'escalier")
				var path = this.dis.Movement.moveToCell(this.dis.currentCell, 388);
		        this.dis.network.sendMessage("GameMapMovementRequestMessage", {
		          keyMovements: path, mapId: 82840582
		        });
		        this.dis.network.sendMessage("GameMapMovementConfirmMessage");
		        this.dis.network.sendMessage("InteractiveUseRequestMessage", {
		          elemId: 465499, skillInstanceUid: 19535195
		        });
		        this.dis.network.sendMessage("MapInformationsRequestMessage", {
		          mapId: 82841606
		        });
		        await tool.wait(1000)
	    		console.log("en haut devant coffre")
				this.dis.network.sendMessage("NpcGenericActionRequestMessage", {
					npcId: 0, npcActionId: 6, npcMapId: 82841606
				});
			}
		}
	}


	private onInventoryContentMessage(data: any) {
		for (var i of data['objects']) {
			if (i.objectGID === 6805) {
				this.dis.itemSpecial = i.objectUID
			}
			if (i.objectGID === 7454) {
				this.dis.res = i.objectUID
				this.dis.resQ = i.quantity
			}
			if (i.objectGID === 7436) {
				this.dis.pui = i.objectUID
				this.dis.puiQ = i.quantity
			}
			this.dis.tutoInventory.push(i)
		}
	}

	private onCharactersListMessage(data: any) {
    	this.dis.network.username = this.dis.username
		if (data.characters.length > 0) {
			this.dis.characterId = data.characters[0].id
		}		
		this.dis.network.sendMessage("CharacterFirstSelectionMessage", {
			doTutorial: false,
			id: this.dis.characterId
		});
	}

	private async onGameRolePlayShowActorMessage(data: any) { // new player on map
		
		
	}

	// connection 
	private onGameActionFightNoSpellCastMessage(data: any) {
		//console.log(data, "fight action fight")
	}
	private onCharacterCreationResultMessage(data: any) {
		if (data.result === 0){
			console.log("Personnage créé avec succès.")
		} else {
			console.log("Impossible de créer le personnage : " + data.result)
			this.dis.network.crashed = 5
			this.dis.network.close()
			shell.exec("cd " + this.dis.path + " && npm start " + this.dis.username)
		  }
	}

	private onCharacterDeletionErrorMessage(data: any) {
		if (data.reason === 0){
			console.log("Personnage supprimé avec succès.")
		}
		else {
			console.log("Impossible de supprimer le personnage : " + data.reason)
		  }
	}



	private onTextInformationMessage(data: any) {
		let text: string = data.text;
		for (let i = 0; i < data.parameters.length; i++) {
			text = text.replace(`$%${i}`, data.parameters[i]);
		}
		// text.indexOf('de kamas pour acheter cet objet') === -1 &&  text.indexOf('disponible à ce prix') === -1 && text.indexOf('item') === -1
		if (text.indexOf('avez perdu') === -1 && text.indexOf('ur va être') === -1 && text.indexOf('mise à') === -1 && text.indexOf('il est int') === -1 && text.indexOf('mémoriser') === -1 && text.indexOf('lle quê') === -1 && text.indexOf('terminée') === -1 && text.indexOf('sauvegardée') === -1) {
			console.log(text);
		}
	}

	private onCurrentMapMessage(data: any) {
		this.dis.mapId = data.mapId
		this.dis.network.sendMessage("MapInformationsRequestMessage", {
			mapId: data.mapId
		});
	}

	
	// connection


	private onBasicLatencyStatsRequestMessage(data: any) {
		this.dis.network.sendMessage("BasicLatencyStatsMessage", {
			latency: 262,
			max: 50,
			sampleCount: 12
		});
	}

	private onSequenceNumberRequestMessage(data: any) {
		this.dis.sequence++;
		this.dis.network.sendMessage("SequenceNumberMessage", {
			number: this.dis.sequence
		});
	}


	private onIdentificationSuccessMessage(data: any) {
		this.dis.login = data.login
		this.dis.subscriber = new Date() < data.subscriptionEndDate;
	}

	private onCharacterSelectedSuccessMessage(data: any) {
		this.dis.network.sendMessage("kpiStartSession", {
			accountSessionId: this.dis.login,
			isSubscriber: this.dis.subscriber
		});
		this.dis.network.send("moneyGoultinesAmountRequest");
		this.dis.network.sendMessage("QuestListRequestMessage");
		this.dis.network.sendMessage("FriendsGetListMessage");
		this.dis.network.sendMessage("IgnoredGetListMessage");
		this.dis.network.sendMessage("SpouseGetInformationsMessage");
		this.dis.network.send("bakSoftToHardCurrentRateRequest");
		this.dis.network.send("bakHardToSoftCurrentRateRequest");
		this.dis.network.send("restoreMysteryBox");
		this.dis.network.sendMessage("ClientKeyMessage", {
			key: tool.randomString(21)
		});
		this.dis.network.sendMessage("GameContextCreateRequestMessage");
	}

	

	private onAuthenticationTicketAcceptedMessage(data: any) {
		this.dis.network.sendMessage('CharactersListRequestMessage');
	}

	private onHelloGameMessage(data: any) {
		this.dis.network.sendMessage("AuthenticationTicketMessage", {
			lang: "fr",
			ticket: this.dis.ticket
		});
		this.dis.network.phase = NetworkPhases.GAME;
	}

	private onSelectedServerDataMessage(data: any) {
		this.dis.ticket = data.ticket
		this.dis.network.switchToGameServer(data._access, {
			address: data.address,
			id: data.serverId,
			port: data.port
		});
	}

	private onServersListMessage(data: any) {
		this.dis.network.sendMessage('ServerSelectionMessage', {
			serverId: this.dis.serverId
		});
	}

	private onHelloConnectMessage(data: any) {
		this.dis.network.phase = NetworkPhases.LOGIN;
		this.dis.key = data.key;
		this.dis.salt = data.salt;

		this.dis.network.send('checkAssetsVersion', {
			assetsVersion: DTConstants.assetsVersion,
			staticDataVersion: DTConstants.staticDataVersion,
		});
	}
	private onAssetsVersionChecked(data: any) {
		this.dis.network.send('login', {
			key: this.dis.key,
			salt: this.dis.salt,
			token: this.dis.haapi.token,
			username: this.dis.username,
		});
	}

}
