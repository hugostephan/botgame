import axios from 'axios';
import DTConstants from './DTConstants';
import { HaapiErrorReasons, IHaapi } from './IHaapi';

export default class HaapiConnection {

  public haapi: IHaapi;
  public token: string;

  public async processHaapi(username: string, password: string, serverId: string): Promise<void> {
    try {
    this.haapi = await this.createApiKey(username, password, serverId);

      if (this.haapi.reason) {
        console.log("HAPPI REASON")
        switch (this.haapi.reason) {
          case HaapiErrorReasons.FAILED:
            this.writeBan([username, serverId])
            console.log("MAUVAIS LOG")
            return
            throw new Error('Identifiants incorrects');
          case HaapiErrorReasons.BAN: 
            this.writeBan([username, serverId])
            console.log("BAN")
            throw new Error('Compte banni!');
        }
      } else {
        console.log("BUG HAPPI REASON")
      }

      this.token = await this.getToken();
    } catch (e) {
      console.log("ERROR POPO")
      this.writeBan([username, serverId])
      return
    }
  }
  private writeBan(result){
    var fs = require('fs')
    fs.writeFile('./src/compteBan.txt', result, 'utf8', function(err) {
      if (err) return console.log(err);
    });
  }
  private async createApiKey(username: string, password: string, serverId: string): Promise<IHaapi> {
    console.log("TRUCK ICI")
    try {
      const response = await axios.post(`${DTConstants.config.haapi.url}/Api/CreateApiKey`,
        'login=' + username + '&password=' + password + '&long_life_token=false');
      console.log(response.data)
      return response.data;
    } catch (e) {
      console.log("CATCH EVENT")
      if (e.response && e.response.status === 601) {
        return e.response.data;
      }
      throw new Error(e.message);
    }
  }

  private async getToken(): Promise<string> {
    const config = {
      headers: {
        apikey: this.haapi.key,
      },
      params: {
        game: DTConstants.config.haapi.id,
      },
    };
    try {
      const response = await axios.get(`${DTConstants.config.haapi.url}/Account/CreateToken`, config);
      console.log(response.data.token)
      return response.data.token;
    } catch (e) {
      console.log("TOKEN 2")
      throw new Error(e.message);
    }
  }
}
