"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const DTConstants_1 = require("./DTConstants");
const MapPoint_1 = require("./path/MapPoint");
const tool = require("./tool/tool");
const frame_1 = require("./frame");
const data_1 = require("./data");
var fs = require('fs');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var messageToSend = [];
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});
io.on('connection', function (socket) {
    //console.log('new man')
    socket.on('disconnect', function () {
        //console.log('socket disconnect')
        socket.disconnect();
    });
    socket.on('chatResponse', function (characterId, receiver, content) {
        messageToSend.push({
            characterId: characterId,
            receiver: receiver,
            content: content
        });
        var param = {
            "characterId": characterId,
            "receiver": receiver,
            "content": content
        };
        socket.broadcast.emit('autreMsg', param);
    });
});
var localtunnel = require('localtunnel');
http.listen(3002, function () {
    console.log('listening on *:3000');
});
class framsethis {
    constructor(username, password, serverId, createNew) {
        this.username = username;
        this.password = password;
        this.serverId = serverId;
        this.createNew = createNew;
        this.start(username, password, serverId, createNew);
    }
    start(username, password, serverId, createNew) {
        return __awaiter(this, void 0, void 0, function* () {
            let Data = new data_1.default();
            Data.serverId = serverId;
            Data.createNew = createNew;
            let Frame;
            yield DTConstants_1.default.Init();
            yield Data.init(username, password, serverId);
            MapPoint_1.default.Init();
            Frame = new frame_1.default(Data, io);
        });
    }
}
exports.default = framsethis;
// hedergrize 405, brutas 407, terracogita 404, grandapan 401, dodge 406, oshimo 403
const _1 = require("./");
function s(ndc) {
    return __awaiter(this, void 0, void 0, function* () {
        // netijavija@1shivom.com
        // start("Echodu01", 'combat123', 405, false)
        //start(ndc, 'azerty321', 405, false)
        new _1.default(ndc, 'azerty321', 405, false);
    });
}
fs.readFile('./src/listAllAccount.txt', 'utf8', function (err, datas) {
    var data = datas.split('\r\n');
    s(data[0]);
});
function checkFinish() {
    return __awaiter(this, void 0, void 0, function* () {
        fs.readFile('./src/finish.txt', 'utf8', function (err, datas) {
            if (datas !== '') {
                fs.readFile('./src/listAllAccount.txt', 'utf8', function (err, datab) {
                    var data = datas.split(',');
                    var result = datab.replace(data[0] + "\r\n", '');
                    console.log('COMPTE QUI A FINI', data[0]);
                    fs.writeFile('./src/listAllAccount.txt', result, 'utf8', function (err) {
                        if (err)
                            return console.log(err);
                    });
                    fs.writeFile('./src/finish.txt', '', 'utf8', function (err) {
                        console.log('RESET FINISH');
                    });
                });
            }
        });
        yield tool.wait(500);
        checkFinish();
    });
}
checkFinish();
