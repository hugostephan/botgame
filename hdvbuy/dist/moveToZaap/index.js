"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
function moveToZaap(Data) {
    return __awaiter(this, void 0, void 0, function* () {
        function go(cellToGo, mapToGo, mapGosth) {
            var path = Data.Movement.moveToCell(Data.currentCell, cellToGo);
            Data.network.sendMessage("GameMapMovementRequestMessage", {
                keyMovements: path,
                mapId: Data.mapId
            });
            Data.network.sendMessage("GameMapMovementConfirmMessage");
            Data.network.sendMessage("ChangeMapMessage", {
                mapId: mapGosth
            });
            Data.network.sendMessage("MapInformationsRequestMessage", {
                mapId: Data.mapId
            });
            //console.log(Data.mapId + ' ==> ' + mapToGo)
        }
        //console.log('on regarde ou on doit aller')
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            if (Data.mapId === 81264642) {
                go(558, 81268739, 81264643);
            }
            else if (Data.mapId === 81268739) {
                go(503, 81269763, 81269251);
            }
            else if (Data.mapId === 81269763) {
                go(447, 81270787, 81270275);
            }
            else if (Data.mapId === 81270787) {
                go(556, 80216068, 81270788);
            }
            else if (Data.mapId === 80216068) {
                go(251, 80216580, 80216580);
            }
            else if (Data.mapId === 80216580) {
                go(363, 80217092, 80217092);
            }
            else if (Data.mapId === 80217092) {
                go(391, 80217604, 80217604);
            }
            else if (Data.mapId === 80217604) {
                go(419, 80218116, 80218116);
            }
            else if (Data.mapId === 80218116) {
                go(503, 80218628, 80218628);
            }
            else if (Data.mapId === 80218628) {
                go(503, 80219140, 80219140);
            }
            else if (Data.mapId === 80219140) {
                go(223, 80219652, 80219652);
            }
            else if (Data.mapId === 80219652) {
                go(419, 80220164, 80220164);
            }
            else if (Data.mapId === 80220164) {
                go(419, 80220676, 80220676);
            }
            else if (Data.mapId === 80220676) {
                Data.network.sendMessage("NpcGenericActionRequestMessage", {
                    npcId: -2,
                    npcActionId: 3,
                    npcMapId: 80220676
                });
                Data.network.sendMessage("ObjectAveragePricesGetMessage");
                Data.network.sendMessage("NpcDialogReplyMessage", {
                    replyId: 10537
                });
                Data.network.sendMessage("NpcDialogReplyMessage", {
                    replyId: 10535
                });
                Data.network.sendMessage("MapInformationsRequestMessage", {
                    mapId: 84804101
                });
                Data.network.sendMessage("AchievementRewardRequestMessage", {
                    achievementId: -1
                });
            }
            else if (Data.mapId === 84804101) {
                go(555, 84804102, 84804102);
            }
            else if (Data.mapId === 84804102) {
                go(555, 84804103, 84804103);
            }
            else if (Data.mapId === 84804103) {
                go(555, 84804104, 84804104);
            }
            else if (Data.mapId === 84804104) {
                go(550, 84804105, 84804105);
            }
            else if (Data.mapId === 84804105) {
                go(223, 84672521, 84672521);
            }
            else if (Data.mapId === 84672521) {
                go(223, 84673033, 84673033);
            }
            else if (Data.mapId === 84673033) {
                go(223, 84673545, 84673545);
            }
            else if (Data.mapId === 84673545) {
                go(554, 84673546, 84673546);
            }
            else if (Data.mapId === 84673546) {
                go(554, 84673547, 84673547);
            }
            else if (Data.mapId === 84673547) {
                go(554, 84673548, 84673548);
            }
            else if (Data.mapId === 84673548) {
                go(554, 84673549, 84673549);
            }
            else if (Data.mapId === 84673549) {
                go(554, 84673550, 84673550);
            }
            else if (Data.mapId === 84673550) {
                go(420, 70778880, 84673038);
            }
            else if (Data.mapId === 70778880) {
                go(443, 775, 775); // a pas de mapgost
            }
            else if (Data.mapId === 775) {
                go(550, 774, 774);
            }
            else if (Data.mapId === 774) {
                go(548, 88080668, 773);
            }
            else if (Data.mapId === 88080668) {
                go(552, 88080667, 88080667);
            }
            else if (Data.mapId === 88080667) {
                go(555, 88080666, 88080666);
            }
            else if (Data.mapId === 88080666) {
                go(555, 88080665, 88080665);
            }
            else if (Data.mapId === 88080665) {
                go(555, 88080664, 88080664);
            }
            else if (Data.mapId === 88080664) {
                go(555, 88080663, 88080663);
            }
            else if (Data.mapId === 88080663) {
                go(336, 88212247, 88212247);
            }
            else if (Data.mapId === 88212247) {
                go(336, 88212759, 88212759);
            }
            else if (Data.mapId === 88212759) {
                go(336, 88213271, 88213271);
            }
            else if (Data.mapId === 88213271) {
                console.log('READY FOR SPAM', Data.username);
            }
            else {
                //console.log('on est perdu', Data.mapId)
            }
            //return reject('TUTO BUGGED')
        }));
    });
}
exports.moveToZaap = moveToZaap;
