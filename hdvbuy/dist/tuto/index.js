"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tool = require("../tool/tool");
function moveTo(Data) {
    return __awaiter(this, void 0, void 0, function* () {
        function go(cellToGo, mapToGo, mapGosth) {
            var path = Data.Movement.moveToCell(Data.currentCell, cellToGo);
            Data.network.sendMessage("GameMapMovementRequestMessage", {
                keyMovements: path,
                mapId: Data.mapId
            });
            Data.network.sendMessage("GameMapMovementConfirmMessage");
            Data.network.sendMessage("ChangeMapMessage", {
                mapId: mapGosth
            });
            Data.network.sendMessage("MapInformationsRequestMessage", {
                mapId: mapToGo
            });
        }
        function acheterMarchand() {
            return __awaiter(this, void 0, void 0, function* () {
                Data.network.sendMessage("ExchangeOnHumanVendorRequestMessage", {
                    humanVendorId: 1610568, humanVendorCell: 232
                });
                yield tool.wait(1000);
                Data.network.sendMessage("ExchangeBuyMessage", {
                    objectToBuyId: Data.marchandObjectId, quantity: 3
                });
                Data.network.sendMessage("LeaveDialogRequestMessage");
                console.log('ITEM ACHETER', Data.marchandObjectId);
                yield tool.wait(1000);
                console.log('map 1');
                go(552, 84806915, 84806915);
                yield tool.wait(1000);
                console.log('map 2');
                go(195, 84806403, 84806403);
                yield tool.wait(1000);
                console.log('map 3');
                go(195, 84805891, 84805891);
                yield tool.wait(1000);
                console.log('map 4');
                go(553, 84805890, 84805890);
                yield tool.wait(1000);
            });
        }
        function goIn() {
            var path = Data.Movement.moveToCell(Data.currentCell, 316);
            Data.network.sendMessage("GameMapMovementRequestMessage", {
                keyMovements: path,
                mapId: Data.mapId
            });
            Data.network.sendMessage("GameMapMovementConfirmMessage");
            Data.network.sendMessage("InteractiveUseRequestMessage", {
                elemId: 475660, skillInstanceUid: 15886272
            });
            Data.network.sendMessage("MapInformationsRequestMessage", {
                mapId: 101450251
            });
        }
        function prendreVieux() {
            console.log('PARLER VIEUX');
            Data.network.sendMessage("NpcGenericActionRequestMessage", { npcId: -2, npcActionId: 3, npcMapId: 101450251 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 13557 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 13556 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 13555 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 13554 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 13552 });
            Data.network.sendMessage("QuestStepInfoRequestMessage", { questId: 954 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19022 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19096 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19094 });
            Data.network.sendMessage("QuestStepInfoRequestMessage", { questId: 1350 });
            Data.network.sendMessage("NpcGenericActionRequestMessage", { npcId: -2, npcActionId: 3, npcMapId: 101450251 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 13563 });
            Data.network.sendMessage("QuestStepInfoRequestMessage", { questId: 1350 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 13562 });
            Data.network.sendMessage("LeaveDialogRequestMessage");
            console.log('FINI PARLER VIEUX');
            goDeuxiemeSalle();
        }
        function goDeuxiemeSalle() {
            console.log('GO MAP 2');
            var path = Data.Movement.moveToCell(Data.currentCell, 274);
            Data.network.sendMessage("GameMapMovementRequestMessage", {
                keyMovements: path,
                mapId: Data.mapId
            });
            Data.network.sendMessage("GameMapMovementConfirmMessage");
            Data.network.sendMessage("InteractiveUseRequestMessage", {
                elemId: 476222, skillInstanceUid: 15892611
            });
            Data.network.sendMessage("MapInformationsRequestMessage", {
                mapId: 101451273
            });
        }
        function prendrePorte() {
            console.log('GO PIERRE');
            var path = Data.Movement.moveToCell(Data.currentCell, 274);
            Data.network.sendMessage("GameMapMovementRequestMessage", {
                keyMovements: path,
                mapId: Data.mapId
            });
            Data.network.sendMessage("GameMapMovementConfirmMessage");
            Data.network.sendMessage("InteractiveUseRequestMessage", {
                elemId: 476221, skillInstanceUid: 15892627
            });
            Data.network.sendMessage("MapInformationsRequestMessage", {
                mapId: 101453317
            });
        }
        function parlerPierre() {
            var path = Data.Movement.moveToCell(Data.currentCell, 398);
            Data.network.sendMessage("GameMapMovementRequestMessage", {
                keyMovements: path,
                mapId: Data.mapId
            });
            Data.network.sendMessage("GameMapMovementConfirmMessage");
            Data.currentCell = 398;
            Data.network.sendMessage("InteractiveUseRequestMessage", {
                elemId: 475638, skillInstanceUid: 15892634
            });
            Data.network.sendMessage("QuestStepInfoRequestMessage", {
                questId: 1348
            });
            console.log('PIERRE FINI');
            parlerBonome();
        }
        function parlerBonome() {
            console.log("PARLER BONOME");
            Data.network.sendMessage("NpcGenericActionRequestMessage", { npcId: -30, npcActionId: 3, npcMapId: 101453317 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19098 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19100 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19099 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19104 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19103 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19102 });
            Data.network.sendMessage("NpcDialogReplyMessage", { replyId: 19101 });
            Data.network.sendMessage("QuestStepInfoRequestMessage", { questId: 1350 });
            Data.network.sendMessage("LeaveDialogRequestMessage");
            outBonome();
        }
        function outBonome() {
            return __awaiter(this, void 0, void 0, function* () {
                //await tool.wait(2000)
                console.log("On sort de la map pierre apres avoir parler au bonome");
                console.log(Data.currentCell);
                var path = Data.Movement.moveToCell(Data.currentCell, 452);
                Data.network.sendMessage("GameMapMovementRequestMessage", {
                    keyMovements: path,
                    mapId: Data.mapId
                });
                Data.network.sendMessage("GameMapMovementConfirmMessage");
                Data.network.sendMessage("MapInformationsRequestMessage", {
                    mapId: 101451273
                });
                Data.almanax = true;
                console.log('ALMANAX OK');
            });
        }
        function almaBackToall() {
            console.log('GO TO HALL');
            var path = Data.Movement.moveToCell(Data.currentCell, 466);
            Data.network.sendMessage("GameMapMovementRequestMessage", {
                keyMovements: path,
                mapId: Data.mapId
            });
            Data.network.sendMessage("GameMapMovementConfirmMessage");
            Data.network.sendMessage("MapInformationsRequestMessage", {
                mapId: 101450251
            });
        }
        function finirParlerVieux() {
            console.log('PARLE FINIR QUETE VIEUX');
            Data.network.sendMessage("NpcGenericActionRequestMessage", {
                npcId: -2, npcActionId: 3, npcMapId: 101450251
            });
            Data.network.sendMessage("NpcDialogReplyMessage", {
                replyId: 13564
            });
            Data.network.sendMessage("NpcDialogReplyMessage", {
                replyId: 13565
            });
            goOut();
        }
        function goOut() {
            console.log('ON SORT DEHORS');
            var path = Data.Movement.moveToCell(Data.currentCell, 465);
            Data.network.sendMessage("GameMapMovementRequestMessage", {
                keyMovements: path,
                mapId: Data.mapId
            });
            Data.network.sendMessage("GameMapMovementConfirmMessage");
            Data.network.sendMessage("MapInformationsRequestMessage", {
                mapId: 84805890
            });
        }
        function vendreMarchand() {
            Data.network.sendMessage("ExchangeOnHumanVendorRequestMessage", {
                humanVendorId: 1610568, humanVendorCell: 232
            });
            Data.network.sendMessage("ExchangeBuyMessage", {
                objectToBuyId: Data.marchandObjectId2, quantity: 1
            });
            Data.network.sendMessage("LeaveDialogRequestMessage");
        }
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            if (!Data.almanax) {
                if (Data.mapId === 84806916) { // map de start   
                    acheterMarchand(); // PLUS GO SUR LAUTRE MAP        
                }
                else if (Data.mapId === 84806915) {
                    //go(195, 84806403, 84806403)
                }
                else if (Data.mapId === 84806403) {
                    //go(195, 84805891, 84805891)
                }
                else if (Data.mapId === 84805891) {
                    //go(553, 84805890, 84805890)
                }
                else if (Data.mapId === 84805890) { // MAP DEHORS
                    console.log('MAP DEHORS');
                    goIn();
                }
                else if (Data.mapId === 101450251) {
                    console.log("MAP DEDANS");
                    prendreVieux();
                }
                else if (Data.mapId === 101451273) {
                    console.log("MAP 2");
                    prendrePorte();
                }
                else if (Data.mapId === 101453317) {
                    console.log("MAP PIERRE");
                    parlerPierre();
                }
                else {
                    console.log('on est perdu', Data.mapId, " ALMANAX PAS FAIT");
                }
            }
            if (Data.almanax) {
                if (Data.mapId === 84806916) { // map de start         
                }
                else if (Data.mapId === 101451273) {
                    almaBackToall();
                }
                else if (Data.mapId === 101450251) {
                    finirParlerVieux();
                }
                else if (Data.mapId === 84805890) {
                    console.log("ON EST DEHORS");
                    go(4, 84805891, 84805891);
                }
                else if (Data.mapId === 84805891) {
                    go(364, 84806403, 84806403);
                }
                else if (Data.mapId === 84806403) {
                    go(364, 84806915, 84806915);
                }
                else if (Data.mapId === 84806915) {
                    go(4, 84806916, 84806916);
                }
                if (Data.mapId === 84806916) { // map de start   
                    console.log('ARRIVER POUR VENDRE');
                    vendreMarchand(); // PLUS GO SUR LAUTRE MAP        
                }
                else {
                    console.log('on est perdu', Data.mapId, " ALMANAX FAIT");
                }
            }
        }));
    });
}
exports.moveTo = moveTo;
