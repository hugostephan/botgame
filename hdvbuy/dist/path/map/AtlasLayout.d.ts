export declare class GraphicSizes {
    sx: number;
    sy: number;
    sw: number;
    sh: number;
}
export declare class AtlasLayout {
    width: number;
    height: number;
    graphicsPositions: Map<number, GraphicSizes>;
}
