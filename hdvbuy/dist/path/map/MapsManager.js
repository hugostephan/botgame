"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
const Cell_1 = require("./Cell");
const DTConstants_1 = require("./../../DTConstants");
const axios_1 = require("axios");
class MapsManager {
    static getMap(mapId) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.get(DTConstants_1.default.config.assetsUrl + "/maps/" + mapId + ".json");
            const data = response.data;
            return this.buildMap(data);
        });
    }
    static buildMap(json) {
        const map = new _1.default(json.id, json.topNeighbourId, json.bottomNeighbourId, json.leftNeighbourId, json.rightNeighbourId);
        for (const cell of json.cells) {
            map.cells.push(new Cell_1.default(cell.l, cell.f, cell.c, cell.s, cell.z));
        }
        for (const key in json.midgroundLayer) {
            if (json.midgroundLayer.hasOwnProperty(key)) {
                const values = json.midgroundLayer[key];
                for (let i = 0; i < values.length; i++) {
                    values[i] = Object.assign({ id: parseInt(key, 10) }, values[i]);
                }
                map.midgroundLayer.set(parseInt(key, 10), values);
            }
        }
        map.atlasLayout.width = json.atlasLayout.width;
        map.atlasLayout.height = json.atlasLayout.height;
        for (const key in json.atlasLayout.graphicsPositions) {
            if (json.atlasLayout.graphicsPositions.hasOwnProperty(key)) {
                const gs = json.atlasLayout.graphicsPositions[key];
                map.atlasLayout.graphicsPositions.set(parseInt(key, 10), gs);
            }
        }
        return map;
    }
}
exports.default = MapsManager;
