"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MapPoint {
    constructor(cellId, x, y) {
        this.cellId = cellId;
        this.x = x;
        this.y = y;
    }
    static Init() {
        this.cells = [];
        for (let i = 0; i < 560; i++) {
            const row = (i % 14) - ~~(i / 28);
            const x = row + 19;
            const y = row + ~~(i / 14);
            this.cells.push(new MapPoint(i, x, y));
        }
    }
    static fromCellId(cellId) {
        return this.cells.find(cell => cell.cellId === cellId);
    }
    static fromCoords(x, y) {
        const cell = this.cells.find(c => c.x === x && c.y === y);
        return cell === undefined ? null : cell;
    }
    static getNeighbourCells(cellId, allowDiagonal) {
        const coord = this.fromCellId(cellId);
        const neighbours = [];
        neighbours.push(this.fromCoords(coord.x, coord.y + 1));
        neighbours.push(this.fromCoords(coord.x - 1, coord.y));
        neighbours.push(this.fromCoords(coord.x, coord.y - 1));
        neighbours.push(this.fromCoords(coord.x + 1, coord.y));
        if (allowDiagonal) {
            neighbours.push(this.fromCoords(coord.x + 1, coord.y + 1));
            neighbours.push(this.fromCoords(coord.x - 1, coord.y + 1));
            neighbours.push(this.fromCoords(coord.x - 1, coord.y - 1));
            neighbours.push(this.fromCoords(coord.x + 1, coord.y - 1));
        }
        return neighbours.filter(n => n !== null);
    }
    distanceTo(destination) {
        return Math.round(Math.sqrt(Math.pow(this.x - destination.x, 2) +
            Math.pow(this.y - destination.y, 2)));
    }
    distanceToCell(destination) {
        return Math.abs(this.x - destination.x) + Math.abs(this.y - destination.y);
    }
}
exports.default = MapPoint;
