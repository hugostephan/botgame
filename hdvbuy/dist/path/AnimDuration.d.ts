export default class AnimDuration {
    linear: number;
    horizontal: number;
    vertical: number;
    constructor(linear: number, horizontal: number, vertical: number);
}
