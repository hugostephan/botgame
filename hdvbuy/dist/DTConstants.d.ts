import { IDofusTouchConfig } from './IDofusTouchConfig';
export default class DTConstants {
    static readonly MAIN_URL: string;
    static appVersion: string;
    static buildVersion: string;
    static assetsVersion: string;
    static staticDataVersion: string;
    static config: IDofusTouchConfig;
    static Init(): Promise<void>;
    static getConfig(): Promise<IDofusTouchConfig>;
    static getAssetsVersions(): Promise<any>;
    static getAppVersion(): Promise<string>;
    static getBuildVersion(): Promise<string>;
}
