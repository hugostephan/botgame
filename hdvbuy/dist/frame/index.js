"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("./../index");
const tool = require("../tool/tool");
const NetworkPhases_1 = require("../NetworkPhases");
const DTConstants_1 = require("../DTConstants");
class frame {
    constructor(dis, io) {
        this.dis = dis;
        this.io = io;
        this.register();
    }
    register() {
        // connection
        this.dis.dis.register('HelloConnectMessage', this.onHelloConnectMessage, this);
        this.dis.dis.register('assetsVersionChecked', this.onAssetsVersionChecked, this);
        this.dis.dis.register('ServersListMessage', this.onServersListMessage, this);
        this.dis.dis.register('SelectedServerDataMessage', this.onSelectedServerDataMessage, this);
        this.dis.dis.register('HelloGameMessage', this.onHelloGameMessage, this);
        this.dis.dis.register('AuthenticationTicketAcceptedMessage', this.onAuthenticationTicketAcceptedMessage, this);
        this.dis.dis.register('CharactersListMessage', this.onCharactersListMessage, this);
        this.dis.dis.register('CharacterSelectedSuccessMessage', this.onCharacterSelectedSuccessMessage, this);
        this.dis.dis.register('IdentificationSuccessMessage', this.onIdentificationSuccessMessage, this);
        this.dis.dis.register('SequenceNumberRequestMessage', this.onSequenceNumberRequestMessage, this);
        this.dis.dis.register('BasicLatencyStatsRequestMessage', this.onBasicLatencyStatsRequestMessage, this);
        this.dis.dis.register('CurrentMapMessage', this.onCurrentMapMessage, this);
        this.dis.dis.register('CharacterCreationResultMessage', this.onCharacterCreationResultMessage, this);
        // map path
        this.dis.dis.register('TextInformationMessage', this.onTextInformationMessage, this);
        this.dis.dis.register('MapComplementaryInformationsDataMessage', this.onMapComplementaryInformationsDataMessage, this);
        this.dis.dis.register('GameRolePlayShowActorMessage', this.onGameRolePlayShowActorMessage, this);
        // marchand
        this.dis.dis.register('ExchangeStartOkHumanVendorMessage', this.onExchangeStartOkHumanVendorMessage, this);
        // invetory
        this.dis.dis.register('InventoryContentMessage', this.onInventoryContentMessage, this);
        // chat
        this.dis.dis.register('ChatServerMessage', this.onChatServerMessage, this);
    }
    onExchangeStartOkHumanVendorMessage(data) {
        return __awaiter(this, void 0, void 0, function* () {
            for (var i of data['objectsInfos']) {
                if (i.objectPrice === 1) {
                    this.dis.marchandObjectId = i.objectUID;
                }
                else {
                    this.dis.marchandObjectId2 = i.objectUID;
                }
            }
            this.dis.marchandObjectId;
        });
    }
    onMapComplementaryInformationsDataMessage(data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.dis.alreadyOnMap) {
                console.log('----------------------------------------');
                this.dis.alreadyOnMap = true;
                const player = data.actors.find(a => a.contextualId === this.dis.characterId);
                this.dis.currentCell = player.disposition.cellId;
                yield this.dis.Movement.setMap(data.mapId);
                yield tool.wait(1000);
                //moveTo(this.dis)	
                this.dis.network.close();
                new index_1.default(this.dis.username, this.dis.password, this.dis.serverId, this.dis.username);
            }
            else {
                this.dis.alreadyOnMap = false;
            }
        });
    }
    onCharactersListMessage(data) {
        console.log('character list message');
        this.dis.characterId = data.characters[this.dis.numberCamp].id;
        this.dis.network.sendMessage("CharacterFirstSelectionMessage", {
            doTutorial: true,
            id: this.dis.characterId
        });
    }
    //chat
    onChatServerMessage(data) {
        if (data.channel === 9) {
            try {
                this.io.emit('chat message', this.dis.characterId, data.senderName, data.content);
                console.log("RECU:" + data.senderName + ": " + data.content);
            }
            catch (e) {
                console.log(e);
            }
        }
        else {
            //console.log(data)
        }
    }
    //map path
    onGameRolePlayShowActorMessage(data) {
        //console.log("FLOOD " + data.informations.name + " level : " , data.informations.alignmentInfos.characterPower - data.informations.contextualId)
        //console.log(data.informations.name);
    }
    // connection 
    onGameActionFightNoSpellCastMessage(data) {
        //console.log(data, "fight action fight")
    }
    onCharacterCreationResultMessage(data) {
        
        //console.log(data, "eeeeeeeeeeeeeeeeeeeeeeeeeeeeee")
    }
    onInventoryContentMessage(data) {
        for (var i of data['objects']) {
            this.dis.tutoInventory.push(i);
        }
    }
    onTextInformationMessage(data) {
        let text = data.text;
        for (let i = 0; i < data.parameters.length; i++) {
            text = text.replace(`$%${i}`, data.parameters[i]);
        }
        // text.indexOf('de kamas pour acheter cet objet') === -1 &&  text.indexOf('disponible à ce prix') === -1 && text.indexOf('item') === -1
        if (text.indexOf('avez perdu') === -1 && text.indexOf('ur va être') === -1 && text.indexOf('mise à') === -1 && text.indexOf('il est int') === -1 && text.indexOf('mémoriser') === -1 && text.indexOf('lle quê') === -1 && text.indexOf('terminée') === -1 && text.indexOf('sauvegardée') === -1) {
            console.log(text);
        }
    }
    onCurrentMapMessage(data) {
        this.dis.mapId = data.mapId;
        this.dis.network.sendMessage("MapInformationsRequestMessage", {
            mapId: data.mapId
        });
    }
    // connection
    onBasicLatencyStatsRequestMessage(data) {
        this.dis.network.sendMessage("BasicLatencyStatsMessage", {
            latency: 262,
            max: 50,
            sampleCount: 12
        });
    }
    onSequenceNumberRequestMessage(data) {
        this.dis.sequence++;
        this.dis.network.sendMessage("SequenceNumberMessage", {
            number: this.dis.sequence
        });
    }
    onIdentificationSuccessMessage(data) {
        this.dis.login = data.login;
        this.dis.subscriber = new Date() < data.subscriptionEndDate;
    }
    onCharacterSelectedSuccessMessage(data) {
        this.dis.network.sendMessage("kpiStartSession", {
            accountSessionId: this.dis.login,
            isSubscriber: this.dis.subscriber
        });
        this.dis.network.send("moneyGoultinesAmountRequest");
        this.dis.network.sendMessage("QuestListRequestMessage");
        this.dis.network.sendMessage("FriendsGetListMessage");
        this.dis.network.sendMessage("IgnoredGetListMessage");
        this.dis.network.sendMessage("SpouseGetInformationsMessage");
        this.dis.network.send("bakSoftToHardCurrentRateRequest");
        this.dis.network.send("bakHardToSoftCurrentRateRequest");
        this.dis.network.send("restoreMysteryBox");
        this.dis.network.sendMessage("ClientKeyMessage", {
            key: tool.randomString(21)
        });
        this.dis.network.sendMessage("GameContextCreateRequestMessage");
    }
    onAuthenticationTicketAcceptedMessage(data) {
        this.dis.network.sendMessage('CharactersListRequestMessage');
    }
    onHelloGameMessage(data) {
        this.dis.network.sendMessage("AuthenticationTicketMessage", {
            lang: "fr",
            ticket: this.dis.ticket
        });
        this.dis.network.phase = NetworkPhases_1.NetworkPhases.GAME;
    }
    onSelectedServerDataMessage(data) {
        this.dis.ticket = data.ticket;
        this.dis.network.switchToGameServer(data._access, {
            address: data.address,
            id: data.serverId,
            port: data.port
        });
    }
    onServersListMessage(data) {
        this.dis.network.sendMessage('ServerSelectionMessage', {
            serverId: this.dis.serverId
        });
    }
    onHelloConnectMessage(data) {
        this.dis.network.phase = NetworkPhases_1.NetworkPhases.LOGIN;
        this.dis.key = data.key;
        this.dis.salt = data.salt;
        this.dis.network.send('checkAssetsVersion', {
            assetsVersion: DTConstants_1.default.assetsVersion,
            staticDataVersion: DTConstants_1.default.staticDataVersion,
        });
    }
    onAssetsVersionChecked(data) {
        this.dis.network.send('login', {
            key: this.dis.key,
            salt: this.dis.salt,
            token: this.dis.haapi.token,
            username: this.dis.username,
        });
    }
}
exports.default = frame;
