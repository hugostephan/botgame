"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NetworkPhases;
(function (NetworkPhases) {
    NetworkPhases[NetworkPhases["NONE"] = 0] = "NONE";
    NetworkPhases[NetworkPhases["LOGIN"] = 1] = "LOGIN";
    NetworkPhases[NetworkPhases["SWITCHING_TO_GAME"] = 2] = "SWITCHING_TO_GAME";
    NetworkPhases[NetworkPhases["GAME"] = 3] = "GAME";
})(NetworkPhases = exports.NetworkPhases || (exports.NetworkPhases = {}));
