import Dispatcher from './Dispatcher';
import HaapiConnection from './HaapiConnection';
import Network from './Network';
import movement from './movement';
import DTConstants from './DTConstants';

export default class data {
  public haapi: HaapiConnection;
  public network: Network;
  public dis: Dispatcher;

  public Movement = new movement();

  public key: number[];
  public salt: string;
  public ticket: string;
  public login: string;

  public username: string;
  public password: string;


  public sequence = 0;
  public subscriber = false;
  public createdCamp = false;
  public characterId = 0;
  public currentCell = 0;
  public serverId = 403
  public mapId = 0;
  public createNew = true; // si il est a false on en créé pas
  public blackList = [];
  public tutoGroupe = -4618;
  public tutoInventory = [];
  public almanax = false;

  public itemId = 0;
  public canScrap = true;
  public canScrap2 = true;
  public allId = {};
  public weCanBuy = true;
  public alreadyHDV = true;
  public inventoryHDV = {};
  public crafted = false;
  public idCraft = 100;
  public alreadyOnMap = false;

  public almaQuestId = 1053 //
  public almaVieux = [13557, 13741, 15661, 15659] //
  public almaBonomeCell = -7 //
  public almaBonome = [15751, 15750, 15749, 15748] //

  public marchandQuantity = 2 //
  public marchandObjectPrice = 1 //
  public marchandCharacterId = 1609903 ///
  public marchandCharacterCell = 192 ///


  public marchandObjectId = 0
  public marchandObjectId2 = 0
  public amaknaUnderground = false
  public astrubRockyInlet = false
  public astrubUnderground = false
  public brakmarUnderground = false
  public numberCamp = 0
  public started = true
  public createdCharacter = false

  public level = 1;

  public phaseGame = 0;

  public reco = false;

  public connectionframe = false

  public statu = false

  public async init(username, password, serverId, numberCamp) {
    
    this.haapi = new HaapiConnection();
    this.username = username;
    this.password = password;
    this.serverId = serverId;
    this.numberCamp = numberCamp
    //console.log('ICI PARIS', this, numberCamp)
    await this.haapi.processHaapi(username, password, serverId);
    //console.log("apres le process api")
     this.dis = new Dispatcher();
     //console.log("apres dispatcher")
    this.network = new Network(this.dis, username);
    //console.log("apres le network")
    this.network.connect(DTConstants.config.sessionId, DTConstants.config.dataUrl);
    //console.log("apres le connect")
  } 
}

