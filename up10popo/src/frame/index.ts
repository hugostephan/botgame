import data from './../data';
import inde from './../index';
var fs = require('fs')
import * as tool from '../tool/tool';
import { NetworkPhases } from '../NetworkPhases';
import DTConstants from '../DTConstants';

import { makeTheTuto } from '../tuto';
import { backStartMap } from '../backStartMap';
import { moveToZaap } from '../moveToZaap';
import mapData from "../path/map";
var md5 = require('md5');
const shell = require('shelljs');
export default class frame {

	constructor(
		public dis: data,
		public io: any
	) {
		this.register()
	}
	private register(){
		// connection
		this.dis.dis.register('HelloConnectMessage', this.onHelloConnectMessage, this);
		this.dis.dis.register('assetsVersionChecked', this.onAssetsVersionChecked, this);
		this.dis.dis.register('ServersListMessage', this.onServersListMessage, this);
		this.dis.dis.register('SelectedServerDataMessage', this.onSelectedServerDataMessage, this);
		this.dis.dis.register('HelloGameMessage', this.onHelloGameMessage, this);
		this.dis.dis.register('AuthenticationTicketAcceptedMessage', this.onAuthenticationTicketAcceptedMessage, this)
		this.dis.dis.register('CharactersListMessage', this.onCharactersListMessage, this)
		this.dis.dis.register('CharacterSelectedSuccessMessage', this.onCharacterSelectedSuccessMessage, this)
		this.dis.dis.register('IdentificationSuccessMessage', this.onIdentificationSuccessMessage, this)
		this.dis.dis.register('SequenceNumberRequestMessage', this.onSequenceNumberRequestMessage, this)
		this.dis.dis.register('BasicLatencyStatsRequestMessage', this.onBasicLatencyStatsRequestMessage, this)
		this.dis.dis.register('CurrentMapMessage', this.onCurrentMapMessage, this)
		this.dis.dis.register('CharacterCreationResultMessage', this.onCharacterCreationResultMessage, this)
		this.dis.dis.register('CharacterDeletionErrorMessage', this.onCharacterDeletionErrorMessage, this)
		
		// map path
		this.dis.dis.register('TextInformationMessage', this.onTextInformationMessage, this)
		this.dis.dis.register('MapComplementaryInformationsDataMessage', this.onMapComplementaryInformationsDataMessage, this)
		this.dis.dis.register('GameRolePlayShowActorMessage', this.onGameRolePlayShowActorMessage, this)

		// invetory
		this.dis.dis.register('InventoryContentMessage', this.onInventoryContentMessage, this)

		
		// marchand
		this.dis.dis.register('ExchangeStartOkHumanVendorMessage', this.onExchangeStartOkHumanVendorMessage, this)

		//level
		this.dis.dis.register('CharacterLevelUpInformationMessage', this.onCharacterLevelUpInformationMessage, this)

		this.dis.dis.register('ObjectAddedMessage', this.onObjectAddedMessage, this)

		// hdv
		this.dis.dis.register('ExchangeTypesItemsExchangerDescriptionForUserMessage', this.onExchangeTypesItemsExchangerDescriptionForUserMessage, this)

		// serveur pas fr
		this.dis.dis.register('SelectedServerRefusedMessage', this.onSelectedServerRefusedMessage, this)


		//
		this.dis.dis.register('AchievementFinishedMessage', this.onAchievementFinishedMessage, this)
	}

	private onAchievementFinishedMessage(data: any) {
		this.dis.network.sendMessage("AchievementRewardRequestMessage", {
	      	achievementId: -1
	    });
	}

	private async onExchangeTypesItemsExchangerDescriptionForUserMessage(data: any) {
		console.log(data['itemTypeDescriptions'][0]['objectUID'], data['itemTypeDescriptions'][0]['prices'][0])
		this.dis.network.sendMessage("ExchangeBidHouseBuyMessage", {
			uid: data['itemTypeDescriptions'][0]['objectUID'], qty: 1, price: data['itemTypeDescriptions'][0]['prices'][0]
		});
		this.dis.network.sendMessage("LeaveDialogRequestMessage");

		//shell.exec("cd /lib/dev/up10popo && npm start " + this.dis.username)
		
	}// 144416
	private async onObjectAddedMessage(data: any) {	
		console.log(data['object']['objectGID'])
		if (data['object']['objectGID'] === 548) {
			this.dis.network.sendMessage("ObjectUseMessage", {
				objectUID: data['object']['objectUID']
			});
			this.dis.network.sendMessage("MapInformationsRequestMessage", {
				mapId: 84804104
			})
			//shell.exec("cd /lib/dev/up10popo && npm start " + this.dis.username)
		}
	}

	private async onCharacterLevelUpInformationMessage(data: any) {
		this.dis.level = data.newLevel
		/*
		var mapUp = 144415
		this.dis.level = data.newLevel
		if (this.dis.mapId === 144415) {
			mapUp = 144416
		}
		if (data.newLevel === 10) {
			this.dis.started = false
			console.log(data, this.dis.mapId)

			
		}
		*/
	}

	private async onExchangeStartOkHumanVendorMessage(data: any) {
		for (var i of data['objectsInfos']){
			if (i.objectPrice === this.dis.marchandObjectPrice ) {
				this.dis.marchandObjectId = i.objectUID
			} else {
				this.dis.marchandObjectId2 = i.objectUID
			}
		}
		this.dis.marchandObjectId
	}

	private async onMapComplementaryInformationsDataMessage(data: any) {	
		if (this.dis.level >= 10 && data.mapId !== 84804104) {
			console.log(this.dis.level, data.mapId)
			this.dis.network.sendMessage("NpcGenericActionRequestMessage", {
	        	npcId: 0, npcActionId: 6, npcMapId: data.mapId
	        });
	        this.dis.network.sendMessage("ExchangeBidHouseTypeMessage", {
	        	type: 12
	        });
	        this.dis.network.sendMessage("ExchangeBidHouseListMessage", {
	        	id: 548
	        });
		} else if (this.dis.level >= 10 && data.mapId === 84804104) {
			shell.exec("cd /lib/dev/up10popo && npm start " + this.dis.username)
		} else {
			if (this.dis.started === true) {
				await tool.wait(80);
				this.dis.alreadyOnMap = true;
				const player = data.actors.find(a => a.contextualId === this.dis.characterId);
				this.dis.currentCell = player.disposition.cellId;
				await this.dis.Movement.setMap(data.mapId);
				for (var i of data.actors) {
					if (i._type === 'GameRolePlayGroupMonsterInformations') {
						this.dis.tutoGroupe = i.contextualId
					}
				}
				if (this.dis.createNew) { // make the tuto !
					console.log("CREATE NEW OKAY");
					this.dis.createNew = false
					await moveToZaap(this.dis)							
				} else {
					//console.log("ON VA DANS LE TRUCK", this.dis.mapId);
					
					await moveToZaap(this.dis)			
				}	
			}	
		}
	}

	private onCharactersListMessage(data: any) {
    	this.dis.network.username = this.dis.username
		if (data.characters.length > 0) {
			this.dis.characterId = data.characters[0].id
		}
		console.log(data.characters.length)
		//if (data.characters.length === 4  && this.dis.createdCharacter === false && data.characters[3].level < 10) {
		if (this.dis.reco === true) {
			this.dis.network.sendMessage("CharacterFirstSelectionMessage", {
				doTutorial: false, // false bypass le tuto meme si c'est le premier ? 
				id: this.dis.characterId
			});
		} else if (data.characters.length === 5 && this.dis.createdCharacter === false && data.characters[0].level === 1 ) {
			this.dis.network.sendMessage("CharacterFirstSelectionMessage", {
				doTutorial: false, // false bypass le tuto meme si c'est le premier ? 
				id: this.dis.characterId
			});
			/*console.log("Suppression du personnage " + data.characters[3].name + " - niveau " + data.characters[3].level + " - id " + data.characters[3].id)
			console.log(md5(data.characters[3].id + "~000000000000000000"))
			this.dis.network.sendMessage("CharacterDeletionRequestMessage", {
				characterId: data.characters[3].id,
				secretAnswerHash: md5(data.characters[3].id + "~000000000000000000")
			});
			reconnect()
			*/
		} else if (data.characters.length < 5 && this.dis.createNew === true){			
			if (this.dis.createdCharacter === false) { // si premiere connection on créé le perso
				let name = tool.capitalize(tool.readableString(6)) + "-" + tool.capitalize(tool.readableString(6));
				console.log("nouveau perso nom : ", name, "sur le compte: ", this.dis.username, "sur le serveur: ", this.dis.serverId)
				this.dis.network.sendMessage('CharacterCreationRequestMessage', {
					breed: 11,
					colors: [33542780, 50307866, 60141352, 72773666, 100657218],
					cosmeticId: 162,
					name: name,
					sex: false,
				});
				this.dis.createdCharacter = true
			} else { // si deuxieme conection connection connection sur le dernier perso créé
				this.dis.network.sendMessage("CharacterFirstSelectionMessage", {
					doTutorial: false, // false bypass le tuto meme si c'est le premier ? 
					id: this.dis.characterId
				});
				console.log('ON CHANGE DE COMPTE')
				console.log('ON CHANGE DE COMPTE')
				console.log('ON CHANGE DE COMPTE')
			}
		} else if (data.characters.length === 5 && this.dis.createdCharacter === true) {
			this.dis.createdCharacter = false
			console.log('DERNIER PERSO A MONTER')
			this.dis.network.sendMessage("CharacterFirstSelectionMessage", {
				doTutorial: false, // false bypass le tuto meme si c'est le premier ? 
				id: this.dis.characterId
			});
			this.dis.createdCharacter = false
		} else {
			function checkL(username) { 
				fs.readFile('/lib/dev/up10popo/do.txt', 'utf8', function(err, data) {
				    data = data.replace(username + '\n', '')
				    fs.writeFile('/lib/dev/up10popo/do.txt', data, 'utf8', function(err) {
				      	if (err) { return console.log(err); }
				      	fs.appendFile('/lib/dev/up10popo/finish.txt', username + "\n", function (err) {
					  		if (err) throw err;
					  		console.log('Saved!');
					  		shell.exec("screen -X -S " + username + " quit")
						});
				    });
				});				
			}
			checkL(this.dis.username);
			this.dis.network.crashed = 5
			this.dis.network.close()
			//shell.exec("screen -X -S " + this.dis.username + " quit")

			console.log('ON CHANGE DE COMPTE')
			console.log('ON CHANGE DE COMPTE')
			console.log('ON CHANGE DE COMPTE')
			console.log('ON CHANGE DE COMPTE')
			console.log('ON CHANGE DE COMPTE')
			console.log('ON CHANGE DE COMPTE')
		}
	}

	//map path
	private async onGameRolePlayShowActorMessage(data: any) { // new player on map
		if (data.informations.name === "Acamir-Epynyw") {
			this.dis.network.sendMessage("AchievementRewardRequestMessage", {
		      achievementId: -1
		    });
		    this.dis.network.sendMessage("ExchangeOnHumanVendorRequestMessage", {
		      humanVendorId: this.dis.marchandCharacterId, humanVendorCell: this.dis.marchandCharacterCell
		    });
		    await tool.wait(1500)
		    this.dis.network.sendMessage("ExchangeBuyMessage", {
		      objectToBuyId: this.dis.marchandObjectId, quantity: this.dis.marchandQuantity
		    });

		    this.dis.network.sendMessage("LeaveDialogRequestMessage");
		    
		    var path = this.dis.Movement.moveToCell(this.dis.currentCell, 552);
		    this.dis.network.sendMessage("GameMapMovementRequestMessage", {
		      keyMovements: path,
		      mapId: this.dis.mapId
		    });
		    this.dis.network.sendMessage("GameMapMovementConfirmMessage");
		    this.dis.network.sendMessage("ChangeMapMessage", {
		      mapId: 84806915
		    });
		    this.dis.network.sendMessage("MapInformationsRequestMessage", {
		      mapId: this.dis.mapId
		    });
		}
		//console.log("FLOOD " + data.informations.name + " level : " , data.informations.alignmentInfos.characterPower - data.informations.contextualId)
		//console.log(data.informations.name);
	}

	// connection 
	private onGameActionFightNoSpellCastMessage(data: any) {
		//console.log(data, "fight action fight")
	}
	private onCharacterCreationResultMessage(data: any) {
		if (data.result === 0){
			console.log("Personnage créé avec succès.")
		} else {
			console.log("Impossible de créer le personnage : " , data)
			this.dis.network.crashed = 5
			//this.dis.network.close()
			shell.exec("cd /lib/dev/up10popo && npm start " + this.dis.username)
		  }
	}

	private async onSelectedServerRefusedMessage(data: any) {
		if (data.error === 3) {
			console.log("COMPTE PAS FR")
			function clear(username) {
				fs.readFile('/lib/dev/up10popo/do.txt', 'utf8', function(err, data) {
				    data = data.replace(username + '\n', '')
				    fs.writeFile('/lib/dev/up10popo/do.txt', data, 'utf8', function(err) {
				      	if (err) { return console.log(err); }
				      	
				    });
				});
			}
			clear(this.dis.username)
			shell.exec("screen -X -S " + this.dis.username + " quit")
		} else {
			console.log("ERROR PAS  PEUT ETRE SAVE DU SERV")
			await tool.wait(60000)
			this.dis.network.close()
		}
	}

	private onCharacterDeletionErrorMessage(data: any) {
		if (data.reason === 0){
			console.log("Personnage supprimé avec succès.")
		}
		else {
			console.log("Impossible de supprimer le personnage : " + data.reason)
		  }
	}

	private onInventoryContentMessage(data: any) {
		for (var i of data['objects']) {
			this.dis.tutoInventory.push(i)
		}
	}

	private onTextInformationMessage(data: any) {
		let text: string = data.text;
		for (let i = 0; i < data.parameters.length; i++) {
			text = text.replace(`$%${i}`, data.parameters[i]);
		}
		// text.indexOf('de kamas pour acheter cet objet') === -1 &&  text.indexOf('disponible à ce prix') === -1 && text.indexOf('item') === -1
		if (text.indexOf('avez perdu') === -1 && text.indexOf('ur va être') === -1 && text.indexOf('mise à') === -1 && text.indexOf('il est int') === -1 && text.indexOf('mémoriser') === -1 && text.indexOf('lle quê') === -1 && text.indexOf('terminée') === -1 && text.indexOf('sauvegardée') === -1) {
			console.log(text);
		}
		if (text.indexOf("osez pas d'assez de kamas pou") !== -1) {
			console.log("CATCH SEND RACHAT")
			this.dis.network.close()
		}
		if (text.indexOf("été plus rapide.") !== -1) {
			console.log("CATCH SEND RACHAT")
			this.dis.network.close()
		}
	}

	private onCurrentMapMessage(data: any) {
		this.dis.mapId = data.mapId
		this.dis.network.sendMessage("MapInformationsRequestMessage", {
			mapId: data.mapId
		});
	}

	
	// connection


	private onBasicLatencyStatsRequestMessage(data: any) {
		this.dis.network.sendMessage("BasicLatencyStatsMessage", {
			latency: 262,
			max: 50,
			sampleCount: 12
		});
	}

	private onSequenceNumberRequestMessage(data: any) {
		this.dis.sequence++;
		this.dis.network.sendMessage("SequenceNumberMessage", {
			number: this.dis.sequence
		});
	}


	private onIdentificationSuccessMessage(data: any) {
		this.dis.login = data.login
		this.dis.subscriber = new Date() < data.subscriptionEndDate;
		setTimeout(() => {
			if (this.dis.connectionframe === false ) {
				
				console.log("RETRY idenfitication")
				this.dis.network.close()
				//shell.exec("cd /lib/dev/up10fork && npm start " + this.dis.username + " d")
			}
		}, 1000)
	}

	private onCharacterSelectedSuccessMessage(data: any) {
		this.dis.level = data['infos'].level
		this.dis.network.sendMessage("kpiStartSession", {
			accountSessionId: this.dis.login,
			isSubscriber: this.dis.subscriber
		});
		this.dis.network.send("moneyGoultinesAmountRequest");
		this.dis.network.sendMessage("QuestListRequestMessage");
		this.dis.network.sendMessage("FriendsGetListMessage");
		this.dis.network.sendMessage("IgnoredGetListMessage");
		this.dis.network.sendMessage("SpouseGetInformationsMessage");
		this.dis.network.send("bakSoftToHardCurrentRateRequest");
		this.dis.network.send("bakHardToSoftCurrentRateRequest");
		this.dis.network.send("restoreMysteryBox");
		this.dis.network.sendMessage("ClientKeyMessage", {
			key: tool.randomString(21)
		});
		this.dis.network.sendMessage("GameContextCreateRequestMessage");
	}

	

	private onAuthenticationTicketAcceptedMessage(data: any) {
		this.dis.network.sendMessage('CharactersListRequestMessage');
	}

	private onHelloGameMessage(data: any) {
		this.dis.network.sendMessage("AuthenticationTicketMessage", {
			lang: "fr",
			ticket: this.dis.ticket
		});
		this.dis.network.phase = NetworkPhases.GAME;
	}

	private onSelectedServerDataMessage(data: any) {
		this.dis.connectionframe = true
		this.dis.ticket = data.ticket
		this.dis.network.switchToGameServer(data._access, {
			address: data.address,
			id: data.serverId,
			port: data.port
		});
		//console.log('on switch')
	}

	private onServersListMessage(data: any) {
		this.dis.network.sendMessage('ServerSelectionMessage', {
			serverId: this.dis.serverId
		});
		setTimeout(() => {
			if (this.dis.connectionframe === false ) {
				
				console.log("RETRY FRAME")
				this.dis.network.close()
				//shell.exec("cd /lib/dev/up10fork && npm start " + this.dis.username + " d")
			}
		}, 1000)
	}

	private onHelloConnectMessage(data: any) {
		this.dis.network.phase = NetworkPhases.LOGIN;
		this.dis.key = data.key;
		this.dis.salt = data.salt;

		this.dis.network.send('checkAssetsVersion', {
			assetsVersion: DTConstants.assetsVersion,
			staticDataVersion: DTConstants.staticDataVersion,
		});
	}
	private onAssetsVersionChecked(data: any) {
		this.dis.network.send('login', {
			key: this.dis.key,
			salt: this.dis.salt,
			token: this.dis.haapi.token,
			username: this.dis.username,
		});
		console.log('login envoyer')
	}

}
