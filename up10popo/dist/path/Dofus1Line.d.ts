import Map from "./map";
export default class Dofus1Line {
    static isLineObstructed(map: Map, sourceCellId: number, targetCellId: number, occupiedCells: number[], diagonal?: boolean): boolean;
    private static isCellObstructed;
}
