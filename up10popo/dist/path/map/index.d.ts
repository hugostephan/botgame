import { AtlasLayout } from "./AtlasLayout";
import Cell from "./Cell";
import GraphicalElement from "./GraphicalElement";
export default class {
    id: number;
    topNeighbourId: number;
    bottomNeighbourId: number;
    leftNeighbourId: number;
    rightNeighbourId: number;
    shadowBonusOnEntities: number;
    cells: Cell[];
    midgroundLayer: Map<number, GraphicalElement[]>;
    atlasLayout: AtlasLayout;
    constructor(id: number, topNeighbourId: number, bottomNeighbourId: number, leftNeighbourId: number, rightNeighbourId: number);
}
