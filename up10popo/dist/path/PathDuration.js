"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AnimDuration_1 = require("./AnimDuration");
const MapPoint_1 = require("./MapPoint");
var AnimDurationTypes;
(function (AnimDurationTypes) {
    AnimDurationTypes[AnimDurationTypes["MOUNTED"] = 0] = "MOUNTED";
    AnimDurationTypes[AnimDurationTypes["PARABLE"] = 1] = "PARABLE";
    AnimDurationTypes[AnimDurationTypes["RUNNING"] = 2] = "RUNNING";
    AnimDurationTypes[AnimDurationTypes["WALKING"] = 3] = "WALKING";
    AnimDurationTypes[AnimDurationTypes["SLIDE"] = 4] = "SLIDE";
})(AnimDurationTypes || (AnimDurationTypes = {}));
class PathDuration {
    static calculate(path, isFight = false, slide = false, riding = false) {
        let duration = 20; // TODO: Adding 20ms just in case, need tests, to see if its gonna cause problems
        if (path.length === 1) {
            return duration;
        }
        let motionScheme;
        if (slide) {
            motionScheme = this.animDurations.get(AnimDurationTypes.SLIDE);
        }
        else if (riding) {
            motionScheme = this.animDurations.get(AnimDurationTypes.MOUNTED);
        }
        else {
            motionScheme =
                path.length > 3
                    ? this.animDurations.get(AnimDurationTypes.RUNNING)
                    : this.animDurations.get(AnimDurationTypes.WALKING);
        }
        let prevX = -1;
        let prevY = -1;
        for (let i = 0; i < path.length; i++) {
            const coord = MapPoint_1.default.fromCellId(path[i]);
            if (i !== 0) {
                if (coord.y === prevY) {
                    duration += motionScheme.horizontal;
                }
                else if (coord.x === prevX) {
                    duration += motionScheme.vertical;
                }
                else {
                    duration += motionScheme.linear;
                }
            }
            prevX = coord.x;
            prevY = coord.y;
        }
        console.log("[PathDuration] The duration for your movement is: " + duration + " ms.");
        return duration;
    }
}
PathDuration.animDurations = new Map([
    [AnimDurationTypes.MOUNTED, new AnimDuration_1.default(135, 200, 120)],
    [AnimDurationTypes.PARABLE, new AnimDuration_1.default(400, 500, 450)],
    [AnimDurationTypes.RUNNING, new AnimDuration_1.default(170, 255, 150)],
    [AnimDurationTypes.WALKING, new AnimDuration_1.default(480, 510, 425)],
    [AnimDurationTypes.SLIDE, new AnimDuration_1.default(57, 85, 50)]
]);
exports.default = PathDuration;
