import PathFinder from '../path';
import { MovementRequestResults } from './enums';
export default class movement {
    pathFinder: PathFinder;
    setMap(mapId: number): Promise<void>;
    moveToCell(current: number, cellId: number, stopNextToTarget?: boolean): number[] | MovementRequestResults.FAILED;
}
