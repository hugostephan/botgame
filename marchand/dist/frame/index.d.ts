import data from './../data';
export default class frame {
    dis: data;
    io: any;
    constructor(dis: data, io: any);
    private register;
    private onExchangeStartOkHumanVendorMessage;
    private onMapComplementaryInformationsDataMessage;
    private onCharactersListMessage;
    private onChatServerMessage;
    private onGameRolePlayShowActorMessage;
    private onGameActionFightNoSpellCastMessage;
    private onCharacterCreationResultMessage;
    private onInventoryContentMessage;
    private onTextInformationMessage;
    private onCurrentMapMessage;
    private onBasicLatencyStatsRequestMessage;
    private onSequenceNumberRequestMessage;
    private onIdentificationSuccessMessage;
    private onCharacterSelectedSuccessMessage;
    private onAuthenticationTicketAcceptedMessage;
    private onHelloGameMessage;
    private onSelectedServerDataMessage;
    private onServersListMessage;
    private onHelloConnectMessage;
    private onAssetsVersionChecked;
}
