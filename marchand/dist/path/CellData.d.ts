import CellPath from "./CellPath";
export default class CellData {
    i: number;
    j: number;
    floor: number;
    zone: number;
    speed: number;
    weight: number;
    candidateRef: CellPath;
    constructor(i: number, j: number);
}
