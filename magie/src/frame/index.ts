import data from './../data';
import inde from './../index';
var fs = require('fs')
import * as tool from '../tool/tool';
import { NetworkPhases } from '../NetworkPhases';
import DTConstants from '../DTConstants';

import { makeTheTuto } from '../tuto';
import { backStartMap } from '../backStartMap';
import { moveToZaap } from '../moveToZaap';
import mapData from "../path/map";
var md5 = require('md5');
const shell = require('shelljs');
export default class frame {

	constructor(
		public dis: data,
		public io: any
	) {
		this.register()
	}
	private register(){
		// connection
		this.dis.dis.register('HelloConnectMessage', this.onHelloConnectMessage, this);
		this.dis.dis.register('assetsVersionChecked', this.onAssetsVersionChecked, this);
		this.dis.dis.register('ServersListMessage', this.onServersListMessage, this);
		this.dis.dis.register('SelectedServerDataMessage', this.onSelectedServerDataMessage, this);
		this.dis.dis.register('HelloGameMessage', this.onHelloGameMessage, this);
		this.dis.dis.register('AuthenticationTicketAcceptedMessage', this.onAuthenticationTicketAcceptedMessage, this)
		this.dis.dis.register('CharactersListMessage', this.onCharactersListMessage, this)
		this.dis.dis.register('CharacterSelectedSuccessMessage', this.onCharacterSelectedSuccessMessage, this)
		this.dis.dis.register('IdentificationSuccessMessage', this.onIdentificationSuccessMessage, this)
		this.dis.dis.register('SequenceNumberRequestMessage', this.onSequenceNumberRequestMessage, this)
		this.dis.dis.register('BasicLatencyStatsRequestMessage', this.onBasicLatencyStatsRequestMessage, this)
		this.dis.dis.register('CurrentMapMessage', this.onCurrentMapMessage, this)
		this.dis.dis.register('CharacterCreationResultMessage', this.onCharacterCreationResultMessage, this)
		this.dis.dis.register('CharacterDeletionErrorMessage', this.onCharacterDeletionErrorMessage, this)
		
		// map path
		this.dis.dis.register('TextInformationMessage', this.onTextInformationMessage, this)
		this.dis.dis.register('MapComplementaryInformationsDataMessage', this.onMapComplementaryInformationsDataMessage, this)
		this.dis.dis.register('GameRolePlayShowActorMessage', this.onGameRolePlayShowActorMessage, this)

		// invetory
		this.dis.dis.register('InventoryContentMessage', this.onInventoryContentMessage, this)

		
		// marchand
		this.dis.dis.register('ExchangeStartOkHumanVendorMessage', this.onExchangeStartOkHumanVendorMessage, this)

		//level
		this.dis.dis.register('CharacterLevelUpInformationMessage', this.onCharacterLevelUpInformationMessage, this)

		//quest

		// magie
		this.dis.dis.register('ExchangeCraftResultMagicWithObjectDescMessage', this.onExchangeCraftResultMagicWithObjectDescMessage, this)
		this.dis.dis.register('ExchangeObjectRemovedMessage', this.onExchangeObjectRemovedMessage, this)
		this.dis.dis.register('ExchangeObjectAddedMessage', this.onExchangeObjectAddedMessage, this)


	}





	public async onExchangeObjectRemovedMessage(data: any) {
		//console.log('REMOVE ', data.objectUID)
	}

	public async onExchangeObjectAddedMessage(data: any) {
		//console.log('ADDED ', data.object.objectUID)
	}


	public getDiff(arr: any) {
		//console.log(arr)
		var index = 0
		for (var i of arr) {
			//console.log("VALEUR ", i)
			index = 0
			for (var j of this.dis.all) {
				if (i.actionId === j.id ) {
					
					if (i.value < j.value) {
						console.log('Break ', j.name , j.value - i.value)
						index = j.value - i.value
					}
				}
				index++
				j.value = i.value
			}
		}
		return index
	}

	private async fm(index: any) {
		//console.log("AVANT FM ", this.dis.all[index].name, this.dis.all[index].uid, this.dis.actuel, index)	

		if (this.dis.actuel !== this.dis.all[index].uid) {
			this.dis.network.sendMessage("ExchangeObjectMoveMessage", {
				objectUID: this.dis.actuel,
				quantity: this.dis.actuelQ * -1
			});
			this.dis.step++;

			this.dis.network.sendMessage("ExchangeObjectMoveMessage", {
				objectUID: this.dis.all[index].uid,
				quantity: this.dis.all[index].q
			});
			this.dis.step++;
			this.dis.actuel = this.dis.all[index].uid;
			this.dis.actuelQ = this.dis.all[index].q
			//console.log("CHANGE RUNE")
		}
		console.log("on monte ", this.dis.all[index].name)
		



		//console.log("APRES FM ", this.dis.all[index].name, this.dis.all[index].uid, this.dis.actuel, index)		
		this.dis.network.sendMessage("ExchangeReadyMessage", {
			ready: true, step: this.dis.step
		});
		this.dis.step++;
		this.dis.all[index].q--

	}

	private async onExchangeCraftResultMagicWithObjectDescMessage(data: any) {
		await tool.wait(200)
		console.log()
		console.log(data['objectInfo']['effects'])
		var res = this.getDiff(data['objectInfo']['effects'])
		if (res === 0) {
			if (data['objectInfo']['effects'][0]['value'] <= 35  ) {
				this.fm(0)
			} else if (data['objectInfo']['effects'][1]['value'] < 8 ) {
				this.fm(14)
			} else if (data['objectInfo']['effects'][2]['value'] < 8 ) {
				this.fm(13)
			} else {
				console.log("ITEM OKAYYYY")
			}
		} else {
			this.fm(res)
		}
	}


	private async onMapComplementaryInformationsDataMessage(data: any) {	
		await tool.wait(1500)
		this.dis.network.sendMessage("InteractiveUseRequestMessage", {
			elemId: 455277,
			skillInstanceUid: 19492837
		});
		await tool.wait(500)
		this.dis.network.sendMessage("ExchangeObjectMoveMessage", {
			objectUID: this.dis.itemSpecial,
			quantity: 1
		});
		await tool.wait(500)
		this.dis.network.sendMessage("ExchangeObjectMoveMessage", {
			objectUID: this.dis.all[0].uid,
			quantity: this.dis.all[0].q
		});
		await tool.wait(500)
		this.dis.actuel = this.dis.all[0].uid;
		this.dis.actuelQ = this.dis.all[0].q;
		
		this.dis.network.sendMessage("ExchangeReadyMessage", {
			ready: true, step: this.dis.step
		});
		this.dis.step++;
		
		for (var i of this.dis.all) { // ajuster quantité du truck que on viens de fm
			if (i.uid === this.dis.actuel) {
				i.q--
				this.dis.actuelQ--
				//console.log(i, "FOUND")
			}
		}
		//this.fm(0);
		
		// ExchangeReadyMessage Object {ready: true, step: 2}
	}



	private onInventoryContentMessage(data: any) {
		for (var i of data['objects']) {
			if (i.objectGID === 6805) {
				this.dis.itemSpecial = i.objectUID
				console.log(i)
				for (var k of i.effects) {
					for (var l of this.dis.all) {
						if (k.actionId === l.id) {
							l.value = k.value
						}
					}
				}
			}
			if (i.objectGID === 1523) {
				this.dis.all[0].uid = i.objectUID
				this.dis.all[0].q = i.quantity
			}
			if (i.objectGID === 1519) {
				this.dis.all[1].uid = i.objectUID
				this.dis.all[1].q = i.quantity
			}
			if (i.objectGID === 1522) {
				this.dis.all[2].uid = i.objectUID
				this.dis.all[2].q = i.quantity
			}
			if (i.objectGID === 1521) {
				this.dis.all[3].uid = i.objectUID
				this.dis.all[3].q = i.quantity
			}
			if (i.objectGID === 7433) {
				this.dis.all[4].uid = i.objectUID
				this.dis.all[4].q = i.quantity
			}
			if (i.objectGID === 11665) {
				this.dis.all[5].uid = i.objectUID
				this.dis.all[5].q = i.quantity
			}
			if (i.objectGID === 11657) {
				this.dis.all[6].uid = i.objectUID
				this.dis.all[6].q = i.quantity
			}
			if (i.objectGID === 11659) {
				this.dis.all[7].uid = i.objectUID
				this.dis.all[7].q = i.quantity
			}
			if (i.objectGID === 7434) {
				this.dis.all[8].uid = i.objectUID
				this.dis.all[8].q = i.quantity
			}
			if (i.objectGID === 7451) {
				this.dis.all[9].uid = i.objectUID
				this.dis.all[9].q = i.quantity
			}
			if (i.objectGID === 7560) {
				this.dis.all[10].uid = i.objectUID
				this.dis.all[10].q = i.quantity
			}
			if (i.objectGID === 7458) {
				this.dis.all[11].uid = i.objectUID
				this.dis.all[11].q = i.quantity
			}
			if (i.objectGID === 11639) {

				this.dis.all[12].uid = i.objectUID
				this.dis.all[12].q = i.quantity
			}
			if (i.objectGID === 7454) {
				this.dis.all[13].uid = i.objectUID
				this.dis.all[13].q = i.quantity
			}
			if (i.objectGID === 7436) {
				this.dis.all[14].uid = i.objectUID
				this.dis.all[14].q = i.quantity
			}
		}
		console.log(this.dis.all)
	}









	private async onCharacterLevelUpInformationMessage(data: any) {
		this.dis.level = data.newLevel
	}

	private async onExchangeStartOkHumanVendorMessage(data: any) {
		for (var i of data['objectsInfos']){
			if (i.objectPrice === this.dis.marchandObjectPrice ) {
				this.dis.marchandObjectId = i.objectUID
			} else {
				this.dis.marchandObjectId2 = i.objectUID
			}
		}
		this.dis.marchandObjectId
	}


	

	private onCharactersListMessage(data: any) {
    	this.dis.network.username = this.dis.username
		if (data.characters.length > 0) {
			this.dis.characterId = data.characters[0].id
		}		
		this.dis.network.sendMessage("CharacterFirstSelectionMessage", {
			doTutorial: false,
			id: this.dis.characterId
		});
	}

	private async onGameRolePlayShowActorMessage(data: any) { // new player on map
		if (data.informations.name === this.dis.marchandName && data.informations.sellType === 3) {
			this.dis.network.sendMessage("AchievementRewardRequestMessage", {
		      achievementId: -1
		    });
		    this.dis.network.sendMessage("ExchangeOnHumanVendorRequestMessage", {
		      humanVendorId: this.dis.marchandCharacterId, humanVendorCell: this.dis.marchandCharacterCell
		    });
		    await tool.wait(1500)
		    this.dis.network.sendMessage("ExchangeBuyMessage", {
		      objectToBuyId: this.dis.marchandObjectId, quantity: this.dis.marchandQuantity
		    });

		    this.dis.network.sendMessage("LeaveDialogRequestMessage");
		    
		    var path = this.dis.Movement.moveToCell(this.dis.currentCell, 552);
		    this.dis.network.sendMessage("GameMapMovementRequestMessage", {
		      keyMovements: path,
		      mapId: this.dis.mapId
		    });
		    this.dis.network.sendMessage("GameMapMovementConfirmMessage");
		    this.dis.network.sendMessage("ChangeMapMessage", {
		      mapId: 84806915
		    });
		    this.dis.network.sendMessage("MapInformationsRequestMessage", {
		      mapId: this.dis.mapId
		    });
		}
		//console.log("FLOOD " + data.informations.name + " level : " , data.informations.alignmentInfos.characterPower - data.informations.contextualId)
		//console.log(data.informations.name);
	}

	// connection 
	private onGameActionFightNoSpellCastMessage(data: any) {
		//console.log(data, "fight action fight")
	}
	private onCharacterCreationResultMessage(data: any) {
		if (data.result === 0){
			console.log("Personnage créé avec succès.")
		} else {
			console.log("Impossible de créer le personnage : " + data.result)
			this.dis.network.crashed = 5
			this.dis.network.close()
			shell.exec("cd " + this.dis.path + " && npm start " + this.dis.username)
		  }
	}

	private onCharacterDeletionErrorMessage(data: any) {
		if (data.reason === 0){
			console.log("Personnage supprimé avec succès.")
		}
		else {
			console.log("Impossible de supprimer le personnage : " + data.reason)
		  }
	}



	private onTextInformationMessage(data: any) {
		let text: string = data.text;
		for (let i = 0; i < data.parameters.length; i++) {
			text = text.replace(`$%${i}`, data.parameters[i]);
		}
		// text.indexOf('de kamas pour acheter cet objet') === -1 &&  text.indexOf('disponible à ce prix') === -1 && text.indexOf('item') === -1
		if (text.indexOf('avez perdu') === -1 && text.indexOf('ur va être') === -1 && text.indexOf('mise à') === -1 && text.indexOf('il est int') === -1 && text.indexOf('mémoriser') === -1 && text.indexOf('lle quê') === -1 && text.indexOf('terminée') === -1 && text.indexOf('sauvegardée') === -1) {
			console.log(text);
		}
	}

	private onCurrentMapMessage(data: any) {
		this.dis.mapId = data.mapId
		this.dis.network.sendMessage("MapInformationsRequestMessage", {
			mapId: data.mapId
		});
	}

	
	// connection


	private onBasicLatencyStatsRequestMessage(data: any) {
		this.dis.network.sendMessage("BasicLatencyStatsMessage", {
			latency: 262,
			max: 50,
			sampleCount: 12
		});
	}

	private onSequenceNumberRequestMessage(data: any) {
		this.dis.sequence++;
		this.dis.network.sendMessage("SequenceNumberMessage", {
			number: this.dis.sequence
		});
	}


	private onIdentificationSuccessMessage(data: any) {
		this.dis.login = data.login
		this.dis.subscriber = new Date() < data.subscriptionEndDate;
	}

	private onCharacterSelectedSuccessMessage(data: any) {
		this.dis.network.sendMessage("kpiStartSession", {
			accountSessionId: this.dis.login,
			isSubscriber: this.dis.subscriber
		});
		this.dis.network.send("moneyGoultinesAmountRequest");
		this.dis.network.sendMessage("QuestListRequestMessage");
		this.dis.network.sendMessage("FriendsGetListMessage");
		this.dis.network.sendMessage("IgnoredGetListMessage");
		this.dis.network.sendMessage("SpouseGetInformationsMessage");
		this.dis.network.send("bakSoftToHardCurrentRateRequest");
		this.dis.network.send("bakHardToSoftCurrentRateRequest");
		this.dis.network.send("restoreMysteryBox");
		this.dis.network.sendMessage("ClientKeyMessage", {
			key: tool.randomString(21)
		});
		this.dis.network.sendMessage("GameContextCreateRequestMessage");
	}

	

	private onAuthenticationTicketAcceptedMessage(data: any) {
		this.dis.network.sendMessage('CharactersListRequestMessage');
	}

	private onHelloGameMessage(data: any) {
		this.dis.network.sendMessage("AuthenticationTicketMessage", {
			lang: "fr",
			ticket: this.dis.ticket
		});
		this.dis.network.phase = NetworkPhases.GAME;
	}

	private onSelectedServerDataMessage(data: any) {
		this.dis.ticket = data.ticket
		this.dis.network.switchToGameServer(data._access, {
			address: data.address,
			id: data.serverId,
			port: data.port
		});
	}

	private onServersListMessage(data: any) {
		this.dis.network.sendMessage('ServerSelectionMessage', {
			serverId: this.dis.serverId
		});
	}

	private onHelloConnectMessage(data: any) {
		this.dis.network.phase = NetworkPhases.LOGIN;
		this.dis.key = data.key;
		this.dis.salt = data.salt;

		this.dis.network.send('checkAssetsVersion', {
			assetsVersion: DTConstants.assetsVersion,
			staticDataVersion: DTConstants.staticDataVersion,
		});
	}
	private onAssetsVersionChecked(data: any) {
		this.dis.network.send('login', {
			key: this.dis.key,
			salt: this.dis.salt,
			token: this.dis.haapi.token,
			username: this.dis.username,
		});
	}

}
