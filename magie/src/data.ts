import Dispatcher from './Dispatcher';
import HaapiConnection from './HaapiConnection';
import Network from './Network';
import movement from './movement';
import DTConstants from './DTConstants';

export default class data {
  public haapi: HaapiConnection;
  public network: Network;
  public dis: Dispatcher;

  public Movement = new movement();

  public key: number[];
  public salt: string;
  public ticket: string;
  public login: string;

  public username: string;
  public password: string;


  public sequence = 0;
  public subscriber = false;
  public createdCamp = false;
  public characterId = 0;
  public currentCell = 0;
  public serverId = 403
  public mapId = 0;
  public createNew = true; // si il est a false on en créé pas
  public blackList = [];
  public tutoGroupe = -4618;
  public tutoInventory = [];
  public almanax = false;

  public itemId = 0;
  public canScrap = true;
  public canScrap2 = true;
  public allId = {};
  public weCanBuy = true;
  public alreadyHDV = true;
  public inventoryHDV = {};
  public crafted = false;
  public idCraft = 100;
  public alreadyOnMap = false;

  public almaQuestId = 996 //
  public almaVieux = [13721, 14316, 14314] //
  public almaBonomeCell = -17 //
  public almaBonome = [14323, 14322, 14321, 14320, 14317] //

  public marchandQuantity = 2 //
  public marchandObjectPrice = 1 //
  public marchandCharacterId = 1609903 ///
  public marchandCharacterCell = 192 ///
  public marchandName = "Acamir-Epynyw" ///

  public path = "/lib/dev/alma"

  public obj = 11521

  public marchandObjectId = 0
  public marchandObjectId2 = 0
  public amaknaUnderground = false
  public astrubRockyInlet = false
  public astrubUnderground = false
  public brakmarUnderground = false
  public numberCamp = 0
  public started = true
  public createdCharacter = false

  public level = 1;

  public phaseGame = 0;

  public reco = false;








  public step = 2;
  public itemSpecial = 0;

  public all = [
    {
      "name": "vi",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 125
    },
    {
      "name": "fo",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 118
    },
    {
      "name": "ine",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 126
    },
    {
      "name": "sa",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 124
    },
    {
      "name": "cri",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 115
    },
    {
      "name": "do neu",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 430
    },
    {
      "name": "do terre",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 422
    },
    {
      "name": "do feu",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 424
    },
    {
      "name": "soin",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 178
    },
    {
      "name": "propec",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 176
    },
    {
      "name": "res P eau",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 211
    },
    {
      "name": "res P air",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 212
    },
    {
      "name": "tacle",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 753
    },
    {
      "name": "res eau",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 241
    },
    {
      "name": "pui",
      "uid": 0,
      "q": 0,
      "value": 0,
      "id": 138
    }
  ]
  public actuel = 0;
  public actuelQ = 0;







  public async init(username, password, serverId, numberCamp) {
    
    this.haapi = new HaapiConnection();
    this.username = username;
    this.password = password;
    this.serverId = serverId;
    this.numberCamp = numberCamp

    //console.log('ICI PARIS', this, numberCamp)
    await this.haapi.processHaapi(username, password, serverId);
    console.log("apres le process api")
     this.dis = new Dispatcher();
     console.log("apres dispatcher")
    this.network = new Network(this.dis);
    console.log("apres le network")
    this.network.connect(DTConstants.config.sessionId, DTConstants.config.dataUrl);
    console.log("apres le connect")
  } 
}

