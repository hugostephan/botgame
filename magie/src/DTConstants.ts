import axios from 'axios';
import { IDofusTouchConfig } from './IDofusTouchConfig';

export default class DTConstants {
  public static readonly MAIN_URL = 'https://proxyconnection.touch.dofus.com';
  public static appVersion: string;
  public static buildVersion: string;
  public static assetsVersion: string;
  public static staticDataVersion: string;
  public static config: IDofusTouchConfig;

  public static async Init() {
    this.config = await DTConstants.getConfig();
    const m = await DTConstants.getAssetsVersions();
    this.assetsVersion = m.assetsVersion;
    this.staticDataVersion = m.staticDataVersion;
    this.appVersion = await DTConstants.getAppVersion();
    this.buildVersion = await DTConstants.getBuildVersion();
  }

  public static async getConfig(): Promise<IDofusTouchConfig> {
    const response = await axios.get(`${DTConstants.MAIN_URL}/config.json`);
    return response.data;
  }

  public static async getAssetsVersions(): Promise<any> {
    const response = await axios.get(`${DTConstants.MAIN_URL}/assetsVersions.json`);
    return response.data;
  }

  public static async getAppVersion(): Promise<string> {
    const response = await axios.get('https://itunes.apple.com/lookup', {
      params: {
        country: 'fr',
        id: 1041406978,
        lang: 'fr',
        limit: 1,
        t: Date.now(),
      },
    });
    console.log("VERSIONNN", response.data.results[0].version)
    return response.data.results[0].version;
  }

  public static async getBuildVersion(): Promise<string> {
    const response = await axios.get(`${DTConstants.MAIN_URL}/build/script.js`);
    const regex = /.*buildVersion=("|')([0-9]*\.[0-9]*\.[0-9]*)("|')/g;
    const m = regex.exec(response.data.substring(1, 10000));
    return m[2];
  }
}
