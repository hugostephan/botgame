import data from './../data';
import * as tool from '../tool/tool';

export async function makeTheTuto(Data: data) { 


  return new Promise(async (resolve, reject) => {
    // step 1
    await tool.wait(2000)
    console.log('start tuto')
    var path = Data.Movement.moveToCell(Data.currentCell, 272);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    console.log('on sest deplacer')
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3499,
      questId: 489
    });
    console.log('miose a jour quest 1')
    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });
    console.log('miose a jour quest 2')


    //step 2
    console.log("start step 2")
    Data.network.sendMessage("NpcGenericActionRequestMessage", {
      npcActionId: 3,
      npcId: -1,
      npcMapId: 81002496
    });
    console.log("ask le png")
    Data.network.sendMessage("NpcDialogReplyMessage", {
      replyId: 7489
    });
    console.log('on lui repond')
    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });

    await tool.wait(2000)
    console.log("met litem")
    for (var i of Data.tutoInventory) {
      if (i.objectGID === 10785) {
        Data.network.sendMessage("ObjectSetPositionMessage", {
          objectUID: i.objectUID,
          position: 2,
          quantity: 1
        });
      }
    }

    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3502,
      questId: 489
    });

    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3502,
      questId: 489
    });

    await tool.wait(2000)
    console.log('change de map')
    var path = Data.Movement.moveToCell(272, 503);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("ChangeMapMessage", {
      mapId: 81003008
    });
    Data.network.sendMessage("MapInformationsRequestMessage ", {
      mapId: 81003520
    });

    await tool.wait(2000)
    console.log('map 2')
    console.log(Data.tutoGroupe, Data.currentCell)
    var path = Data.Movement.moveToCell(Data.currentCell, 272);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: 81003520
    });

    Data.network.sendMessage("GameRolePlayAttackMonsterRequestMessage", {
      monsterGroupId: Data.tutoGroupe
    });
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3504,
      questId: 489
    });

    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });

    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3507,
      questId: 489
    });
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3507,
      questId: 489
    });

    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });

    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true
    });

    await tool.wait(2000)
    //console.log('aggro le palmier')
    var path = Data.Movement.moveToCell(Data.currentCell, 272);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: 81003520
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("GameRolePlayAttackMonsterRequestMessage", {
      monsterGroupId: Data.tutoGroupe
    });
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3504,
      questId: 489
    });

    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });

    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3507,
      questId: 489
    });
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3507,
      questId: 489
    });

    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });

    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true
    });
    Data.network.sendMessage("GameFightPlacementPositionRequestMessage", {
      cellId: 316
    });

    Data.network.sendMessage("GameFightReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });
    await tool.wait(2000)
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3508,
      questId: 489
    });

    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });

    console.log('move cell')
    var path = Data.Movement.moveToCell(316, 287); // pas de tp car combat
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: 81003520
    });
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3508,
      questId: 489
    });
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3508,
      questId: 489
    });

    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    await tool.wait(2000)
    console.log('lance le sort')
    Data.network.sendMessage("GameActionFightCastRequestMessage", {
      cellId: 272,
      spellId: 21
    });
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3509,
      questId: 489
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 5
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });
    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });
    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 81003522
    });

    // palmier tuer
    await tool.wait(2000)
    Data.network.sendMessage("NpcGenericActionRequestMessage", {
      npcId: -1,
      npcActionId: 3,
      npcMapId: 81003522
    }); Data.network.sendMessage("NpcDialogReplyMessage", {
      replyId: 7590
    }); Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 492
    }); Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });
    for (var i of Data.tutoInventory) {
      if (i.objectGID === 10800) {
        console.log('on équipe la cape')
        Data.network.sendMessage("ObjectSetPositionMessage", {
          objectUID: i.objectUID,
          position: 7,
          quantity: 1
        });
      } else if (i.objectGID === 10784) {
        console.log('on équipe lamullette')
        Data.network.sendMessage("ObjectSetPositionMessage", {
          objectUID: i.objectUID,
          position: 0,
          quantity: 1
        });
      } else if (i.objectGID === 10799) {
        console.log('on équipe la ceinture')
        Data.network.sendMessage("ObjectSetPositionMessage", {
          objectUID: i.objectUID,
          position: 3,
          quantity: 1
        });
      } else if (i.objectGID === 10797) {
        console.log('on équipe lépée')
        Data.network.sendMessage("ObjectSetPositionMessage", {
          objectUID: i.objectUID,
          position: 1,
          quantity: 1
        });
      } else if (i.objectGID === 10794) {
        console.log('on équipe les bottes')
        Data.network.sendMessage("ObjectSetPositionMessage", {
          objectUID: i.objectUID,
          position: 5,
          quantity: 1
        });
      }
    }
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3530,
      questId: 489
    });
    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });

    // item équiper all


    await tool.wait(2000)
    console.log('change de map pour aller vers milimulou')
    var path = Data.Movement.moveToCell(127, 503);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: 81003522
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("ChangeMapMessage", {
      mapId: 81004034
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 81004544
    });

    // millimilou aggro
    await tool.wait(2000)
    console.log('aggro mili')
    var path = Data.Movement.moveToCell(85, 287);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: 81004544
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("GameRolePlayAttackMonsterRequestMessage", {
      monsterGroupId: Data.tutoGroupe
    });
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      objectiveId: 3513,
      questId: 489
    });
    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: 489
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });
    console.log('je suis en combat')
    Data.network.sendMessage("GameFightPlacementPositionRequestMessage", {
      cellId: 214
    });
    Data.network.sendMessage("GameFightReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });






    await tool.wait(2000)
    console.log('attente premier tour')
    Data.network.sendMessage("GameFightTurnFinishMessage");
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });

    await tool.wait(2000)
    console.log('attente deuxieme tour')
    Data.network.sendMessage("GameFightTurnFinishMessage");
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });

    await tool.wait(2000)
    console.log('attente troisieme tour')
    Data.network.sendMessage("GameFightTurnFinishMessage");
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });

    await tool.wait(2000)
    console.log('milou 1')
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 4
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });

    await tool.wait(2000)
    console.log('attente quatrieme tour')
    Data.network.sendMessage("GameFightTurnFinishMessage");
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });

    await tool.wait(2000)
    console.log('milou 2')
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 4
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });

    await tool.wait(2000)
    console.log('attente cinquieme tour')
    Data.network.sendMessage("GameFightTurnFinishMessage");
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });

    await tool.wait(2000)
    console.log('milou3')
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 4
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });

    await tool.wait(2000)
    console.log('attente 6 tour')
    Data.network.sendMessage("GameFightTurnFinishMessage");
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });

    await tool.wait(2000)
    console.log('milou 4')
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 4
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });

    await tool.wait(2000)
    console.log('on attaque 1')
    Data.network.sendMessage("GameActionFightCastRequestMessage", {
      cellId: 229,
      spellId: 21
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 4
    });
    Data.network.sendMessage("GameFightTurnFinishMessage");
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });

    await tool.wait(2000)
    console.log('milou 5')
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 4
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });

    await tool.wait(2000)
    console.log('on attaque 2')
    Data.network.sendMessage("GameActionFightCastRequestMessage", {
      cellId: 229,
      spellId: 21
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 4
    });
    Data.network.sendMessage("GameFightTurnFinishMessage");
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });

    await tool.wait(2000)
    console.log('milou 6')
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 4
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });

    await tool.wait(2000)
    console.log('on attaque 3')
    Data.network.sendMessage("GameActionFightCastRequestMessage", {
      cellId: 229,
      spellId: 21
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 4
    });
    Data.network.sendMessage("GameFightTurnFinishMessage");
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: 3
    });
    Data.network.sendMessage("GameFightTurnReadyMessage", {
      isReady: true
    });
    Data.network.sendMessage("GameActionAcknowledgementMessage", {
      valid: true,
      actionId: undefined
    });


    // combat fini milou
    await tool.wait(2000)
    console.log('combat fini talk pour sortir')
    Data.network.sendMessage("QuestObjectiveValidationMessage", {
      questId: 1461,
      objectiveId: 8078
    });
    await tool.wait(1000)
    Data.network.sendMessage("NpcGenericActionRequestMessage", {
      npcId: -1,
      npcActionId: 3,
      npcMapId: 81004546
    });

    Data.network.sendMessage("NpcDialogReplyMessage", {
      replyId: 7542
    });

    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 81264642
    });
    await tool.wait(1000)
    Data.network.sendMessage("AchievementRewardRequestMessage", {
      achievementId: -1
    });
    /*
    await tool.wait(5000)
    if (Data.mapId === 81264642) {
      return resolve('TUTO FINI')
    } else {
      return reject('TUTO BUGGED')
    }
    */
  })

}