export default class PathDuration {
    private static animDurations;
    static calculate(path: number[], isFight?: boolean, slide?: boolean, riding?: boolean): number;
}
