export declare enum MapChangeDirections {
    NONE = 0,
    Left = 56,
    Right = 131,
    Top = 224,
    Bottom = 14,
    Gauche = 56,
    Droite = 131,
    Haut = 224,
    Bas = 14
}
export declare enum MovementRequestResults {
    MOVED = 0,
    ALREADY_THERE = 1,
    FAILED = 2,
    PATH_BLOCKED = 3
}
