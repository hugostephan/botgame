import PathFinder from '../path';
import MapsManager from '../path/map/MapsManager';
import { MapChangeDirections, MovementRequestResults } from './enums';

export default class movement {
	public pathFinder: PathFinder = new PathFinder()
	public async setMap(mapId:number) {
		const data = await MapsManager.getMap(mapId);
		this.pathFinder.setMap(data)
	}
	public moveToCell(		current:number, cellId: number,		stopNextToTarget: boolean = false	) {
		if (cellId < 0 || cellId > 560) {
			console.log("cell id peut pas aller plus 7854")
			return MovementRequestResults.FAILED;
		}
		const path = this.pathFinder.getPath(
			current,
			cellId,
			[],
			true,
			stopNextToTarget
		);
		return path;
	}
}