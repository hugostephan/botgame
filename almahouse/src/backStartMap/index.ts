import data from './../data';
import * as tool from '../tool/tool';
import inde from './../index';
var fs = require('fs')
export async function backStartMap(Data: data) { 

  function go(cellToGo, mapToGo, mapGosth) {
    var path = Data.Movement.moveToCell(Data.currentCell, cellToGo);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("ChangeMapMessage", {
      mapId: mapGosth
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: mapToGo
    });
  }
  return new Promise(async (resolve, reject) => {   
    if (Data.mapId === 101453317) {
      go(452, 101451273, 101451273)
    } else if (Data.mapId === 101451273) {
      go (466, 101450251, 101450251)
    } else if (Data.mapId === 101450251) {
      go(465, 84805890, 84805890)
    } else if (Data.mapId === 84805890) { 
      go(4, 84805891, 84805891)     
    } else if (Data.mapId === 84805891) { 
      go(364, 84806403, 84806403)
    } else if (Data.mapId === 84806403) { 
      go(364, 84806915, 84806915)
    } else if (Data.mapId === 84806915) { 
      go(4, 84806916, 84806916)
      Data.started = false
      Data.network.close()
      var tamer = Data.numberCamp + 1;
      if (tamer < 5){
        console.log('ON LANCE SUR LE PERSO 6 ', tamer + 1)
        new inde(Data.username, Data.password, Data.serverId, Data.username, tamer)
      } else {
        console.log(" FAIT SUR TOUT LES PERSO")
        fs.writeFile('./src/finish.txt', Data.username, 'utf8', function(err) { // reset les ban, SI 2 en meme temps = bug donc mettre timer 
          console.log('RESET FINISH')
        });
      }
    } else {
      console.log('on est perdu rien commencer')
    }    
  })
}

