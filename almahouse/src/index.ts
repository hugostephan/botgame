import Dispatcher from './Dispatcher';
import DTConstants from './DTConstants';
import HaapiConnection from './HaapiConnection';
import Network from './Network';
import { NetworkPhases } from './NetworkPhases';
import accConnect from './';
import MapPoint from './path/MapPoint';
import movement from './movement';
import * as tool from './tool/tool';
import frame from './frame';
import data from './data';

var fs = require('fs')
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var messageToSend = []

let Data = new data();  

export default class {
  constructor(
      public username: any,
      public password: any,
      public serverId: any,
      public createNew: any,
      public numberCamp: any,
      public reco: any
    ) {
      if (username.includes("#")){
        console.log("Il n'y a plus de comptes dans la liste, arrêt du script.")
        process.exit()
      }
      this.start(username, password, serverId, createNew, numberCamp, reco)
  }
  public async start(username, password, serverId, createNew, numberCamp, reco) {

    Data.serverId = serverId
    Data.createNew = createNew
    Data.reco = reco
    let Frame: frame;
    await DTConstants.Init();
    await Data.init(username, password, serverId, numberCamp)
    MapPoint.Init();
    Frame = new frame(Data, io);
    console.log("apres le frame")
  }
}
// hedergrize 405, brutas 407, terracogita 404, grandapan 401, dodge 406, oshimo 403

export async function connect(user, pass, reco) {
  // netijavija@1shivom.com
  // start("Echodu01", 'combat123', 405, false)
  // start(ndc, 'azerty321', 405, false)
  if (reco ) {
    new accConnect(user, pass, 404, true, 0, true)
  } else {
    new accConnect(user, pass, 404, true, 0, false)
  }
  console.log(reco)
  console.log('====>', user, ':', pass)
  //new accConnect(user, pass, 403, true, 0)
}

console.log(process.argv[2]);
connect(process.argv[2], "motdepasse123", process.argv[3])

/*
fs.readFile('./src/listAllAccount.txt', 'utf8', async function(err, data) {
  var rawacc = data.split('\n')
  var acc = rawacc[0].split(':')
  connect(acc[0], acc[1])
})
*/
