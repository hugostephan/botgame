const shell = require('shelljs');
var fs = require('fs')

import data from './../data';
import * as tool from '../tool/tool';
import accConnect from "../.";
import connect from '../index';

// top 15 13 23 10 27
  //left 420 266 140
  // bottom 556
  // right 69 335 517

export async function moveToZaap(Data: data) { 
  function prendreVieux(){
    //console.log('PARLER VIEUX')
    Data.network.sendMessage("NpcGenericActionRequestMessage", {      npcId: -2, npcActionId: 3, npcMapId: 101450251    });
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13557});
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13556});
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13555});
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13554});
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13552});
    Data.network.sendMessage("QuestStepInfoRequestMessage", {questId: 954});
    for (var i of Data.almaVieux) {
      Data.network.sendMessage("NpcDialogReplyMessage", {replyId: i});
    }
    Data.network.sendMessage("QuestStepInfoRequestMessage", {questId: Data.almaQuestId});//
    Data.network.sendMessage("NpcGenericActionRequestMessage", {      npcId: -2, npcActionId: 3, npcMapId: 101450251    });
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13563});
    Data.network.sendMessage("QuestStepInfoRequestMessage", {questId: Data.almaQuestId});//
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13562});
    Data.network.sendMessage("LeaveDialogRequestMessage")

    console.log('FINI PARLER VIEUX')
    goDeuxiemeSalle()
  }
  async function goDeuxiemeSalle(){
    await tool.wait(2000)
    //console.log('GO MAP 2')
    var path = Data.Movement.moveToCell(Data.currentCell, 274);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("InteractiveUseRequestMessage", {
      elemId: 476222, skillInstanceUid: 19508806
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 101453321
    });
  }
  function prendrePorte(){
    console.log('GO PIERRE')
    var path = Data.Movement.moveToCell(Data.currentCell, 375);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("InteractiveUseRequestMessage", {
      elemId: 476219, skillInstanceUid: 19508824
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 101451265
    });
  }
  function parlerPierre(){
    var path = Data.Movement.moveToCell(Data.currentCell, 398);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.currentCell = 398
    Data.network.sendMessage("InteractiveUseRequestMessage", {
      elemId: 479595, skillInstanceUid: 19508832
    });
    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: Data.almaQuestId
    });
    console.log('PIERRE FINI')
    parlerBonome()
  }
  function parlerBonome(){
    console.log("PARLER BONOME")
    Data.network.sendMessage("NpcGenericActionRequestMessage", {      npcId: Data.almaBonomeCell, npcActionId: 3, npcMapId: 101451265    });
    for (var i of Data.almaBonome) {
      Data.network.sendMessage("NpcDialogReplyMessage", {replyId: i});
    }
    Data.network.sendMessage("QuestStepInfoRequestMessage", {questId: Data.almaQuestId});
    Data.network.sendMessage("LeaveDialogRequestMessage")
    outBonome()
  }
  async function outBonome() {
    await tool.wait(2000)
    console.log("On sort de la map pierre apres avoir parler au bonome")
    var path = Data.Movement.moveToCell(Data.currentCell, 452)
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.almanax = true
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    await tool.wait(2000)
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId:  101453321
    });
  }
  async function almaBackToall() {
    await tool.wait(2000)
    console.log('GO TO HALL')
    var path = Data.Movement.moveToCell(Data.currentCell, 466);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 101450251
    });
  }
  async function finirParlerVieux() {
    console.log('PARLE FINIR QUETE VIEUX')
    
    Data.network.sendMessage("NpcGenericActionRequestMessage", {
      npcId: -2, npcActionId: 3, npcMapId: 101450251
    });
    Data.network.sendMessage("NpcDialogReplyMessage", {
      replyId: 13564
    });
    Data.network.sendMessage("NpcDialogReplyMessage", {
      replyId: 13565
    });
    
    Data.network.sendMessage("LeaveDialogRequestMessage");
    console.log("ID POPO", Data.rappel)
    await tool.wait(100)
    Data.network.sendMessage("ObjectUseMessage", {
      objectUID: Data.rappel
    });
    //await tool.wait(100)
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 84804104
    });
    
  }
  function go(cellToGo, mapToGo, mapGosth) {
    var path = Data.Movement.moveToCell(Data.currentCell, cellToGo);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("ChangeMapMessage", {
      mapId: mapGosth
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: Data.mapId
    });
    console.log(Data.mapId + ' ==> ' + mapToGo)
  }

  async function perdu(name) {
    fs.readFile('/lib/dev/almahouse/do.txt', 'utf8', function(err, data) {
      //fconsole.log(data, name)
      data = data.replace(name + '\n', '')
      //fconsole.log(data, name)
      fs.writeFile('/lib/dev/almahouse/do.txt', data, 'utf8', function(err) {
          if (err) { return console.log(err); }
      });
    });
    Data.network.close()
    shell.exec("cd /lib/dev/almahouse && npm start " + name + " d")
    //shell.exec("screen -X -S " + name + " quit")   
  }

  return new Promise(async (resolve, reject) => {
    if (Data.almanax === false) {
      if (Data.mapId === 84804104) {
        console.log("STATUS")
        go(23, 84804103, 84804103)
      } else if (Data.mapId === 84804103) {
        go(23, 84804102, 84804102)
      } else if (Data.mapId === 84804102) {
        console.log("DEHORS MAISON")
        var path = Data.Movement.moveToCell(Data.currentCell, 241);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path, mapId: 84804102
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 465555, skillInstanceUid: 19535197
        });
        Data.network.sendMessage("LockableUseCodeMessage", {
          code: "231111__"
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 82840582
        });
      } else if (Data.mapId === 82840582) { 
        console.log("DANS MAISON")

        var path = Data.Movement.moveToCell(Data.currentCell, 388);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path, mapId: 82840582
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 465499, skillInstanceUid: 19535195
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 82841606
        });
      } else if (Data.mapId === 82841606) {
        console.log("DEVANT COFFRE")
        var path = Data.Movement.moveToCell(Data.currentCell, 289);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path, mapId: 82841606
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 465599, skillInstanceUid: 19535191
        });
        Data.network.sendMessage("LockableUseCodeMessage", {
          code: "231111__"
        });
      } else if (Data.mapId === 101450251) {
        console.log("DANS LE TEMPLE")
        prendreVieux()
      } else if (Data.mapId === 101453321) {
        console.log("4 PORTE")
        prendrePorte()
      } else if (Data.mapId === 101451265) {
        console.log("MAP PIERRE", Data.level)
        parlerPierre()   
      } else {
        console.log("PERDU ALMA NON", Data.mapId)
        perdu(Data.username)
      }
    } else if (Data.almanax === true) {
      if (Data.mapId === 101453321 ) {
        Data.porteBack = true
        console.log("4 PORTE BACK")
        almaBackToall()
      } else if (Data.mapId === 101450251  ) {
        Data.parlerVieux = true
        console.log("ARRIVER FIN ALMA")
        finirParlerVieux()
      } else if (Data.mapId === 84804104) {
        console.log("STATUS BACK")
        go(23, 84804103, 84804103)
      } else if (Data.mapId === 84804103) {
        go(23, 84804102, 84804102)
      } else if (Data.mapId === 84804102) {
        console.log("DEHORS MAISON BACK")
        var path = Data.Movement.moveToCell(Data.currentCell, 241);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path, mapId: 84804102
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 465555, skillInstanceUid: 19535197
        });
        Data.network.sendMessage("LockableUseCodeMessage", {
          code: "231111__"
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 82840582
        });
      } else if (Data.mapId === 82840582) { 
        console.log("DANS MAISON")
        var path = Data.Movement.moveToCell(Data.currentCell, 388);
        console.log(path)
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path, mapId: 82840582
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 465499, skillInstanceUid: 19535195
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 82841606
        });
      } else if (Data.mapId === 82841606) {
        console.log("DEVANT COFFRE")
        var path = Data.Movement.moveToCell(Data.currentCell, 289);
        console.log(path)
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path, mapId: 82841606
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 465599, skillInstanceUid: 19535191
        });
        Data.network.sendMessage("LockableUseCodeMessage", {
          code: "231111__"
        });
      } else {
        console.log("PERDU ALMA OUI", Data.mapId)
        //perdu(Data.username)
      }
    } else {
      console.log("PERDUUUUUUUUUUUUUUU")
    }
    resolve()
  });
}

/*
lse if (Data.mapId === 101450251 && !Data.almanax) {
      console.log("MAP DEDANS")
      prendreVieux()
    } else if (Data.mapId ===  101452297 && !Data.almanax) {
      console.log("MAP 2")
      prendrePorte()
    } else if (Data.mapId === 101450245 && !Data.almanax) {
      console.log("MAP PIERRE")
      parlerPierre()     
    }
      */