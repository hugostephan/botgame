import data from './../data';
import inde from './../index';
var fs = require('fs')
import * as tool from '../tool/tool';
import { NetworkPhases } from '../NetworkPhases';
import DTConstants from '../DTConstants';

import { makeTheTuto } from '../tuto';
import { backStartMap } from '../backStartMap';
import { moveToZaap } from '../moveToZaap';
import mapData from "../path/map";
var md5 = require('md5');
const shell = require('shelljs');
export default class frame {

	constructor(
		public dis: data,
		public io: any
	) {
		this.register()
	}
	private register(){
		// connection
		this.dis.dis.register('HelloConnectMessage', this.onHelloConnectMessage, this);
		this.dis.dis.register('assetsVersionChecked', this.onAssetsVersionChecked, this);
		this.dis.dis.register('ServersListMessage', this.onServersListMessage, this);
		this.dis.dis.register('SelectedServerDataMessage', this.onSelectedServerDataMessage, this);
		this.dis.dis.register('HelloGameMessage', this.onHelloGameMessage, this);
		this.dis.dis.register('AuthenticationTicketAcceptedMessage', this.onAuthenticationTicketAcceptedMessage, this)
		this.dis.dis.register('CharactersListMessage', this.onCharactersListMessage, this)
		this.dis.dis.register('CharacterSelectedSuccessMessage', this.onCharacterSelectedSuccessMessage, this)
		this.dis.dis.register('IdentificationSuccessMessage', this.onIdentificationSuccessMessage, this)
		this.dis.dis.register('SequenceNumberRequestMessage', this.onSequenceNumberRequestMessage, this)
		this.dis.dis.register('BasicLatencyStatsRequestMessage', this.onBasicLatencyStatsRequestMessage, this)
		this.dis.dis.register('CurrentMapMessage', this.onCurrentMapMessage, this)
		this.dis.dis.register('CharacterCreationResultMessage', this.onCharacterCreationResultMessage, this)
		this.dis.dis.register('CharacterDeletionErrorMessage', this.onCharacterDeletionErrorMessage, this)
		
		// map path
		this.dis.dis.register('TextInformationMessage', this.onTextInformationMessage, this)
		this.dis.dis.register('MapComplementaryInformationsDataMessage', this.onMapComplementaryInformationsDataMessage, this)
		this.dis.dis.register('GameRolePlayShowActorMessage', this.onGameRolePlayShowActorMessage, this)

		// invetory
		this.dis.dis.register('InventoryContentMessage', this.onInventoryContentMessage, this)

		
		// marchand
		this.dis.dis.register('ExchangeStartOkHumanVendorMessage', this.onExchangeStartOkHumanVendorMessage, this)

		//level
		this.dis.dis.register('CharacterLevelUpInformationMessage', this.onCharacterLevelUpInformationMessage, this)


		// copopo

		this.dis.dis.register('ExchangeTypesItemsExchangerDescriptionForUserMessage', this.onExchangeTypesItemsExchangerDescriptionForUserMessage, this)

		this.dis.dis.register('ObjectAddedMessage', this.onObjectAddedMessage, this)

		// house map
		this.dis.dis.register('MapComplementaryInformationsDataInHouseMessage', this.onMapComplementaryInformationsDataInHouseMessage, this)

		// coffre
		this.dis.dis.register('LockableStateUpdateStorageMessage', this.onLockableStateUpdateStorageMessage, this)
		this.dis.dis.register('StorageInventoryContentMessage', this.onStorageInventoryContentMessage, this)
		this.dis.dis.register('StorageObjectUpdateMessage', this.onStorageObjectUpdateMessage, this)

		//quest
		this.dis.dis.register('QuestListMessage', this.onQuestListMessage, this)

		// kamas
		this.dis.dis.register('KamasUpdateMessage', this.onKamasUpdateMessage, this)		

	}

	private onKamasUpdateMessage(data: any) {
		this.dis.kamas = data.kamasTotal
	}

	private async onQuestListMessage(data: any) {
		if (data.finishedQuestsIds.length !== 0) {
			console.log("QUETE FAIT")
			function fi(username) {
				fs.readFile('/lib/dev/almahouse/do.txt', 'utf8', function(err, data) {
				    data = data.replace(username + '\n', '')
				    fs.writeFile('/lib/dev/almahouse/do.txt', data, 'utf8', function(err) {
				      	if (err) { return console.log(err); }
				      	fs.appendFile('/lib/dev/almahouse/finish.txt', username + "\n", function (err) {
					  		if (err) throw err;
					  		console.log('Saved!');
					  		shell.exec("screen -X -S " + username + " quit")
						});	      	
				    });
				});
			}
			fi(this.dis.username)
			this.dis.network.crashed = 5
	        this.dis.network.close()
		}
	}

	private onStorageObjectUpdateMessage(data: any) {
		if (this.dis.itmNbr === 1) {
			this.dis.itmNbr = 2
			this.dis.network.sendMessage("ExchangeObjectMoveMessage", {
	          objectUID: this.dis.almaUID, quantity: this.dis.itemAlmaQTY * -1
	        });
		} else {
			this.dis.network.sendMessage("ExchangeObjectMoveMessage", {
	          objectUID: this.dis.temple, quantity: -1
	        });
	        this.dis.network.sendMessage("ExchangeObjectMoveKamaMessage", {
	          quantity: this.dis.kamas
	        });
			this.dis.network.sendMessage("LeaveDialogRequestMessage");
	        this.dis.network.sendMessage("ObjectUseMessage", {
	          objectUID: this.dis.temple
	        });
	        this.dis.network.sendMessage("MapInformationsRequestMessage", {
	          mapId: 101450251
	        });
		}
	}

	private async onStorageInventoryContentMessage(data: any) {
		console.log("COFFRE OUVERT", data.kamas)
		this.dis.coffre = false
		if (this.dis.almanax === false) {
			this.dis.coffre = false
			for (var i of data['objects']) {
				if (i.objectGID === 548) {
					this.dis.rappel = i.objectUID
				}
				if (i.objectGID === 13442) {
					this.dis.temple = i.objectUID
				}
				if (i.objectGID === this.dis.itemAlmaGID) {
					this.dis.almaUID = i.objectUID
				}
				
			}
			//await tool.wait(500)
			this.dis.network.sendMessage("ExchangeObjectMoveMessage", {
	          objectUID: this.dis.rappel, quantity: -1
	        });
	        this.dis.network.sendMessage("ExchangeObjectMoveKamaMessage", {
	          quantity: this.dis.kamas
	        });
	        /*
	        setTimeout(() => {
	        	this.dis.network.sendMessage("LeaveDialogRequestMessage");
	        	 this.dis.network.sendMessage("InteractiveUseRequestMessage", {
		          elemId: 465599, skillInstanceUid: 19535191
		        });
		        this.dis.network.sendMessage("LockableUseCodeMessage", {
		          code: "231111__"
        		});
	        }, 3000)
	        */
	    } else {
	    	this.dis.coffre = false
	    	await tool.wait(200)
	    	console.log("COFFRE OUVERT ON MET LES KAMAS", this.dis.gainKamasAlma)
	    	this.dis.network.sendMessage("ExchangeObjectMoveKamaMessage", {
	          quantity: this.dis.kamas
	        });
	        this.dis.network.sendMessage("LeaveDialogRequestMessage");await tool.wait(200)
	    	console.log("COFFRE OUVERT ON MET LES KAMAS", this.dis.gainKamasAlma)
	    	this.dis.network.sendMessage("ExchangeObjectMoveKamaMessage", {
	          quantity: this.dis.kamas
	        });
	        this.dis.network.sendMessage("LeaveDialogRequestMessage");await tool.wait(200)
	    	console.log("COFFRE OUVERT ON MET LES KAMAS", this.dis.gainKamasAlma)
	    	this.dis.network.sendMessage("ExchangeObjectMoveKamaMessage", {
	          quantity: this.dis.kamas
	        });
	        this.dis.network.sendMessage("LeaveDialogRequestMessage");await tool.wait(200)
	    	console.log("COFFRE OUVERT ON MET LES KAMAS", this.dis.gainKamasAlma)
	    	this.dis.network.sendMessage("ExchangeObjectMoveKamaMessage", {
	          quantity: this.dis.kamas
	        });
	        this.dis.network.sendMessage("LeaveDialogRequestMessage");await tool.wait(200)
	    	console.log("COFFRE OUVERT ON MET LES KAMAS", this.dis.gainKamasAlma)
	    	this.dis.network.sendMessage("ExchangeObjectMoveKamaMessage", {
	          quantity: this.dis.kamas
	        });
	        this.dis.network.sendMessage("LeaveDialogRequestMessage");
	        console.log("FINI ON DECO")
	        this.dis.network.crashed = 5
	        this.dis.network.close()
	        shell.exec("cd /lib/dev/almahouse && npm start " + this.dis.username)
	    }
	}

	private async onLockableStateUpdateStorageMessage(data: any) {
		console.log("OUVERTURE DU COFFRE")
		//await tool.wait(250)
		if (this.dis.coffre === false) {
			console.log("COFFRE OCCUPER")
			this.dis.network.sendMessage("InteractiveUseRequestMessage", {
	          elemId: 465599, skillInstanceUid: 19535191
	        });
	        this.dis.network.sendMessage("LockableUseCodeMessage", {
	          code: "231111__"
	        });
		} else {
			console.log("COFFRE PAS OCCUPER")
			this.dis.coffre = false
		}
	}

	private onObjectAddedMessage(data: any) {
		if (data.object['_type'] === "ObjectItem" && data.object.objectGID === 548) {
			this.dis.rappel = data.object.objectUID
		}
		this.dis.network.sendMessage("ObjectUseMessage", {
			objectUID: data['object']['objectUID']
		});
		this.dis.network.sendMessage("MapInformationsRequestMessage", {
			mapId: 84804104
		})
		this.dis.statu = true
	}

	private async onExchangeTypesItemsExchangerDescriptionForUserMessage(data: any) {
		console.log(data['itemTypeDescriptions'][0]['objectUID'], data['itemTypeDescriptions'][0]['prices'][0])
		this.dis.network.sendMessage("ExchangeBidHouseBuyMessage", {
			uid: data['itemTypeDescriptions'][0]['objectUID'], qty: 1, price: data['itemTypeDescriptions'][0]['prices'][0]
		});
		this.dis.network.sendMessage("LeaveDialogRequestMessage");
		
	}
	



	private async onCharacterLevelUpInformationMessage(data: any) {
		this.dis.level = data.newLevel
	}

	private async onExchangeStartOkHumanVendorMessage(data: any) {
		
	}

	private async onMapComplementaryInformationsDataMessage(data: any) {	
		//await tool.wait(200)
		const player = data.actors.find(a => a.contextualId === this.dis.characterId);
		this.dis.currentCell = player.disposition.cellId;
		await this.dis.Movement.setMap(data.mapId);			
		await moveToZaap(this.dis)	
		//console.log("TRIGGER", this.dis.mapId)
		setTimeout(() => {
			//this.dis.network.crashed = 5
	        this.dis.network.close()
    	}, 60000)

	}

	private async onMapComplementaryInformationsDataInHouseMessage(data: any) {	
		await tool.wait(200)
		setTimeout(() => {
			//this.dis.network.crashed = 5
	        this.dis.network.close()
    	}, 60000)
		if (this.dis.inHouse === false) {
			this.dis.inHouse = true
			const player = data.actors.find(a => a.contextualId === this.dis.characterId);
			this.dis.currentCell = player.disposition.cellId;
			await this.dis.Movement.setMap(data.mapId);			
			await moveToZaap(this.dis)	
			//console.log("TRIGGER", this.dis.mapId)
		} else {
			this.dis.inHouse = false
		}
	}

	private onCharactersListMessage(data: any) {
    	this.dis.network.username = this.dis.username
    	//console.log(data.characters[3], data.characters.length)
		if (data.characters.length === 5) {
			this.dis.characterId = data.characters[3].id
			this.dis.network.sendMessage("CharacterFirstSelectionMessage", {
				doTutorial: false,
				id: this.dis.characterId
			});
		} else {
			console.log("PAS 5 PERSO")
			var e = this.dis.username
			fs.readFile('/lib/dev/almahouse/do.txt', 'utf8', function(err, data) {
				//console.log(data, e)
    		    data = data.replace(e + '\n', '')
    		    //console.log(data, e)
    		    fs.writeFile('/lib/dev/almahouse/do.txt', data, 'utf8', function(err) {
    		      	if (err) { return console.log(err); }
    		    });
    		});
    		this.dis.network.crashed = 5
    		shell.exec("screen -X -S " + this.dis.username + " quit")
    		this.dis.network.close()
		}
	}

	private async onGameRolePlayShowActorMessage(data: any) { // new player on map
		
		//console.log("FLOOD " + data.informations.name + " level : " , data.informations.alignmentInfos.characterPower - data.informations.contextualId)
		//console.log(data.informations.name);
	}

	// connection 
	private onGameActionFightNoSpellCastMessage(data: any) {
		//console.log(data, "fight action fight")
	}
	private onCharacterCreationResultMessage(data: any) {
		
	}

	private onCharacterDeletionErrorMessage(data: any) {
		if (data.reason === 0){
			console.log("Personnage supprimé avec succès.")
		}
		else {
			console.log("Impossible de supprimer le personnage : " + data.reason)
		  }
	}

	private onInventoryContentMessage(data: any) {
		this.dis.kamas = data.kamas
		for (var i of data['objects']) {
			this.dis.tutoInventory.push(i)
			if (i.objectGID === 548) {
				this.dis.rappel = i.objectUID
			}
			if (i.objectGID === 13442) {
				this.dis.temple = i.objectUID
			}
		}
	}

	private onTextInformationMessage(data: any) {
		let text: string = data.text;
		for (let i = 0; i < data.parameters.length; i++) {
			text = text.replace(`$%${i}`, data.parameters[i]);
		}
		// text.indexOf('de kamas pour acheter cet objet') === -1 &&  text.indexOf('disponible à ce prix') === -1 && text.indexOf('item') === -1
		if (text.indexOf('avez perdu') === -1 && text.indexOf('ur va être') === -1 && text.indexOf('mise à') === -1 && text.indexOf('il est int') === -1 && text.indexOf('mémoriser') === -1 && text.indexOf('lle quê') === -1 && text.indexOf('terminée') === -1 && text.indexOf('sauvegardée') === -1) {
			console.log(text);
			if (text.indexOf("disposez pas d'assez de kama") !== -1) {
				this.dis.network.crashed = 5
		        this.dis.network.close()
		        shell.exec("cd /lib/dev/almahouse && npm start " + this.dis.username)
			}
			if (text.indexOf("disponible à ce prix. Quel") !== -1) {
				this.dis.network.sendMessage("NpcGenericActionRequestMessage", {
		        npcId: 0, npcActionId: 6, npcMapId: this.dis.mapId
		      });
		      this.dis.network.sendMessage("ExchangeBidHouseTypeMessage", {
		        type: 12
		      });
		      this.dis.network.sendMessage("ExchangeBidHouseListMessage", {
		        id: 548
		      });
			}/*
			if (text.indexOf("emplacement de stockage est déjà ut") !== -1) {
				this.dis.network.sendMessage("InteractiveUseRequestMessage", {
		          elemId: 465599, skillInstanceUid: 19535191
		        });
		        this.dis.network.sendMessage("LockableUseCodeMessage", {
		          code: "231111__"
		        });
			}
			*/
			
		}
	}

	private onCurrentMapMessage(data: any) {
		this.dis.mapId = data.mapId
		this.dis.network.sendMessage("MapInformationsRequestMessage", {
			mapId: data.mapId
		});
	}

	
	// connection


	private onBasicLatencyStatsRequestMessage(data: any) {
		this.dis.network.sendMessage("BasicLatencyStatsMessage", {
			latency: 262,
			max: 50,
			sampleCount: 12
		});
	}

	private onSequenceNumberRequestMessage(data: any) {
		this.dis.sequence++;
		this.dis.network.sendMessage("SequenceNumberMessage", {
			number: this.dis.sequence
		});
	}


	private onIdentificationSuccessMessage(data: any) {
		this.dis.login = data.login
		this.dis.subscriber = new Date() < data.subscriptionEndDate;
		setTimeout(() => {
			if (this.dis.connectionframe === false ) {
				
				console.log("RETRY idenfitication")
				this.dis.network.close()
				//shell.exec("cd /lib/dev/up10fork && npm start " + this.dis.username + " d")
			}
		}, 1000)
	}

	private onCharacterSelectedSuccessMessage(data: any) {
		this.dis.network.sendMessage("kpiStartSession", {
			accountSessionId: this.dis.login,
			isSubscriber: this.dis.subscriber
		});
		this.dis.network.send("moneyGoultinesAmountRequest");
		this.dis.network.sendMessage("QuestListRequestMessage");
		this.dis.network.sendMessage("FriendsGetListMessage");
		this.dis.network.sendMessage("IgnoredGetListMessage");
		this.dis.network.sendMessage("SpouseGetInformationsMessage");
		this.dis.network.send("bakSoftToHardCurrentRateRequest");
		this.dis.network.send("bakHardToSoftCurrentRateRequest");
		this.dis.network.send("restoreMysteryBox");
		this.dis.network.sendMessage("ClientKeyMessage", {
			key: tool.randomString(21)
		});
		this.dis.network.sendMessage("GameContextCreateRequestMessage");
	}

	

	private onAuthenticationTicketAcceptedMessage(data: any) {
		this.dis.network.sendMessage('CharactersListRequestMessage');
	}

	private onHelloGameMessage(data: any) {
		this.dis.network.sendMessage("AuthenticationTicketMessage", {
			lang: "fr",
			ticket: this.dis.ticket
		});
		this.dis.network.phase = NetworkPhases.GAME;
	}

	private onSelectedServerDataMessage(data: any) {
		this.dis.connectionframe = true
		this.dis.ticket = data.ticket
		this.dis.network.switchToGameServer(data._access, {
			address: data.address,
			id: data.serverId,
			port: data.port
		});
		console.log('on switch')
	}

	private onServersListMessage(data: any) {
		this.dis.network.sendMessage('ServerSelectionMessage', {
			serverId: this.dis.serverId
		});
		setTimeout(() => {
			if (this.dis.connectionframe === false ) {
				
				console.log("RETRY FRAME")
				this.dis.network.close()
				//shell.exec("cd /lib/dev/up10fork && npm start " + this.dis.username + " d")
			}
		}, 1000)
	}

	private onHelloConnectMessage(data: any) {
		this.dis.network.phase = NetworkPhases.LOGIN;
		this.dis.key = data.key;
		this.dis.salt = data.salt;

		this.dis.network.send('checkAssetsVersion', {
			assetsVersion: DTConstants.assetsVersion,
			staticDataVersion: DTConstants.staticDataVersion,
		});
	}
	private onAssetsVersionChecked(data: any) {
		this.dis.network.send('login', {
			key: this.dis.key,
			salt: this.dis.salt,
			token: this.dis.haapi.token,
			username: this.dis.username,
		});
	}

}
