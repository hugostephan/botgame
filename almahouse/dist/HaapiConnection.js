"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const DTConstants_1 = require("./DTConstants");
const IHaapi_1 = require("./IHaapi");
class HaapiConnection {
    processHaapi(username, password, serverId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                this.haapi = yield this.createApiKey(username, password, serverId);
                if (this.haapi.reason) {
                    switch (this.haapi.reason) {
                        case IHaapi_1.HaapiErrorReasons.FAILED:
                            this.writeBan([username, serverId]);
                            return;
                        //throw new Error('Identifiants incorrects');
                        case IHaapi_1.HaapiErrorReasons.BAN:
                            this.writeBan([username, serverId]);
                            return;
                        //throw new Error('Compte banni!');
                    }
                }
                this.token = yield this.getToken();
            }
            catch (e) {
                this.writeBan([username, serverId]);
                return;
            }
        });
    }
    writeBan(result) {
        var fs = require('fs');
        fs.writeFile('./src/compteBan.txt', result, 'utf8', function (err) {
            if (err)
                return console.log(err);
        });
    }
    createApiKey(username, password, serverId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield axios_1.default.post(`${DTConstants_1.default.config.haapi.url}/Api/CreateApiKey`, 'login=' + username + '&password=' + password + '&long_life_token=false');
                return response.data;
            }
            catch (e) {
                if (e.response && e.response.status === 601) {
                    return e.response.data;
                }
                throw new Error(e.message);
            }
        });
    }
    getToken() {
        return __awaiter(this, void 0, void 0, function* () {
            const config = {
                headers: {
                    apikey: this.haapi.key,
                },
                params: {
                    game: DTConstants_1.default.config.haapi.id,
                },
            };
            try {
                const response = yield axios_1.default.get(`${DTConstants_1.default.config.haapi.url}/Account/CreateToken`, config);
                return response.data.token;
            }
            catch (e) {
                throw new Error(e.message);
            }
        });
    }
}
exports.default = HaapiConnection;
