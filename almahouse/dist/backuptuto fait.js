"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const DTConstants_1 = require("./DTConstants");
const MapPoint_1 = require("./path/MapPoint");
const frame_1 = require("./frame");
const data_1 = require("./data");
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var messageToSend = [];
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});
io.on('connection', function (socket) {
    console.log('new man');
    socket.on('disconnect', function () {
        console.log('socket disconnect');
        socket.disconnect();
    });
    socket.on('chatResponse', function (characterId, receiver, content) {
        messageToSend.push({
            characterId: characterId,
            receiver: receiver,
            content: content
        });
    });
});
http.listen(3000, function () {
    console.log('listening on *:3000');
});
function start(username, password) {
    return __awaiter(this, void 0, void 0, function* () {
        let Data = new data_1.default();
        let Frame;
        function main() {
            return __awaiter(this, void 0, void 0, function* () {
                yield DTConstants_1.default.Init();
                yield Data.init(username, password);
                MapPoint_1.default.Init();
                Frame = new frame_1.default(Data, io);
            });
        }
        setInterval(function () {
            for (var i of messageToSend) {
                if (parseInt(i.characterId) === Data.characterId) {
                    Data.network.sendMessage("ChatClientPrivateMessage", {
                        content: i.content,
                        receiver: i.receiver
                    });
                    messageToSend = [];
                }
            }
        }, 250);
        main();
    });
}
function s() {
    return __awaiter(this, void 0, void 0, function* () {
        //await tool.wait(5000)
        start('dnyxvjb5t5', 'azerty321');
        //await tool.wait(15000)
        //start('70v04zw46v', 'azerty321')
    });
}
s();
