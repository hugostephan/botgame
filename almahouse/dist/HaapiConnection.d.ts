import { IHaapi } from './IHaapi';
export default class HaapiConnection {
    haapi: IHaapi;
    token: string;
    processHaapi(username: string, password: string, serverId: string): Promise<void>;
    private writeBan;
    private createApiKey;
    private getToken;
}
