export default class CellPath {
    i: number;
    j: number;
    w: number;
    d: number;
    path: CellPath;
    constructor(i: number, j: number, w: number, d: number, path: CellPath);
}
