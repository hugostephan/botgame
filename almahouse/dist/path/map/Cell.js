"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Cell {
    constructor(l, f, c, s, z) {
        this.l = l;
        this.f = f;
        this.c = c;
        this.s = s;
        this.z = z;
    }
    isWalkable(isFightMode = false) {
        return (this.l & (isFightMode ? 5 : 1)) === 1;
    }
    isFarmCell() {
        return (this.l & 32) === 32;
    }
    isVisible() {
        return (this.l & 64) === 64;
    }
    isObstacle() {
        return (this.l & 2) !== 2;
    }
}
exports.default = Cell;
