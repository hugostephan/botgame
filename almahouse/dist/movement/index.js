"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("../path");
const MapsManager_1 = require("../path/map/MapsManager");
const enums_1 = require("./enums");
class movement {
    constructor() {
        this.pathFinder = new path_1.default();
    }
    setMap(mapId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield MapsManager_1.default.getMap(mapId);
            this.pathFinder.setMap(data);
        });
    }
    moveToCell(current, cellId, stopNextToTarget = false) {
        if (cellId < 0 || cellId > 560) {
            console.log("cell id peut pas aller plus 7854");
            return enums_1.MovementRequestResults.FAILED;
        }
        const path = this.pathFinder.getPath(current, cellId, [], true, stopNextToTarget);
        return path;
    }
}
exports.default = movement;
