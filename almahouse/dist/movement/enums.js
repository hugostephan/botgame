"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MapChangeDirections;
(function (MapChangeDirections) {
    MapChangeDirections[MapChangeDirections["NONE"] = 0] = "NONE";
    MapChangeDirections[MapChangeDirections["Left"] = 56] = "Left";
    MapChangeDirections[MapChangeDirections["Right"] = 131] = "Right";
    MapChangeDirections[MapChangeDirections["Top"] = 224] = "Top";
    MapChangeDirections[MapChangeDirections["Bottom"] = 14] = "Bottom";
    MapChangeDirections[MapChangeDirections["Gauche"] = 56] = "Gauche";
    MapChangeDirections[MapChangeDirections["Droite"] = 131] = "Droite";
    MapChangeDirections[MapChangeDirections["Haut"] = 224] = "Haut";
    MapChangeDirections[MapChangeDirections["Bas"] = 14] = "Bas";
})(MapChangeDirections = exports.MapChangeDirections || (exports.MapChangeDirections = {}));
var MovementRequestResults;
(function (MovementRequestResults) {
    MovementRequestResults[MovementRequestResults["MOVED"] = 0] = "MOVED";
    MovementRequestResults[MovementRequestResults["ALREADY_THERE"] = 1] = "ALREADY_THERE";
    MovementRequestResults[MovementRequestResults["FAILED"] = 2] = "FAILED";
    MovementRequestResults[MovementRequestResults["PATH_BLOCKED"] = 3] = "PATH_BLOCKED";
})(MovementRequestResults = exports.MovementRequestResults || (exports.MovementRequestResults = {}));
