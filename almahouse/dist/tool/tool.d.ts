export declare function randomString(len: number, bits?: number): string;
export declare function generatePassword(lowercase: number, uppercase: number, numerics: number): string;
export declare function getRandomInt(min: number, max: number): number;
export declare function capitalize(str: string): string;
export declare function insertAt(main: string, index: number, text: string): string;
export declare function readableString(length: number): string;
export declare function wait(milleseconds: any): Promise<{}>;
