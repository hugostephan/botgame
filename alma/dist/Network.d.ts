import Dispatcher from './Dispatcher';
import { NetworkPhases } from './NetworkPhases';
export default class Network {
    connected: boolean;
    phase: NetworkPhases;
    private sessionId;
    private socket;
    private server;
    private dispatcher;
    constructor(dispatcher: Dispatcher);
    connect(sessionId: string, url: string): void;
    close(): void;
    send(call: string, data?: any): void;
    switchToGameServer(url: string, server: any): void;
    sendMessage(messageName: string, data?: any): void;
    private setCurrentConnection;
    private makeSticky;
    private createSocket;
}
