export default class MapPoint {
    static cells: MapPoint[];
    x: number;
    y: number;
    cellId: number;
    constructor(cellId: number, x: number, y: number);
    static Init(): void;
    static fromCellId(cellId: number): MapPoint;
    static fromCoords(x: number, y: number): MapPoint;
    static getNeighbourCells(cellId: number, allowDiagonal: boolean): MapPoint[];
    distanceTo(destination: MapPoint): number;
    distanceToCell(destination: MapPoint): number;
}
