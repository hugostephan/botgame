"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function randomString(len, bits = 36) {
    let outStr = "";
    let newStr;
    while (outStr.length < len) {
        newStr = Math.random()
            .toString(bits)
            .slice(2);
        outStr += newStr.slice(0, Math.min(newStr.length, len - outStr.length));
    }
    return outStr.toUpperCase();
}
exports.randomString = randomString;
function generatePassword(lowercase, uppercase, numerics) {
    const lowers = "abcdefghijklmnopqrstuvwxyz";
    const uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const numbers = "0123456789";
    let generated = "!";
    for (let i = 1; i <= lowercase; i++) {
        generated = insertAt(generated, getRandomInt(0, generated.length), lowers.charAt(getRandomInt(0, lowers.length - 1)));
    }
    for (let i = 1; i <= uppercase; i++) {
        generated = insertAt(generated, getRandomInt(0, generated.length), uppers.charAt(getRandomInt(0, uppers.length - 1)));
    }
    for (let i = 1; i <= numerics; i++) {
        generated = insertAt(generated, getRandomInt(0, generated.length), numbers.charAt(getRandomInt(0, numbers.length - 1)));
    }
    return generated.replace("!", "");
}
exports.generatePassword = generatePassword;
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
exports.getRandomInt = getRandomInt;
function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}
exports.capitalize = capitalize;
function insertAt(main, index, text) {
    return main.substr(0, index) + text + main.substr(index);
}
exports.insertAt = insertAt;
function readableString(length) {
    const VOWELS = "aeiouy".split("");
    const CONSONANTS = "bcdfghjklmnprstvwxz".split("");
    const VOWELS_LENGTH = VOWELS.length;
    const CONSONANTS_LENGTH = CONSONANTS.length;
    let randomstring = "";
    const salt = Math.floor(Math.random() * 2);
    for (let i = length + salt, end = 0 + salt; i > end; i -= 1) {
        if (i % 2 === 0) {
            randomstring += CONSONANTS[Math.floor(Math.random() * CONSONANTS_LENGTH)];
        }
        else {
            randomstring += VOWELS[Math.floor(Math.random() * VOWELS_LENGTH)];
        }
    }
    return randomstring;
}
exports.readableString = readableString;
function wait(milleseconds) {
    return new Promise(resolvetime => setTimeout(resolvetime, milleseconds));
}
exports.wait = wait;
