const shell = require('shelljs');
import Dispatcher from './Dispatcher';
import DTConstants from './DTConstants';
import * as tool from './tool/tool';
import { NetworkPhases } from './NetworkPhases';
const Primus = require("primus"); // tslint:disable-line
     
export default class Network {

  public connected: boolean = false;
  public phase: NetworkPhases; 
  public crashed = 0;
  public username = "bertrand"

  private sessionId: string;
  private socket: any;
  private server: any;
  private dispatcher: Dispatcher;

  constructor(dispatcher: Dispatcher, username: any) {
    this.dispatcher = dispatcher;
    this.phase = NetworkPhases.NONE;
    this.username = username
  }

  public connect(sessionId: string, url: string) {
    if (this.connected || this.phase !== NetworkPhases.NONE) {
      return;
    }
    this.sessionId = sessionId;
    const currentUrl = this.makeSticky(url, this.sessionId);
    this.socket = this.createSocket(currentUrl);
    this.setCurrentConnection();
    this.socket.open();
  }

  public close() {
    if (!this.connected) {
      return;
    }

    if (this.socket) {
      this.socket.destroy();
    }
  }

  public send(call: string, data?: any) {
    if (!this.connected) {
      return;
    }

    let msg;
    let msgName;

    if (call === 'sendMessage') {
      msgName = data.type;
      if (data.data) {
        msg = {call, data};
      } else {
        msg = {call, data: {type: data.type}};
      }
    } else {
      msgName = call;
      if (data) {
        msg = {call, data};
      } else {
        msg = {call};
      }
    }

    //console.log('Sent', msg.data && msg.data.type ? msg.data.type : msg.call);
    //console.log(msg)
    this.socket.write(msg);
  }
  public switchToGameServer(url: string, server: any) {
    this.server = server;
    if (!this.connected || this.phase !== NetworkPhases.LOGIN) {
      return;
    }
    this.phase = NetworkPhases.SWITCHING_TO_GAME;
    this.send("disconnecting", "SWITCHING_TO_GAME");
    this.socket.destroy();
    const currentUrl = this.makeSticky(url, this.sessionId);    
    this.socket = this.createSocket(currentUrl);
    this.setCurrentConnection();
    this.socket.open();
  }

  public sendMessage(messageName: string, data?: any) {
    this.send('sendMessage', { type: messageName, data });
  }

  private setCurrentConnection() {
    this.socket.on('open', () => {
      this.connected = true;

      if (this.phase === NetworkPhases.NONE) {
        this.send('connecting', {
          appVersion: DTConstants.appVersion,
          buildVersion: DTConstants.buildVersion,
          client: 'android',
          language: 'fr',
          server: 'login',
        });
      } else {
        this.send('connecting', {
          appVersion: DTConstants.appVersion,
          buildVersion: DTConstants.buildVersion,
          client: 'android',
          language: 'fr',
          server: this.server,
        });
      }
    });

    this.socket.on('data', (data: any) => {
      //console.log('Received', data._messageType);
      // this.onMessageReceived.trigger({type: data._messageType, data});
      this.dispatcher.emit(data._messageType, data);
    });

    this.socket.on('error', (err: Error) => {
      console.log("ERROR")
      console.log(err.message);
    });

    this.socket.on('reconnect', (opts: any) => {
      console.log('primus reconnect');
    });

    this.socket.on('close', () => {
      console.log('---------------------------------- primus close ---------------------');

      this.connected = false;

      if (this.phase === NetworkPhases.SWITCHING_TO_GAME) {
      } else if (this.crashed === 0) {    
        async function reco(username) {

          await tool.wait(5000)
          shell.exec("cd /lib/dev/copopo && npm start " + username + " d")

        }
        if (this.username !== "bertrand") {
          reco(this.username)
        }
        
        console.log("CRASH PATCHED -->", this.crashed)
      } else {
        console.log("CRASHHHHHED -->", this.crashed)
        this.phase = NetworkPhases.NONE;
      }
    });

    this.socket.on('destroy', () => {
      console.log('primus destroyed');
    });
  }

  private makeSticky(url: string, sessionId: string): string {
    const seperator = url.indexOf('?') === -1 ? '?' : '&';
    return url + seperator + 'STICKER' + '=' + encodeURIComponent(sessionId);
  }

  private createSocket(url: string) {
    const Socket = Primus.createSocket({
      transformer: 'engine.io',
    });
    return new Socket(url, {
      manual: true,
      reconnect: {
        max: 3000,
        min: 1000,
        retries: 10,
      },
      strategy: 'disconnect,timeout',
    });
  }
}
