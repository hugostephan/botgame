"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CellData {
    constructor(i, j) {
        this.floor = -1;
        this.zone = -1;
        this.speed = 1;
        this.weight = 0;
        this.candidateRef = null;
        this.i = i;
        this.j = j;
    }
}
exports.default = CellData;
