import MapPoint from "./MapPoint";
import Map from "./map";
import Cell from "./map/Cell";
export default class Pathfinder {
    private readonly OCCUPIED_CELL_WEIGHT;
    private readonly ELEVATION_TOLERANCE;
    private readonly WIDTH;
    private readonly HEIGHT;
    private firstCellZone;
    private grid;
    private oldMovementSystem;
    constructor();
    setMap(map: Map): void;
    getPath(source: number, target: number, occupiedCells: number[], allowDiagonal: boolean, stopNextToTarget: boolean): number[];
    compressPath(path: number[]): number[];
    normalizePath(path: number[]): number[];
    getAccessibleCells(i: number, j: number): MapPoint[];
    updateCellPath(cellId: number, cell: Cell): void;
    expandPath(path: number[]): number[];
    private checkPath;
    private updateCellPath2;
    private areCommunicating;
    private canMoveDiagonnalyTo;
    private addCandidate;
    private addCandidates;
    private getAdjacentCells;
}
