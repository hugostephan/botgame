"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
class DTConstants {
    static Init() {
        return __awaiter(this, void 0, void 0, function* () {
            this.config = yield DTConstants.getConfig();
            const m = yield DTConstants.getAssetsVersions();
            this.assetsVersion = m.assetsVersion;
            this.staticDataVersion = m.staticDataVersion;
            this.appVersion = yield DTConstants.getAppVersion();
            this.buildVersion = yield DTConstants.getBuildVersion();
        });
    }
    static getConfig() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.get(`${DTConstants.MAIN_URL}/config.json`);
            return response.data;
        });
    }
    static getAssetsVersions() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.get(`${DTConstants.MAIN_URL}/assetsVersions.json`);
            return response.data;
        });
    }
    static getAppVersion() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.get('https://itunes.apple.com/lookup', {
                params: {
                    country: 'fr',
                    id: 1041406978,
                    lang: 'fr',
                    limit: 1,
                    t: Date.now(),
                },
            });
            return response.data.results[0].version;
        });
    }
    static getBuildVersion() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios_1.default.get(`${DTConstants.MAIN_URL}/build/script.js`);
            const regex = /.*buildVersion=("|')([0-9]*\.[0-9]*\.[0-9]*)("|')/g;
            const m = regex.exec(response.data.substring(1, 10000));
            return m[2];
        });
    }
}
DTConstants.MAIN_URL = 'https://proxyconnection.touch.dofus.com';
exports.default = DTConstants;
