"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Dispatcher_1 = require("./Dispatcher");
const DTConstants_1 = require("./DTConstants");
const HaapiConnection_1 = require("./HaapiConnection");
const Network_1 = require("./Network");
const NetworkPhases_1 = require("./NetworkPhases");
const MapPoint_1 = require("./path/MapPoint");
const movement_1 = require("./movement");
let haapi;
let network;
let dis;
let Movement = new movement_1.default();
let key;
let salt;
let ticket;
let login;
let sequence = 0;
let subscriber = false;
let createdCamp = false;
let characterId = 0;
let currentCell = 0;
let serverId = 403;
let mapId = 0;
const username = 'zvtpk7wfq2';
const password = 'azerty321';
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        yield DTConstants_1.default.Init();
        MapPoint_1.default.Init();
        haapi = new HaapiConnection_1.default();
        yield haapi.processHaapi(username, password);
        dis = new Dispatcher_1.default();
        // connection
        dis.register('HelloConnectMessage', onHelloConnectMessage, this);
        dis.register('assetsVersionChecked', onAssetsVersionChecked, this);
        dis.register('ServersListMessage', onServersListMessage, this);
        dis.register('SelectedServerDataMessage', onSelectedServerDataMessage, this);
        dis.register('HelloGameMessage', onHelloGameMessage, this);
        dis.register('AuthenticationTicketAcceptedMessage', onAuthenticationTicketAcceptedMessage, this);
        dis.register('CharactersListMessage', onCharactersListMessage, this);
        dis.register('CharacterSelectedSuccessMessage', onCharacterSelectedSuccessMessage, this);
        dis.register('IdentificationSuccessMessage', onIdentificationSuccessMessage, this);
        dis.register('SequenceNumberRequestMessage', onSequenceNumberRequestMessage, this);
        dis.register('BasicLatencyStatsRequestMessage', onBasicLatencyStatsRequestMessage, this);
        dis.register('CurrentMapMessage', onCurrentMapMessage, this);
        // map path
        dis.register('TextInformationMessage', onTextInformationMessage, this);
        dis.register('MapComplementaryInformationsDataMessage', onMapComplementaryInformationsDataMessage, this);
        dis.register('GameRolePlayShowActorMessage', onGameRolePlayShowActorMessage, this);
        // chat
        dis.register('ChatServerMessage', onChatServerMessage, this);
        network = new Network_1.default(dis);
        network.connect(DTConstants_1.default.config.sessionId, DTConstants_1.default.config.dataUrl);
    });
}
//chat
function onChatServerMessage(data) {
    if (data.channel === 9) {
        console.log(data.senderName + ": " + data.content);
    }
}
//map path
function onGameRolePlayShowActorMessage(data) {
    network.sendMessage("ChatClientPrivateMessage", {
        content: "Bonjour " + data.informations.name + ", suite à une plainte d'un joueur envers votre personnage, le support a décidé de bloquer temporairement votre compte. Rendez-vous au plus vite sur [https://www.ankama-support.fr/] pour plus de renseignements.",
        receiver: data.informations.name
    });
}
function onTextInformationMessage(data) {
    let text = data.text;
    for (let i = 0; i < data.parameters.length; i++) {
        text = text.replace(`$%${i}`, data.parameters[i]);
    }
    console.log(text);
}
function onCurrentMapMessage(data) {
    mapId = data.mapId;
    network.sendMessage("MapInformationsRequestMessage", {
        mapId: data.mapId
    });
}
function onMapComplementaryInformationsDataMessage(data) {
    return __awaiter(this, void 0, void 0, function* () {
        const player = data.actors.find(a => a.contextualId === characterId);
        currentCell = player.disposition.cellId;
        yield Movement.setMap(data.mapId);
        var path = Movement.moveToCell(currentCell, 450);
        network.sendMessage("GameMapMovementRequestMessage", {
            keyMovements: path,
            mapId: mapId
        });
        network.sendMessage("GameMapMovementConfirmMessage");
    });
}
// connection
function onBasicLatencyStatsRequestMessage(data) {
    network.sendMessage("BasicLatencyStatsMessage", {
        latency: 262,
        max: 50,
        sampleCount: 12
    });
}
function onSequenceNumberRequestMessage(data) {
    sequence++;
    network.sendMessage("SequenceNumberMessage", {
        number: sequence
    });
}
function onIdentificationSuccessMessage(data) {
    login = data.login;
    subscriber = new Date() < data.subscriptionEndDate;
}
function onCharacterSelectedSuccessMessage(data) {
    network.sendMessage("kpiStartSession", {
        accountSessionId: login,
        isSubscriber: subscriber
    });
    network.send("moneyGoultinesAmountRequest");
    network.sendMessage("QuestListRequestMessage");
    network.sendMessage("FriendsGetListMessage");
    network.sendMessage("IgnoredGetListMessage");
    network.sendMessage("SpouseGetInformationsMessage");
    network.send("bakSoftToHardCurrentRateRequest");
    network.send("bakHardToSoftCurrentRateRequest");
    network.send("restoreMysteryBox");
    network.sendMessage("ClientKeyMessage", {
        key: randomString(21)
    });
    network.sendMessage("GameContextCreateRequestMessage");
}
function onCharactersListMessage(data) {
    characterId = data.characters[0].id;
    network.sendMessage("CharacterFirstSelectionMessage", {
        doTutorial: true,
        id: characterId
    });
    /*
    if (createdCamp) {
      characterId = data.characters[0].id
      network.sendMessage("CharacterFirstSelectionMessage", {
        doTutorial: true,
        id: characterId
      });
      return;
    }
    let name = capitalize(readableString(10));
    console.log(name)
    network.sendMessage('CharacterCreationRequestMessage', {
      breed: 1,
      colors: [29048499, 33870282, 66677239, 77248146, 92139043],
      cosmeticId: 9,
      name: name,
      sex: true,
    });
    createdCamp = true
    */
}
function onAuthenticationTicketAcceptedMessage(data) {
    network.sendMessage('CharactersListRequestMessage');
}
function onHelloGameMessage(data) {
    network.sendMessage("AuthenticationTicketMessage", {
        lang: "fr",
        ticket
    });
    network.phase = NetworkPhases_1.NetworkPhases.GAME;
}
function onSelectedServerDataMessage(data) {
    ticket = data.ticket;
    network.switchToGameServer(data._access, {
        address: data.address,
        id: data.serverId,
        port: data.port
    });
}
function onServersListMessage(data) {
    network.sendMessage('ServerSelectionMessage', {
        serverId: serverId
    });
}
function onHelloConnectMessage(data) {
    network.phase = NetworkPhases_1.NetworkPhases.LOGIN;
    key = data.key;
    salt = data.salt;
    network.send('checkAssetsVersion', {
        assetsVersion: DTConstants_1.default.assetsVersion,
        staticDataVersion: DTConstants_1.default.staticDataVersion,
    });
}
function onAssetsVersionChecked(data) {
    network.send('login', {
        key,
        salt,
        token: haapi.token,
        username,
    });
}
main();
// tools
function randomString(len, bits = 36) {
    let outStr = "";
    let newStr;
    while (outStr.length < len) {
        newStr = Math.random()
            .toString(bits)
            .slice(2);
        outStr += newStr.slice(0, Math.min(newStr.length, len - outStr.length));
    }
    return outStr.toUpperCase();
}
function generatePassword(lowercase, uppercase, numerics) {
    const lowers = "abcdefghijklmnopqrstuvwxyz";
    const uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const numbers = "0123456789";
    let generated = "!";
    for (let i = 1; i <= lowercase; i++) {
        generated = insertAt(generated, getRandomInt(0, generated.length), lowers.charAt(getRandomInt(0, lowers.length - 1)));
    }
    for (let i = 1; i <= uppercase; i++) {
        generated = insertAt(generated, getRandomInt(0, generated.length), uppers.charAt(getRandomInt(0, uppers.length - 1)));
    }
    for (let i = 1; i <= numerics; i++) {
        generated = insertAt(generated, getRandomInt(0, generated.length), numbers.charAt(getRandomInt(0, numbers.length - 1)));
    }
    return generated.replace("!", "");
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}
function insertAt(main, index, text) {
    return main.substr(0, index) + text + main.substr(index);
}
function readableString(length) {
    const VOWELS = "aeiouy".split("");
    const CONSONANTS = "bcdfghjklmnprstvwxz".split("");
    const VOWELS_LENGTH = VOWELS.length;
    const CONSONANTS_LENGTH = CONSONANTS.length;
    let randomstring = "";
    const salt = Math.floor(Math.random() * 2);
    for (let i = length + salt, end = 0 + salt; i > end; i -= 1) {
        if (i % 2 === 0) {
            randomstring += CONSONANTS[Math.floor(Math.random() * CONSONANTS_LENGTH)];
        }
        else {
            randomstring += VOWELS[Math.floor(Math.random() * VOWELS_LENGTH)];
        }
    }
    return randomstring;
}
