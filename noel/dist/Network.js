"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DTConstants_1 = require("./DTConstants");
const NetworkPhases_1 = require("./NetworkPhases");
const Primus = require("primus"); // tslint:disable-line
class Network {
    constructor(dispatcher) {
        this.connected = false;
        this.dispatcher = dispatcher;
        this.phase = NetworkPhases_1.NetworkPhases.NONE;
    }
    connect(sessionId, url) {
        if (this.connected || this.phase !== NetworkPhases_1.NetworkPhases.NONE) {
            return;
        }
        this.sessionId = sessionId;
        const currentUrl = this.makeSticky(url, this.sessionId);
        this.socket = this.createSocket(currentUrl);
        this.setCurrentConnection();
        this.socket.open();
    }
    close() {
        if (!this.connected) {
            return;
        }
        if (this.socket) {
            this.socket.destroy();
        }
    }
    send(call, data) {
        if (!this.connected) {
            return;
        }
        let msg;
        let msgName;
        if (call === 'sendMessage') {
            msgName = data.type;
            if (data.data) {
                msg = { call, data };
            }
            else {
                msg = { call, data: { type: data.type } };
            }
        }
        else {
            msgName = call;
            if (data) {
                msg = { call, data };
            }
            else {
                msg = { call };
            }
        }
        //console.log('Sent', msg.data && msg.data.type ? msg.data.type : msg.call);
        //console.log(msg)
        this.socket.write(msg);
    }
    switchToGameServer(url, server) {
        this.server = server;
        if (!this.connected || this.phase !== NetworkPhases_1.NetworkPhases.LOGIN) {
            return;
        }
        this.phase = NetworkPhases_1.NetworkPhases.SWITCHING_TO_GAME;
        this.send("disconnecting", "SWITCHING_TO_GAME");
        this.socket.destroy();
        const currentUrl = this.makeSticky(url, this.sessionId);
        this.socket = this.createSocket(currentUrl);
        this.setCurrentConnection();
        this.socket.open();
    }
    sendMessage(messageName, data) {
        this.send('sendMessage', { type: messageName, data });
    }
    setCurrentConnection() {
        this.socket.on('open', () => {
            this.connected = true;
            //console.log('Je suis connecté!');
            if (this.phase === NetworkPhases_1.NetworkPhases.NONE) {
                this.send('connecting', {
                    appVersion: DTConstants_1.default.appVersion,
                    buildVersion: DTConstants_1.default.buildVersion,
                    client: 'android',
                    language: 'fr',
                    server: 'login',
                });
            }
            else {
                this.send('connecting', {
                    appVersion: DTConstants_1.default.appVersion,
                    buildVersion: DTConstants_1.default.buildVersion,
                    client: 'android',
                    language: 'fr',
                    server: this.server,
                });
            }
        });
        this.socket.on('data', (data) => {
            //console.log('Received', data._messageType);
            // this.onMessageReceived.trigger({type: data._messageType, data});
            this.dispatcher.emit(data._messageType, data);
        });
        this.socket.on('error', (err) => {
            console.log(err.message);
        });
        this.socket.on('reconnect', (opts) => {
            console.log('primus reconnect');
        });
        this.socket.on('close', () => {
            console.log('primus close');
            this.connected = false;
            if (this.phase === NetworkPhases_1.NetworkPhases.SWITCHING_TO_GAME) {
                //
            }
            else {
                this.phase = NetworkPhases_1.NetworkPhases.NONE;
            }
        });
        this.socket.on('destroy', () => {
            console.log('primus destroyed');
        });
    }
    makeSticky(url, sessionId) {
        const seperator = url.indexOf('?') === -1 ? '?' : '&';
        return url + seperator + 'STICKER' + '=' + encodeURIComponent(sessionId);
    }
    createSocket(url) {
        const Socket = Primus.createSocket({
            transformer: 'engine.io',
        });
        return new Socket(url, {
            manual: true,
            reconnect: {
                max: 5000,
                min: 500,
                retries: 10,
            },
            strategy: 'disconnect,timeout',
        });
    }
}
exports.default = Network;
