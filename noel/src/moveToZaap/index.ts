const shell = require('shelljs');

import data from './../data';
import * as tool from '../tool/tool';
import accConnect from "../.";
import connect from '../index';

export async function moveToZaap(Data: data) { 
  function go(cellToGo, mapToGo, mapGosth) {
    var path = Data.Movement.moveToCell(Data.currentCell, cellToGo);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("ChangeMapMessage", {
      mapId: mapGosth
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: Data.mapId
    });
    console.log(Data.mapId + ' ==> ' + mapToGo)
  }

  // top 15 13 23 10 27
  //left 420 266 140
  // bottom 556
  // right 69 335 517

  return new Promise(async (resolve, reject) => {
    if (Data.mapId === 66063617) {
      console.log("OKKKKKK")
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66063617
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11804
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66063617
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11802
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11801
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(556, 66063360, 66063360)
    } else if (Data.mapId === 66063360) { // premier
      go(556, 66063361, 66063361)
    } else if (Data.mapId === 66063361) { 
      go(420, 66062849, 66062849)
    } else if (Data.mapId === 66062849) { 
      go(551, 66062850, 66062850)
    } else if (Data.mapId === 66062850) { 
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66063617
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11804
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66062850
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11802
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11801
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(556, 66062851, 66062851)
    } else if (Data.mapId === 66062851) { 
      go(420, 66062339, 66062339)
    } else if (Data.mapId === 66062339) { // finir
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66062339
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11804
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66062339
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11802
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11801
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(420, 66061827, 66061827)
    } else if (Data.mapId === 66061827) { 
      go(266, 66061315, 66061315)
    } else if (Data.mapId === 66061315) { 
      go(420, 66060803, 66060803)
    } else if (Data.mapId === 66060803) { 
      go(556, 66060804, 66060804)
    } else if (Data.mapId === 66060804) { 
      go(556, 66060805, 66060805)
    } else if (Data.mapId === 66060805) { 
      go(140, 66060293, 66060293)
    } else if (Data.mapId === 66060293) { 
      go(140, 66191877, 66191877)
    } else if (Data.mapId === 66191877) { // deuxieme
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66191877
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11809
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(140, 66192389, 66192389)
    } else if (Data.mapId === 66192389) { 
      go(13, 66192388, 66192388)
    } else if (Data.mapId === 66192388) { 
      go(140, 66192900, 66192900)
    } else if (Data.mapId === 66192900) { 
      go(266, 66193412, 66193412)
    } else if (Data.mapId === 66193412) { // deuxieme
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66193412
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11807
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(23, 66193411, 66193411)
    } else if (Data.mapId === 66193411) { 
      go(140, 66193923, 66193923)
    } else if (Data.mapId === 66193923) { // deuxieme
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66193923
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11809  
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(10, 66193922, 66193922)
    } else if (Data.mapId === 66193922) { 
      go(27, 66193921, 66193921)
    } else if (Data.mapId === 66193921) { 
      go(21, 66193920, 66193920)
    } else if (Data.mapId === 66193920) { 
      go(22, 66194177, 66194177)
    } else if (Data.mapId === 66194177) { 
      go(69, 66193665, 66193665)
    } else if (Data.mapId === 66193665) { 
      go(10, 66193666, 66193666)
    } else if (Data.mapId === 66193666) { 
      go(23, 66193667, 66193667)
    } else if (Data.mapId === 66193667) { 
      go(27, 66193668, 66193668)
    } else if (Data.mapId === 66193668) { 
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66193668
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11813
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(335, 66193156, 66193156)
    } else if (Data.mapId === 66193156) { 
      go(517, 66192644, 66192644)
    } else if (Data.mapId === 66192644) { 
      go(23, 66192645, 66192645)
    } else if (Data.mapId === 66192645) { 
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66192645
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11811
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(335, 66192133, 66192133)
    } else if (Data.mapId === 66192133) { 
      go(335, 66060549, 66060549)
    } else if (Data.mapId === 66060549) { 
      go(335, 66061061, 66061061)
    } else if (Data.mapId === 66061061) { // troisieme
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66061061
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11804
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(545, 66061573, 66061573)
    } else if (Data.mapId === 66061573) { 
      go(551, 66061572, 66061572)
    } else if (Data.mapId === 66061572) { 
      go(544, 66061571, 66061571)
    } else if (Data.mapId === 66061571) { 
      go(538, 66061570, 66061570)
    } else if (Data.mapId === 66061570) { 
      go(538, 66061569, 66061569)
    } else if (Data.mapId === 66061569) { 
      go(517, 66062081, 66062081)
    } else if (Data.mapId === 66062081) { 
      go(517, 66062593, 66062593)
    } else if (Data.mapId === 66062593) { 
      go(517, 66063105, 66063105)
    } else if (Data.mapId === 66063105) { 
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66063105
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11804
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -1, 
        npcActionId: 3, 
        npcMapId: 66063105
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11802
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 11801
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 857
      });
      Data.network.sendMessage("LeaveDialogRequestMessage");
      go(517, 66063617, 66063617)
    } else {
      console.log("perduuuuuu", Data.mapId)
    }
  });
}
  
/*

//process.exit() // TU VAS PAS M'AIMER POPEYE
      //console.log(Data.username, Data.password, Data.serverId, true, 0)
      //new connect(Data.username, Data.password, Data.serverId, true, 0)
      //new accConnect(Data.username, Data.password, Data.serverId, true, 0);
      //Data.started = true


      */