import Dispatcher from './Dispatcher';
import DTConstants from './DTConstants';
import HaapiConnection from './HaapiConnection';
import Network from './Network';
import { NetworkPhases } from './NetworkPhases';

import MapPoint from './path/MapPoint';
import movement from './movement';

import * as tool from './tool/tool';
import frame from './frame';
import data from './data';

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var messageToSend = []

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {  
  console.log('new man')
  socket.on('disconnect', function() {
    console.log('socket disconnect')
    socket.disconnect();
  });
  socket.on('chatResponse', function(characterId, receiver, content) {
    messageToSend.push({
      characterId: characterId,
      receiver: receiver,
      content: content
    })
  })
});

http.listen(3000, function() {
  console.log('listening on *:3000');
});


async function start(username, password) {
  let Data = new data();  
  let Frame: frame;
  async function main() {
    await DTConstants.Init();
    await Data.init(username, password)
    MapPoint.Init()   
    Frame = new frame(Data, io);
  }
  
  setInterval(function() {
    for (var i of messageToSend) {
      if (parseInt(i.characterId) === Data.characterId) {
        Data.network.sendMessage("ChatClientPrivateMessage", {
            content: i.content,
            receiver: i.receiver
         });
         messageToSend = []
       }
    }
  }, 250); 

  main();
}
async function s() {
  //await tool.wait(5000)
  start('dnyxvjb5t5', 'azerty321')
  //await tool.wait(15000)
  //start('70v04zw46v', 'azerty321')
}
s()
