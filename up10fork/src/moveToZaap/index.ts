const shell = require('shelljs');

import data from './../data';
import * as tool from '../tool/tool';
import accConnect from "../.";
import connect from '../index';

export async function moveToZaap(Data: data) { 
  function go(cellToGo, mapToGo, mapGosth) {
    var path = Data.Movement.moveToCell(Data.currentCell, cellToGo);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("ChangeMapMessage", {
      mapId: mapGosth
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: Data.mapId
    });
    //console.log(Data.mapId + ' ==> ' + mapToGo)
  }
  async function acheterMarchand(){
    Data.network.sendMessage("AchievementRewardRequestMessage", {
      achievementId: -1
    });
    Data.network.sendMessage("ExchangeOnHumanVendorRequestMessage", {
      humanVendorId: Data.marchandCharacterId, humanVendorCell: Data.marchandCharacterCell
    });
    await tool.wait(1500)
    Data.network.sendMessage("ExchangeBuyMessage", {
      objectToBuyId: Data.marchandObjectId, quantity: Data.marchandQuantity
    });

    Data.network.sendMessage("LeaveDialogRequestMessage");
    console.log('la ici', Data.mapId)
    go(552, 84806915,84806915 )
    if (Data.marchandObjectId !== 0){
      console.log('ITEM ACHETER', Data.marchandObjectId)
    } else {
      console.log('PAS AstrubCHETER ITEM ')
      Data.network.crashed = 5
      Data.network.close()
      shell.exec("cd /lib/dev/up10fork && npm start " + Data.username)

    }
  }
  function goIn(){
    var path = Data.Movement.moveToCell(Data.currentCell, 316);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("InteractiveUseRequestMessage", {
      elemId: 475660, skillInstanceUid: 17480380
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 101450251
    });
  }
  function prendreVieux(){
    console.log('PARLER VIEUX')
    Data.network.sendMessage("NpcGenericActionRequestMessage", {      npcId: -2, npcActionId: 3, npcMapId: 101450251    });
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13557});
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13556});
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13555});
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13554});
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13552});
    Data.network.sendMessage("QuestStepInfoRequestMessage", {questId: 954});
    for (var i of Data.almaVieux) {
      Data.network.sendMessage("NpcDialogReplyMessage", {replyId: i});
    }
    Data.network.sendMessage("QuestStepInfoRequestMessage", {questId: Data.almaQuestId});//
    Data.network.sendMessage("NpcGenericActionRequestMessage", {      npcId: -2, npcActionId: 3, npcMapId: 101450251    });
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13563});
    Data.network.sendMessage("QuestStepInfoRequestMessage", {questId: Data.almaQuestId});//
    Data.network.sendMessage("NpcDialogReplyMessage", {replyId: 13562});
    Data.network.sendMessage("LeaveDialogRequestMessage")

    console.log('FINI PARLER VIEUX')
    goDeuxiemeSalle()
  }
  async function goDeuxiemeSalle(){
    await tool.wait(1000)
    console.log('GO MAP 2')
    var path = Data.Movement.moveToCell(Data.currentCell, 274);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("InteractiveUseRequestMessage", {
      elemId: 476222, skillInstanceUid: 17486740
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 101452297
    });
  }
  function prendrePorte(){
    console.log('GO PIERRE')
    var path = Data.Movement.moveToCell(Data.currentCell, 374);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("InteractiveUseRequestMessage", {
      elemId: 476220, skillInstanceUid: 17486760
    });
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 101450245
    });
  }
  function parlerPierre(){
    var path = Data.Movement.moveToCell(Data.currentCell, 398);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.currentCell = 398
    Data.network.sendMessage("InteractiveUseRequestMessage", {
      elemId: 475747, skillInstanceUid: 17486744
    });
    Data.network.sendMessage("QuestStepInfoRequestMessage", {
      questId: Data.almaQuestId
    });
    console.log('PIERRE FINI')
    parlerBonome()
  }
  function parlerBonome(){
    console.log("PARLER BONOME")
    Data.network.sendMessage("NpcGenericActionRequestMessage", {      npcId: Data.almaBonomeCell, npcActionId: 3, npcMapId: 101450245    });
    for (var i of Data.almaBonome) {
      Data.network.sendMessage("NpcDialogReplyMessage", {replyId: i});
    }
    Data.network.sendMessage("QuestStepInfoRequestMessage", {questId: Data.almaQuestId});
    Data.network.sendMessage("LeaveDialogRequestMessage")
    outBonome()
  }
  async function outBonome() {
    await tool.wait(1000)
    console.log("On sort de la map pierre apres avoir parler au bonome")
    var path = Data.Movement.moveToCell(Data.currentCell, 452)
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId:  101452297
    });
    Data.almanax = true
    console.log('ALMANAX OK')
  }
  function almaBackToall(){
    console.log('GO TO HALL')
    var path = Data.Movement.moveToCell(Data.currentCell, 466);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 101450251
    });
  }
  function finirParlerVieux(){
    console.log('PARLE FINIR QUETE VIEUX')
    Data.network.sendMessage("NpcGenericActionRequestMessage", {
      npcId: -2, npcActionId: 3, npcMapId: 101450251
    });
    Data.network.sendMessage("NpcDialogReplyMessage", {
      replyId: 13564
    });
    Data.network.sendMessage("NpcDialogReplyMessage", {
      replyId: 13565
    });

    goOut()
  }
  async function goOut(){
    Data.network.sendMessage("LeaveDialogRequestMessage");
    console.log('ON SORT DEHORS', Data.currentCell, Data.mapId)
    var path = Data.Movement.moveToCell(Data.currentCell, 465);
    Data.network.sendMessage("GameMapMovementRequestMessage", {
      keyMovements: path,
      mapId: Data.mapId
    });
    Data.network.sendMessage("GameMapMovementConfirmMessage");
    Data.network.sendMessage("MapInformationsRequestMessage", {
      mapId: 84805890
    });
  }
  async function vendreMarchand(){
    console.log(Data.mapId)
    Data.network.sendMessage("ExchangeOnHumanVendorRequestMessage", {
      humanVendorId: Data.marchandCharacterId, humanVendorCell: Data.marchandCharacterCell
    });
    await tool.wait(1500)
    Data.network.sendMessage("ExchangeBuyMessage", {
      objectToBuyId: Data.marchandObjectId2, quantity: 1
    });

    Data.network.sendMessage("LeaveDialogRequestMessage");

    Data.network.sendMessage("AchievementRewardRequestMessage", {
      achievementId: -1
    });
    Data.network.crashed = 5
    Data.network.close()
    shell.exec("cd /lib/dev/up10fork && npm start " + Data.username)
  }

  return new Promise(async (resolve, reject) => {
    if (Data.mapId === 81265666) {
      go(558, 81265667, 81268739)
    } else if (Data.mapId === 81267712) {
      go(558, 81268739, 81267713)
    } else if (Data.mapId === 81264642) {
      go(558, 81268739, 81264643)
    } else if (Data.mapId === 81268739) {
      go(503, 81269763, 81269251)
    } else if (Data.mapId === 81269763) {
      go(447, 81270787, 81270275)
    } else if (Data.mapId === 81270787) {
      go(556, 80216068, 81270788)
    } else if (Data.mapId === 80216068) {
      // quetes 42 kamas
      /*
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -3, npcActionId: 3, npcMapId: 80216068
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 31777
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 31775
      });
      Data.network.sendMessage("QuestStepInfoRequestMessage", {
        questId: 1427
      });
      await tool.wait(500)
      */
      go(11, 80216067, 80216067)  // 81267714 enu
    
    } else if (Data.mapId === 81267714) {
      go(558 , 81267715, 81267715)
    } else if (Data.mapId === 80216067) {
      go(279 , 80216579, 80216579)
    } else if (Data.mapId === 80216579) {
      go(8, 80216578 , 80216578 )
    } else if (Data.mapId === 80216578 ) {
      go(335, 80217090 , 80217090 )
    } else if (Data.mapId === 80217090 ) {
      go(553, 80217091 , 80217091 )
    } else if (Data.mapId === 80217091 ) {
      go(555, 80217092, 80217092)
    } else if (Data.mapId === 80217092) {
      go(555, 80217093, 80217093)
    } else if (Data.mapId === 80217093) {
      go(555, 80217094, 80217094)
    } else if (Data.mapId === 80217094) {
      go(555, 80217095, 80217095)
    } else if (Data.mapId === 80217095) {
      go(279, 80217607, 80217607)
    } else if (Data.mapId === 80217607) {
      go(447, 80218119, 80218119)
    } else if (Data.mapId === 80218119) {
      go(447, 80218631, 80218631)
    } else if (Data.mapId === 80218631) {
      go(447, 80219143, 80219143)
    } else if (Data.mapId === 80219143) {
      go(1, 80219142, 80219142)
    } else if (Data.mapId === 80219143) {
      go(1, 80219142, 80219142)
    } else if (Data.mapId === 80219142) {
      go(1, 80219141, 80219141)
    } else if (Data.mapId === 80219142) {
      go(1, 80219141, 80219141)
    } else if (Data.mapId === 80219141) {
      go(504, 80218629, 80218629)
    } else if (Data.mapId === 80218629) {
      go(12, 80218628, 80218628)
    } else if (Data.mapId === 80218628) {
      go(476, 80218116, 80218116)
    } else if (Data.mapId === 80218116) {
      go(12, 80218115, 80218115)
    } else if (Data.mapId === 80218115) {
      go(503, 80218627, 80218627)
    } else if (Data.mapId === 80218627) {
      go(503, 80219139, 80219139)
    } else if (Data.mapId === 80219139) {
      go(503, 80219651, 80219651)
    } else if (Data.mapId === 80219651) {
      go(503, 80220163, 80220163)
    } else if (Data.mapId === 80220163) {
      go(533, 80220164, 80220164)
    } else if (Data.mapId === 80220164) {
      go(279, 80220676, 80220676)
    } else if (Data.mapId === 80220676) {
      Data.network.sendMessage("NpcGenericActionRequestMessage", {
        npcId: -2, 
        npcActionId: 3, 
        npcMapId: 80220676
      });
      Data.network.sendMessage("ObjectAveragePricesGetMessage");
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 10537
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 10535
      });
      Data.network.sendMessage("NpcDialogReplyMessage", {
        replyId: 33028
      });
      Data.network.sendMessage("MapInformationsRequestMessage", {
        mapId: 84804101
      });
      Data.network.sendMessage("AchievementRewardRequestMessage", {
        achievementId: -1
      });
    } else if (Data.mapId === 84804101) {
      go(555, 84804102, 84804102)
    } else if (Data.mapId === 84804102) {
      go(555, 84804103, 84804103)
    } else if (Data.mapId === 84804103) {
      go(548, 84804104, 84804104)
    } else if (Data.mapId === 84804104) {
      go(548, 84804105, 84804105)
    } else if (Data.mapId === 84804105) {
      go(167, 84672521, 84672521)
    } else if (Data.mapId === 84672521) {
      go(167, 84673033, 84673033)
    } else if (Data.mapId === 84673033) {
      go(553, 84673034, 84673034)
    } else if (Data.mapId === 84673034) {
      go(553, 84673035, 84673035)
    } else if (Data.mapId === 84673035) {
      go(335, 84673547, 84673547)
    } else if (Data.mapId === 84673547) {
      go(553, 84673548, 84673548)
    } else if (Data.mapId === 84673548) {
      go(553, 84673549, 84673549)
    } else if (Data.mapId === 84673548) {
      go(553, 84673549, 84673549)
    } else if (Data.mapId === 84673549) {
      go(335, 84674061, 84674061)
    } else if (Data.mapId === 84674061) {
      var path = Data.Movement.moveToCell(Data.currentCell, 167);
      Data.network.sendMessage("GameMapMovementRequestMessage", {
        keyMovements: path,
        mapId: Data.mapId
      });
      Data.network.sendMessage("GameMapMovementConfirmMessage");
      Data.network.sendMessage("ChangeMapMessage", {
        mapId: 84674573
      });
      Data.network.sendMessage("MapInformationsRequestMessage", {
        mapId: 71303168
      });
    } else if (Data.mapId === 71303168) {
       var path = Data.Movement.moveToCell(Data.currentCell, 167);
       Data.network.sendMessage("GameMapMovementRequestMessage", {
         keyMovements: path,
         mapId: Data.mapId
       });
       Data.network.sendMessage("GameMapMovementConfirmMessage");
       Data.network.sendMessage("ChangeMapMessage", {
         mapId: 71303680
       });
       Data.network.sendMessage("MapInformationsRequestMessage", {
         mapId: 84675085
       });
       
    } else if (Data.mapId === 84675085) {
      go(167, 84675597, 84675597)
    } else if (Data.mapId === 84675597) {
      go(167, 84676109, 84676109)
    }  else if (Data.mapId === 84676109) {
      if (Data.amaknaUnderground == false) {
        var path = Data.Movement.moveToCell(Data.currentCell, 275);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 84676109
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 479314, skillInstanceUid: 19503477
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 101717525
        });
      } else {
        go(335, 84676621, 84676621)
      }
    }

    else if (Data.mapId === 101717525) { // souterrain amakna
      Data.amaknaUnderground = true                  
      Data.network.sendMessage("MapInformationsRequestMessage", {
        mapId: 101717525
      });
      Data.network.sendMessage("AchievementRewardRequestMessage", {
        achievementId: -1
      });
      Data.network.sendMessage("InteractiveUseRequestMessage", {
        elemId: 476435, skillInstanceUid: 19508884
      });
      Data.network.sendMessage("MapInformationsRequestMessage", {
        mapId: 84676109
      });
      
    } else if (Data.mapId === 84676621) {
      console.log("ARRIVER MAP DROITE DE LA MINE")
      go(8, 84676620, 84676620)
    } else if (Data.mapId === 84676620) {
      go(8, 84676619, 84676619)
    } else if (Data.mapId === 84676619) {
      go(11, 84676618, 84676618)
    } else if (Data.mapId === 84676618) {
      go(8, 84676617, 84676617)
    } else if (Data.mapId === 84676617) {
      go(8, 84676616, 84676616)
    } else if (Data.mapId === 84676616) {
      go(8, 84676615, 84676615)
    } else if (Data.mapId === 84676615) {
      go(8, 84676614, 84676614)
    } else if (Data.mapId === 84676614) {
      go(8, 84676613, 84676613)
    } else if (Data.mapId === 84676613) {
      go(8, 84676612, 84676612)
    } else if (Data.mapId === 84676612) {
      go(8, 84676611, 84676611)
    } else if (Data.mapId === 84676611) {
      go(8, 84676610, 84676610)
    } else if (Data.mapId === 84676610) {
      go(8, 84676609, 84676609)
    } else if (Data.mapId === 84676609) {
      go(307, 84677121, 84677121 )
    } else if (Data.mapId === 84676609) {
      go(307, 84677121, 84677121 )
    } else if (Data.mapId === 84677121) {
      go(307, 84677633, 84677633 )
    } else if (Data.mapId === 84677633) {
      go(307, 84678145, 84678145 )
    } else if (Data.mapId === 84678145) {
      go(167, 84678657, 84678657)
    }  else if (Data.mapId === 84678657) {
        go(5, 84678656, 84678656)
    } else if (Data.mapId === 84678656) {
      if (Data.astrubRockyInlet == false) {
        go(8, 84678913, 84678913)
        Data.astrubRockyInlet = true
      }
      else {
        go(280, 84678144, 84678144 )
      }
    } else if (Data.mapId === 84678913) {
      go(557, 84678656, 84678656)
    } else if (Data.mapId === 84678144) {
      go(8, 84678401, 84678401)
    } else if (Data.mapId === 84678401) {
      go(364, 84677889, 84677889)
    } else if (Data.mapId === 84677889) {
      go(6, 84677890, 84677890)
    } else if (Data.mapId === 84677890) {
      go(448, 84677378, 84677378)
    } else if (Data.mapId === 84677890) {
      go(448, 84677378, 84677378)
    } else if (Data.mapId === 84677378) {
      go(280, 84676866, 84676866)
    } else if (Data.mapId === 84676866) {
      go(280, 84676354, 84676354)
    } else if (Data.mapId === 84676354) {
      go(280, 84675842, 84675842)
    } else if (Data.mapId === 84675842) {
      go(280, 84675330, 84675330)
    } else if (Data.mapId === 84675330) {
      go(308, 84674818, 84674818)
    } else if (Data.mapId === 84674818) {
      go(280, 84674306, 84674306)
    } else if (Data.mapId === 84674306) {
      go(554, 84674305, 84674305)
    } else if (Data.mapId === 84674305) {
      go(223, 84674817, 84674817)
    } else if (Data.mapId === 84674817) {
      go(307, 84675329, 84675329)
    } else if (Data.mapId === 84675329) {
      go(555, 84675072, 84675072) 
    } else if (Data.mapId === 84675072) {
      console.log("Entrée d'Astrub")
      go(553, 84675073, 84675073) 
    } else if (Data.mapId === 84675073) {
      go(55, 84675585, 84675585) 
    } else if (Data.mapId === 84675585) {
      go(553, 84675586, 84675586)
    } else if (Data.mapId === 84675586) {
        go(553, 84675587, 84675587) 
    } else if (Data.mapId === 84675587) {
      console.log("HDV Mineur")
      if (Data.astrubUnderground == false) {
        var path = Data.Movement.moveToCell(Data.currentCell, 304);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 84675587
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 465427, skillInstanceUid: 19502969
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 101716483
        });
      } else {
        go(224, 84675075, 84675075)
      }
    } else if (Data.mapId === 101716483) {
      console.log("Entrée Souterrain Astrub")
      if (Data.astrubUnderground == false) {
        var path = Data.Movement.moveToCell(Data.currentCell, 171);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 101716483
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 101715459
        });         
      } else {
        var path = Data.Movement.moveToCell(Data.currentCell, 522);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 101716483
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 84675587
        });
      }
    } else if (Data.mapId === 101715459) {
      console.log("Souterrain Astrub Avant Escaliers")
      if (Data.astrubUnderground == false) {
        var path = Data.Movement.moveToCell(Data.currentCell, 208);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 101715459
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 510070, skillInstanceUid: 19508978
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 101715461
        });
      } else {
        var path = Data.Movement.moveToCell(Data.currentCell, 501);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 101715459
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");        
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 101716483
        });
      }
    } else if (Data.mapId === 101715461) {
      console.log("Souterrain Astrub Intérieur")
      if (Data.astrubUnderground == false) {
        var path = Data.Movement.moveToCell(Data.currentCell, 477)
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 101715461
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 101715463
        });
      } else {
        var path = Data.Movement.moveToCell(Data.currentCell, 204);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 101715461
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 476188, skillInstanceUid: 19508986
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 101715459
        });
      }
    } else if (Data.mapId === 101715463) {
      console.log("Souterrain Profond d'Astrub")
      Data.astrubUnderground = true
      if (Data.astrubUnderground == true) {
        var path = Data.Movement.moveToCell(Data.currentCell, 135)
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 101715463
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 101715461
        });   
        console.log("Si je spam ça, c'est que je me suis fait aggro")
      }
    } else if (Data.mapId === 84675075) {
      console.log("ARRIVER APRES MINE - Statut Iop")
      go(8, 84675074, 84675074)
    } else if (Data.mapId === 84675074) {
      go(392, 84674562, 84674562)
    } else if (Data.mapId === 84674562) {
      go(308, 84674050, 84674050)
    } else if (Data.mapId === 84674050) {
      go(420, 84673538, 84673538)
    } else if (Data.mapId === 84673538) {
      go(420, 84673026, 84673026)
    } else if (Data.mapId === 84673026) {
      go(420, 84672514, 84672514)
    } else if (Data.mapId === 84672514) {
      go(420, 84804098, 84804098)
    } else if (Data.mapId === 84804098) {
      go(308, 84804610, 84804610)
    } else if (Data.mapId === 84804610) {
      go(364, 84805122, 84805122)
    } else if (Data.mapId === 84805122) {
      go(364, 84805634, 84805634)
    } else if (Data.mapId === 84805634) {
      go(336, 84806146, 84806146)
    } else if (Data.mapId === 84806146) {
      go(336, 84806658, 84806658)
    } else if (Data.mapId === 84806658) {
      var path = Data.Movement.moveToCell(Data.currentCell, 392);
      Data.network.sendMessage("GameMapMovementRequestMessage", {
        keyMovements: path,
        mapId: Data.mapId
      });
      Data.network.sendMessage("GameMapMovementConfirmMessage");
      Data.network.sendMessage("ChangeMapMessage", {
        mapId: 84807170
      });
      Data.network.sendMessage("MapInformationsRequestMessage", {
        mapId:  134932
      });
    } else if (Data.mapId === 134932) {
      go(336, 135444, 135444)
    } else if (Data.mapId === 135444) {
      go(336, 135956, 135956)
    } else if (Data.mapId === 135956) {
      go(336, 136468, 136468)
    } else if (Data.mapId === 136468) {
      go(336, 136980, 136980)
    } else if (Data.mapId === 136980) {
      go(420, 137492, 137492)
    } else if (Data.mapId === 137492) {
      go(392, 138004, 138004)
    } else if (Data.mapId === 138004) {
      go(392, 138516, 138516)
    } else if (Data.mapId === 138516) {
      go(392, 139028, 139028)
    } else if (Data.mapId === 139028) {
      go(392, 139540, 139540)
    } else if (Data.mapId === 139540) {
      go(392, 140052, 140052)
    } else if (Data.mapId === 140052) {
      go(392, 140564, 140564)
    } else if (Data.mapId === 140564) {
      go(392, 141076, 141076)
    } else if (Data.mapId === 141076) {
      go(392, 141588, 141588)
    } else if (Data.mapId === 141588) {
      go(557, 141587, 141587)
    } else if (Data.mapId === 141587) {
      go(557, 141586, 141586)
    } else if (Data.mapId === 141586) {
      go(557, 141585, 141585)
    } else if (Data.mapId === 141585) {
      go(557, 141584, 141584)
    } else if (Data.mapId === 141584) {
      go(557, 141583, 141583)
    } else if (Data.mapId === 141583) {
      go(557, 141582, 141582)
    } else if (Data.mapId === 141582) {
      go(557, 141581, 141581)
    } else if (Data.mapId === 141581) {
      go(557, 141580, 141580)
    } else if (Data.mapId === 141580) {
      go(308, 142092, 142092)
    } else if (Data.mapId === 142092) {
      go(308, 142604, 142604)
    } else if (Data.mapId === 142604) {
      go(308, 143116, 143116)
    } else if (Data.mapId === 14311) {
      go(308, 143116, 143116)
    } else if (Data.mapId === 143116) {
      go(308, 143628, 143628)
    } else if (Data.mapId === 143628) {
      go(308, 144140, 144140)
    } else if (Data.mapId === 144140) {
      if (Data.brakmarUnderground === false) {
        go(308, 144652, 144652)
      }
      else {
        go(8, 144141, 144141)
      }
    } else if (Data.mapId === 144652) {
      go(555, 144651, 144651)
    } else if (Data.mapId === 144651) {
      go(555, 144650, 144650)
    } else if (Data.mapId === 144650) {
      go(555, 144649, 144649)
    } else if (Data.mapId === 144649) {
      go(555, 144648, 144648)
    } else if (Data.mapId === 144648) {
      go(555, 144647, 144647)
    } else if (Data.mapId === 144647) {
      go(555, 144646, 144646)
    } else if (Data.mapId === 144646) {
      go(555, 144645, 144645)
    } else if (Data.mapId === 144645) {
      go(555, 144644, 144644)
    } else if (Data.mapId === 144644) {
      go(555, 144643, 144643)
    } else if (Data.mapId === 144643) {
      go(555, 144642, 144642)
    } else if (Data.mapId === 144642) {
      go(555, 144641, 144641)
    } else if (Data.mapId === 144641) {
      go(555, 144384, 144384)
    } else if (Data.mapId === 144384) {
      go(555, 144385, 144385)
    } else if (Data.mapId === 144385) {
      go(555, 144386, 144386)
    } else if (Data.mapId === 144386) {
      go(555, 144387, 144387)
    } else if (Data.mapId === 144387) {
      go(555, 144388, 144388)
    } else if (Data.mapId === 144388) {
      go(555, 144389, 144389)
    } else if (Data.mapId === 144389) {
      go(555, 144390, 144390)
    } else if (Data.mapId === 144390) {
      go(555, 144391, 144391)
    } else if (Data.mapId === 144391) {
      go(555, 144392, 144392)
    } else if (Data.mapId === 144392) {
      go(555, 144393, 144393)
    } else if (Data.mapId === 144393) {
      go(555, 144394, 144394)
    } else if (Data.mapId === 144394) {
      go(555, 144395, 144395)
    } else if (Data.mapId === 144395) {
      go(555, 144396, 144396)
    } else if (Data.mapId === 144396) {
      go(555, 144397, 144397)
    } else if (Data.mapId === 144397) {
      go(555, 144398, 144398)
    } else if (Data.mapId === 144398) {
      go(555, 144399, 144399)
    } else if (Data.mapId === 144399) {
      go(555, 144400, 144400)
    } else if (Data.mapId === 144400) {
      go(555, 144401, 144401)
    } else if (Data.mapId === 144401) {
      go(555, 144402, 144402)
    } else if (Data.mapId === 144402) {
      go(555, 144403, 144403)
    } else if (Data.mapId === 144403) {
      go(555, 144404, 144404)
    } else if (Data.mapId === 144404) {
      go(555, 144405, 144405)
    } else if (Data.mapId === 144405) {
      go(555, 144406, 144406)
    } else if (Data.mapId === 144406) {
      go(555, 144407, 144407)
    } else if (Data.mapId === 144407) {
      go(555, 144408, 144408)
    } else if (Data.mapId === 144408) {
      go(555, 144409, 144409)
    } else if (Data.mapId === 144409) {
      go(555, 144410, 144410)
    } else if (Data.mapId === 144410) {
      go(555, 144411, 144411)
    } else if (Data.mapId === 144411) {
      go(555, 144412, 144412)
    } else if (Data.mapId === 144412) {
      go(555, 144413, 144413)
    } else if (Data.mapId === 144413) {
      go(555, 144414, 144414)
    } else if (Data.mapId === 144414) {
      go(251, 143902, 143902)
      //go(251, 143902, 143902)
      /*
      var path = Data.Movement.moveToCell(Data.currentCell, 251);
      Data.network.sendMessage("GameMapMovementRequestMessage", {
        keyMovements: path,
        mapId: Data.mapId
      });
      Data.network.sendMessage("GameMapMovementConfirmMessage");
      Data.network.sendMessage("ChangeMapMessage", {
        mapId: 143902
      });
      Data.network.sendMessage("MapInformationsRequestMessage", {
        mapId: 143902
      });
      */
    } else if (Data.mapId === 143902){
      if (Data.brakmarUnderground == false) {  
        var path = Data.Movement.moveToCell(Data.currentCell, 547);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: Data.mapId
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");

        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 414099, skillInstanceUid: 19487800
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 144415
        });
      }
      else {
        go(9, 143901, 143901)
      }
    } else if (Data.mapId === 144415) {
      if (Data.brakmarUnderground == false) {  
        console.log("ARRIVER BRAKMAR SA MER")
        Data.network.sendMessage("AchievementRewardRequestMessage", {
          achievementId: -1
        });
        go(555, 144416, 144416)
      }
      else {
        var path = Data.Movement.moveToCell(Data.currentCell, 110);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: Data.mapId
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");

        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 406480, skillInstanceUid: 19488281
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 143902
        });
      }
    } else if (Data.mapId === 144416) {
      go(551, 144417, 144417)
    } else if (Data.mapId === 144417) {
      go(555, 144418, 144418)
    } else if (Data.mapId === 144418) {
      go(83, 143906, 143906)
    } else if (Data.mapId === 143906) {
      go(83, 143394, 143394)
    } else if (Data.mapId === 143394) {
      go(83, 142882, 142882)
    } else if (Data.mapId === 142882) {
      go(83, 142370, 142370)
    } else if (Data.mapId === 142370) {
      go(83, 141858, 141858)
    } else if (Data.mapId === 141858) {
      go(83, 141346, 141346)
    } else if (Data.mapId === 141346) {
      go(555, 141347, 141347)
    } else if (Data.mapId === 141347) {
      go(555, 141348, 141348)
    } else if (Data.mapId === 141348) {
      go(555, 141349, 141349)
    } else if (Data.mapId === 141349) {
      go(554, 141350, 141350)
    } else if (Data.mapId === 141350) {
      go(555, 1413451, 141351)
    } else if (Data.mapId === 141351) {
      go(280, 141863, 141863)
    } else if (Data.mapId === 141863) {
      go(280, 142375, 142375)
    } else if (Data.mapId === 142375) {
      go(308, 142887, 142887)
    } else if (Data.mapId === 142887) {
      go(8, 142886, 142886)
    } else if (Data.mapId === 142886) {
      go(11, 142885, 142885)
    } else if (Data.mapId === 142885) {
      go(140, 143397, 143397)
    } else if (Data.mapId === 143397) {
      go(555, 143398, 143398)
    } else if (Data.mapId === 143398) {
      go(551, 143399, 143399)
    } else if (Data.mapId === 143399) {
      go(392, 143911, 143911)
    } else if (Data.mapId === 143911) {
      go(112, 144423, 144423)
    } else if (Data.mapId === 144423) {
      go(504, 144935, 144935)
    } else if (Data.mapId === 144935) {
      go(336, 145447, 145447)
    } else if (Data.mapId === 145447) {
      go(448, 145959, 145959)
    } else if (Data.mapId === 145959) {
      go(10, 145958, 145958)
    } else if (Data.mapId === 145958) {
      go(392, 146470, 146470)
    } else if (Data.mapId === 146470) {
      go(8, 146469, 146469)
    } else if (Data.mapId === 146469) {
      go(8, 146468, 146468)
    } else if (Data.mapId === 146468) {
      go(5, 146467, 146467)
    } else if (Data.mapId === 146467) {
      go(7, 146466, 146466)
    } else if (Data.mapId === 146466) {
      go(9, 146465, 146465)
    } else if (Data.mapId === 146465) {
      go(9, 146464, 146464)
    } else if (Data.mapId === 146464) {
      go(9, 146463, 146463)
    } else if (Data.mapId === 146463) {
      go(279, 145951, 145951)
    } else if (Data.mapId === 145951) {
      if (Data.brakmarUnderground == false) {
        var path = Data.Movement.moveToCell(Data.currentCell, 327);
        Data.network.sendMessage("GameMapMovementRequestMessage", {
          keyMovements: path,
          mapId: 145951
        });
        Data.network.sendMessage("GameMapMovementConfirmMessage");
        Data.network.sendMessage("InteractiveUseRequestMessage", {
          elemId: 435747, skillInstanceUid: 19489944
        });
        Data.network.sendMessage("MapInformationsRequestMessage", {
          mapId: 28445445
        });
      } else {
        go(447, 145439, 145439)
      }
    } else if (Data.mapId === 28445445) {
      Data.brakmarUnderground = true                  
      Data.network.sendMessage("MapInformationsRequestMessage", {
        mapId: 28445445
      });
      Data.network.sendMessage("AchievementRewardRequestMessage", {
        achievementId: -1
      });
      Data.network.sendMessage("InteractiveUseRequestMessage", {
        elemId: 435756, skillInstanceUid: 19494586
      });
      Data.network.sendMessage("MapInformationsRequestMessage", {
        mapId: 145951
      });
    } else if (Data.mapId === 145439) {
      go(307, 144927, 144927)
    } else if (Data.mapId === 144927) {
      go(363, 144415, 144415)
    } else if (Data.mapId === 143901) {
      console.log("Fin de Brakmar.")
      go(8, 143900, 143900)
    } else if (Data.mapId === 143900) {
      go(9, 143899, 143899)
    } else if (Data.mapId === 143899) {
      go(9, 143898, 143898)
    } else if (Data.mapId === 143898) {
      go(9, 143897, 143897)
    } else if (Data.mapId === 143897) {
      go(9, 143896, 143896)
    } else if (Data.mapId === 143896) {
      go(9, 143895, 143895)
    } else if (Data.mapId === 143895) {
      go(7, 143894, 143894)
    } else if (Data.mapId === 143894) {
      go(8, 143893, 143893)
    } else if (Data.mapId === 143893) {
      go(8, 143892, 143892)
    } else if (Data.mapId === 143892) {
      go(8, 143891, 143891)
    } else if (Data.mapId === 143891) {
      go(8, 143890, 143890)
    } else if (Data.mapId === 143890) {
      go(8, 143889, 143889)
    } else if (Data.mapId === 143889) {
      go(8, 143888, 143888)
    } else if (Data.mapId === 143888) {
      go(9, 143887, 143887)
    } else if (Data.mapId === 143887) {
      go(9, 143886, 143886)
    } else if (Data.mapId === 143886) {
      go(9, 143885, 143885)
    } else if (Data.mapId === 143885) {
      go(9, 143884, 143884)
    } else if (Data.mapId === 143884) {
      go(8, 143883, 143883)
    } else if (Data.mapId === 143883) {
      go(9, 143882, 143882)
    } else if (Data.mapId === 143882) {
      go(9, 143881, 143881)
    } else if (Data.mapId === 143881) {
      go(9, 143880, 143880)
    } else if (Data.mapId === 143880) {
      go(8, 143879, 143879)
    } else if (Data.mapId === 143879) {
      go(8, 143878, 143878)
    } else if (Data.mapId === 143878) {
      go(8, 143877, 143877)
    } else if (Data.mapId === 143877) {
      go(8, 143876, 143876)
    } else if (Data.mapId === 143876) {
      go(8, 143875, 143875)
    } else if (Data.mapId === 143875) {
      go(8, 143874, 143874)
    } else if (Data.mapId === 143874) {
      go(8, 143873, 143873)
    } else if (Data.mapId === 143873) {
      go(8, 143872, 143872)
    } else if (Data.mapId === 143872) {
      go(8, 144129, 144129)
    } else if (Data.mapId === 144129) {
      go(8, 144130, 144130)
    } else if (Data.mapId === 144130) {
      go(8, 144131, 144131)
    } else if (Data.mapId === 144131) {
      go(8, 144132, 144132)
    } else if (Data.mapId === 144132) {
      go(8, 144133, 144133)
    } else if (Data.mapId === 144133) {
      go(8, 144134, 144134)
    } else if (Data.mapId === 144134) {
      go(8, 144135, 144135)
    } else if (Data.mapId === 144135) {
      go(8, 144136, 144136)
    } else if (Data.mapId === 144136) {
      go(8, 144137, 144137)
    } else if (Data.mapId === 144137) {
      go(8, 144138, 144138)
    } else if (Data.mapId === 144138) {
      go(8, 144139, 144139)
    } else if (Data.mapId === 144139) {
      go(8, 144140, 144140)
    } else if (Data.mapId === 144141) {
      go(8, 144142, 144142)
    } else if (Data.mapId === 144142) {
      go(8, 144143, 144143)
    } else if (Data.mapId === 144143) {
      go(8, 144144, 144144)
    } else if (Data.mapId === 144144) {
      go(503, 143632, 143632)
    } else if (Data.mapId === 143632) {
      go(503, 143120, 143120)
    } else if (Data.mapId === 143120) {
      go(503, 142608, 142608)
    } else if (Data.mapId === 142608) {
      go(503, 142096, 142096)
    } else if (Data.mapId === 142096) {
      go(11, 142097, 142097)
    } else if (Data.mapId === 142097) {
      go(11, 142098, 142098)
    } else if (Data.mapId === 142098) {
      go(11, 142099, 142099)
    } else if (Data.mapId === 142099) {
      go(6, 142100, 142100)
    } else if (Data.mapId === 142100) {
      go(6, 142101, 142101)
    } else if (Data.mapId === 142101) {
      go(6, 142102, 142102)
    } else if (Data.mapId === 142102) {
      go(223, 141590, 141590)
    } else if (Data.mapId === 141590) {
      go(223, 141078, 141078)
    } else if (Data.mapId === 141078) {
      go(223, 140566, 140566)
    } else if (Data.mapId === 140566) {
      go(223, 140054, 140054)
    } else if (Data.mapId === 140054) {
      go(223, 139542, 139542)
    } else if (Data.mapId === 139542) {
      go(279, 139030, 139030)
    } else if (Data.mapId === 139030) {
      go(279, 138518, 138518)
    } else if (Data.mapId === 138518) {
      go(279, 138006, 138006)
    } else if (Data.mapId === 138006) {
      go(279, 137494, 137494)
    } else if (Data.mapId === 137494) {
      go(279, 136982, 136982)
    } else if (Data.mapId === 136982) {
      go(279, 136470, 136470)
    } else if (Data.mapId === 136470) {
      go(503, 135958, 135958)
    } else if (Data.mapId === 135958) {
      go(279, 135446, 135446)
    } else if (Data.mapId === 135446) {
      go(8, 135447, 135447)
    } else if (Data.mapId === 135447) {
      go(8, 135448, 135448)
    } else if (Data.mapId === 135448) {
      go(8, 135449, 135449)
    } else if (Data.mapId === 135449) {
      go(8, 135450, 135450)
    } else if (Data.mapId === 135450) {
      go(251, 134938, 134938)
    } else if (Data.mapId === 134938) {
      var path = Data.Movement.moveToCell(Data.currentCell, 251);
      Data.network.sendMessage("GameMapMovementRequestMessage", {
        keyMovements: path,
        mapId: Data.mapId
      });
      Data.network.sendMessage("GameMapMovementConfirmMessage");
      Data.network.sendMessage("ChangeMapMessage", {
        mapId: 134426
      });
      Data.network.sendMessage("MapInformationsRequestMessage", {
        mapId: 71303168
      });
    } else if (Data.mapId === 84806916 && !Data.almanax) {
      console.log("ARRIVÉE A LA MAP DE START !!!")
      shell.exec("cd /lib/dev/up10fork && npm start " + Data.username)
      /*
      if (Data.level < 10  ) {
        console.log("PERSO PLUS PETIT QUE 10")
        Data.network.crashed = 5
        Data.network.close()
        shell.exec("cd /lib/dev/up10fork && npm start " + Data.username)
      } else {
        shell.exec("cd /lib/dev/up10fork && npm start " + Data.username)
        //acheterMarchand()
      }
      */
    } else if (Data.mapId === 84806915 && !Data.almanax) {
      console.log('915')
      go(195, 84806403, 84806403)
    } else if (Data.mapId === 84806403 && !Data.almanax) {
      go(195, 84805891, 84805891)
    } else if (Data.mapId === 84805891 && !Data.almanax) {
      go(553, 84805890, 84805890)
    } else if (Data.mapId === 84805890 && !Data.almanax) { // MAP DEHORS
      console.log('MAP DEHORS')
      goIn()
    } else if (Data.mapId === 101450251 && !Data.almanax) {
      console.log("MAP DEDANS")
      prendreVieux()
    } else if (Data.mapId ===  101452297 && !Data.almanax) {
      console.log("MAP 2")
      prendrePorte()
    } else if (Data.mapId === 101450245 && !Data.almanax) {
      console.log("MAP PIERRE")
      parlerPierre()     
    } else if (Data.mapId ===  101452297 && Data.almanax) {
      almaBackToall()
    }  else if (Data.mapId === 101450251 && Data.almanax) {
      finirParlerVieux()
    } else if (Data.mapId === 84805890 && Data.almanax) {
      go(4, 84805891, 84805891)
      console.log("ON EST DEHORS")
    } else if (Data.mapId === 84805891 && Data.almanax) {
      go(364, 84806403, 84806403)
    } else if (Data.mapId === 84806403 && Data.almanax) {
      go(364, 84806915, 84806915)
    } else if (Data.mapId === 84806915 && Data.almanax) {
      go(4, 84806916, 84806916)
    } else if (Data.mapId === 84806916 && Data.almanax) { // map de start
      console.log('ARRIVER POUR VENDRE')
      vendreMarchand()// PLUS GO SUR LAUTRE MAP
    }
      



    else {
      console.log("PERDUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU")
    }
  });
}

/*

 //process.exit() // TU VAS PAS M'AIMER POPEYE
      //console.log(Data.username, Data.password, Data.serverId, true, 0)
      //new connect(Data.username, Data.password, Data.serverId, true, 0)
      //new accConnect(Data.username, Data.password, Data.serverId, true, 0);
      //Data.started = true


      */