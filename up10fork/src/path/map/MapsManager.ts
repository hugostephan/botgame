import Map from ".";
import { GraphicSizes } from "./AtlasLayout";
import Cell from "./Cell";
import DTConstants from "./../../DTConstants";
import axios from "axios";


export default class MapsManager {
  public static async getMap(mapId: number): Promise<Map> {
    

    const response = await axios.get(
      DTConstants.config.assetsUrl + "/maps/" + mapId + ".json"
    );
    const data = response.data;
    return this.buildMap(data);
  }

  

  private static buildMap(json: any): Map {
    const map = new Map(
      json.id,
      json.topNeighbourId,
      json.bottomNeighbourId,
      json.leftNeighbourId,
      json.rightNeighbourId
    );
    for (const cell of json.cells) {
      map.cells.push(new Cell(cell.l, cell.f, cell.c, cell.s, cell.z));
    }
    for (const key in json.midgroundLayer) {
      if (json.midgroundLayer.hasOwnProperty(key)) {
        const values = json.midgroundLayer[key];
        for (let i = 0; i < values.length; i++) {
          values[i] = { id: parseInt(key, 10), ...values[i] };
        }
        map.midgroundLayer.set(parseInt(key, 10), values);
      }
    }
    map.atlasLayout.width = json.atlasLayout.width;
    map.atlasLayout.height = json.atlasLayout.height;
    for (const key in json.atlasLayout.graphicsPositions) {
      if (json.atlasLayout.graphicsPositions.hasOwnProperty(key)) {
        const gs = json.atlasLayout.graphicsPositions[key] as GraphicSizes;
        map.atlasLayout.graphicsPositions.set(parseInt(key, 10), gs);
      }
    }

    return map;
  }
}
