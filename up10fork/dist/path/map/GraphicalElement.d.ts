export default class GraphicalElement {
    id: number;
    g: number;
    x: number;
    y: number;
    sx: number;
    sy: number;
    hue: number[];
    cx: number;
    cy: number;
    cw: number;
    ch: number;
}
