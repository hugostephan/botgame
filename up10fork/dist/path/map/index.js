"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AtlasLayout_1 = require("./AtlasLayout");
class default_1 {
    constructor(id, topNeighbourId, bottomNeighbourId, leftNeighbourId, rightNeighbourId) {
        this.cells = [];
        this.midgroundLayer = new Map();
        this.atlasLayout = new AtlasLayout_1.AtlasLayout();
        this.id = id;
        this.topNeighbourId = topNeighbourId;
        this.bottomNeighbourId = bottomNeighbourId;
        this.leftNeighbourId = leftNeighbourId;
        this.rightNeighbourId = rightNeighbourId;
    }
}
exports.default = default_1;
