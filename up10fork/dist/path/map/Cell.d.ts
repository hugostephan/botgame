export default class Cell {
    l: number;
    f: number;
    c: number;
    s: number;
    z: number;
    constructor(l?: number, f?: number, c?: number, s?: number, z?: number);
    isWalkable(isFightMode?: boolean): boolean;
    isFarmCell(): boolean;
    isVisible(): boolean;
    isObstacle(): boolean;
}
