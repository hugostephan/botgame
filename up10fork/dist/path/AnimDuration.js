"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AnimDuration {
    constructor(linear, horizontal, vertical) {
        this.linear = linear;
        this.horizontal = horizontal;
        this.vertical = vertical;
        //
    }
}
exports.default = AnimDuration;
