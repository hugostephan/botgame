"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Dispatcher_1 = require("./Dispatcher");
const HaapiConnection_1 = require("./HaapiConnection");
const Network_1 = require("./Network");
const movement_1 = require("./movement");
const DTConstants_1 = require("./DTConstants");
class data {
    constructor() {
        this.Movement = new movement_1.default();
        this.sequence = 0;
        this.subscriber = false;
        this.createdCamp = false;
        this.characterId = 0;
        this.currentCell = 0;
        this.serverId = 403;
        this.mapId = 0;
        this.createNew = 0; // si il est a false on en créé pas
        this.blackList = [];
        this.tutoGroupe = -4618;
        this.tutoInventory = [];
        this.almanax = false;
        this.itemId = 0;
        this.canScrap = true;
        this.canScrap2 = true;
        this.allId = {};
        this.weCanBuy = true;
        this.alreadyHDV = true;
        this.inventoryHDV = {};
        this.crafted = false;
        this.idCraft = 100;
        this.alreadyOnMap = false;
        this.marchandObjectId = 0;
        this.marchandObjectId2 = 0;
        this.numberCamp = 0;
    }
    init(username, password, serverId) {
        return __awaiter(this, void 0, void 0, function* () {
            this.haapi = new HaapiConnection_1.default();
            this.username = username;
            this.password = password;
            this.serverId = serverId;
            yield this.haapi.processHaapi(username, password, serverId);
            this.dis = new Dispatcher_1.default();
            this.network = new Network_1.default(this.dis);
            this.network.connect(DTConstants_1.default.config.sessionId, DTConstants_1.default.config.dataUrl);
        });
    }
}
exports.default = data;
