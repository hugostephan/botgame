export default class Dispatcher {
    private prefix;
    private listeners;
    constructor();
    register(eventName: string, callback: any, bind?: any): void;
    emit(eventName: string, ...params: any[]): void;
}
